from django.conf import settings
from django_hosts import patterns, host


host_patterns = patterns(
    '',
    host('fims', settings.ROOT_URLCONF, name='fims'),
    host('onboarding', 'fims.routers.onboarding', name='onboarding'),
    host('challenges', 'fims.routers.challenges', name='challenges'),
    host('oauth', 'fims.routers.oauth', name='oauth'),
    host('certificates', 'fims.routers.certificates', name='certificates'),
    # host('bcard', 'fims.routers.bcard', name='bcard'),
)


