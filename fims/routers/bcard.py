from django.conf.urls import url
from django.http import HttpResponse, Http404
from django.shortcuts import redirect
from django.urls import path
from django.views.generic import DetailView, RedirectView

from apps.user.models import BCard
from fims import settings


def bcard(request, username=None):
    return HttpResponse(str(username))


class BcardDetailView(DetailView):
    slug_url_kwarg = 'username'
    slug_field = 'username'
    model = BCard
    template_name = 'bcard/profile.html'
    template_name_404 = 'bcard/home.html'

    def get_object(self, queryset=None):
        try:
            return super(BcardDetailView, self).get_object(queryset)
        except Exception as e:
            pass

    def get_context_data(self, **kwargs):
        kwargs = super(BcardDetailView, self).get_context_data(**kwargs)
        kwargs['object_list'] = self.model.objects.all()
        return kwargs

    def get_template_names(self):
        if self.object:
            return self.template_name
        return self.template_name_404


class Maintenance(RedirectView):
    url = '/'


urlpatterns = [
    path('', BcardDetailView.as_view(), name="bcard-home"),
    path('<slug:username>/', BcardDetailView.as_view(), name="bcard"),
    url(r'^', Maintenance.as_view(), name='maintenance')
]





