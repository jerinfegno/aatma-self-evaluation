from django.http import HttpResponse
from django.urls import path, include


def hello(request):
    return HttpResponse('ON-BOARDING.FEGNO.COM')


urlpatterns = [
    path('', include('apps.onboarding.urls')),
    path('ping.html', hello, name="hello"),
]





