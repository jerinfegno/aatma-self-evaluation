"""FIMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin, messages
from django.shortcuts import redirect
from django.urls import path, include, reverse
import debug_toolbar
from django.conf.urls import url

from rest_auth.views import (
    LogoutView, UserDetailsView, PasswordChangeView,
    PasswordResetView, PasswordResetConfirmView
)
from apps.generic.login import LoginView as APILoginView

from apps.generic.index import HomeView, trigger_error, LoginView as WebLoginView
from apps.root import root


def redirect_to_login(request):
    """
    this view is used to disable allauth signup which redirects on failed social login;
    redirect it to settings.LOGIN_URL
    """
    messages.warning(request, "No account found with the registered email address.")
    return redirect(reverse(settings.LOGIN_URL))


urlpatterns = [
    path('fegnomin/', admin.site.urls),
    path('accounts/', include("django.contrib.auth.urls")),
    # path('login/', LoginView.as_view(), name='login'),
    path('login/', WebLoginView.as_view(template_name="paper/login.html"), name='login'),
    path('', HomeView.as_view(), name="dashboard-index"),
    # path('', login_required(RedirectView.as_view(pattern_name='leave:list')), name="dashboard-index"),
    path('', include("apps.accounts.urls")),
    path('', include("apps.asset.urls")),
    path('', include("apps.documents.urls")),
    path('', include("apps.leave.urls")),
    path('', include("apps.petty.urls")),
    path('', include("apps.project.urls")),
    path('', include("apps.user.urls")),
    path('', include("apps.payslip.urls")),
    path('sop/', include("apps.sop.urls")),
    path('', include("apps.drar.urls")),
    path('api/v1/bcard/', include("apps.bcard")),
    path('careers/recruitment/', include("apps.recruitment.urls")),
    path('api/v1/', root, name="root"),
    path('api/v1/', include('apps.leave.api_urls')),

    # path(r'nda-agreed-contents/', include('private_files.urls')),
    path('sentry-debug/', trigger_error),
    path('tinymce/', include('tinymce.urls')),
    path('accounts/social/signup/', redirect_to_login, name="socialaccount_signup"),
    path('accounts/', include('allauth.urls')),

    path('rest-auth/', include([
        # URLs that do not require a session or valid token
        url(r'^password/reset/$', PasswordResetView.as_view(),
            name='rest_password_reset'),
        url(r'^password/reset/confirm/$', PasswordResetConfirmView.as_view(),
            name='rest_password_reset_confirm'),
        url(r'^login/$', APILoginView.as_view(), name='rest_login'),
        # URLs that require a user to be logged in with a valid session / token.
        url(r'^logout/$', LogoutView.as_view(), name='rest_logout'),
        url(r'^user/$', UserDetailsView.as_view(), name='rest_user_details'),
        url(r'^password/change/$', PasswordChangeView.as_view(),
            name='rest_password_change'),
    ])),
    # path('base.html', TemplateView.as_view(template_name="paper/layout.html")),
    # path('dashboard.html', TemplateView.as_view(template_name="paper/examples/dashboard.html")),
    # path('icons.html', TemplateView.as_view(template_name="paper/examples/icons.html")),
    # path('notifications.html', TemplateView.as_view(template_name="paper/examples/notifications.html")),
    # path('tables.html', TemplateView.as_view(template_name="paper/examples/tables.html")),
    # path('typography.html', TemplateView.as_view(template_name="paper/examples/typography.html")),
    # path('user.html', TemplateView.as_view(template_name="paper/examples/user.html")),
    path('baton/', include('baton.urls')),
    path('__debug__/', include(debug_toolbar.urls)),
]


if settings.DEBUG:
    urlpatterns.extend([
        *static(settings.STATIC_URL, document_root=os.path.join(settings.BASE_DIR, settings.STATICFILES_DIRS[0])),
        *static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT),
    ])



