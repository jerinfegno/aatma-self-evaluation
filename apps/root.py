from collections import OrderedDict
from datetime import date

from django.contrib.auth.decorators import login_required
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse


@api_view()
@login_required
def root(request):
    def url(view_name, **kwargs):
        return request.build_absolute_uri(reverse(view_name, kwargs=kwargs))
    return Response(OrderedDict([
        ("Auth", OrderedDict([
            ('Login', url('rest_login', )),
            ('Logout', url('rest_logout', )),
            ('QRCode', url('user:qrcode', )),
            ('User Details', url('user:user-profile', )),
            ('Change Password', url('rest_password_change', )),
            ('Reset Password', url('rest_password_reset', )),
            ('Reset Password Confirm', url('rest_password_reset_confirm', )),
        ])),
        ("Petty", OrderedDict([
            ("Petty List", reverse('petty:petty-list', request=request,
                                   kwargs={'month': date.today().month, 'year': date.today().year}, )),
            ("Petty Detail", reverse('petty:petty-detail', request=request, kwargs={'pk': 1})),
        ])),
        ("Documents", OrderedDict([
            ("Circulars", reverse('documents:circular-api-v1', request=request)),
            ("Policy", reverse('documents:shared-document-api-v1', request=request, )),
            ("List/Add Documents", reverse('documents:user-document-api-v1', request=request, )),
            ("Update Documents", reverse('documents:user-document-detail-api-v1', request=request, kwargs={'pk': 1})),
        ])),
        ("Leave", OrderedDict([
            ("Leave Type", reverse('leave-api:leave-type-api', request=request)),
            ("Leave Request Create", reverse('leave-api:leave-request-api-v1', request=request)),
            ("Leave Request Display", reverse('leave-api:leave-request-display-api-v1', request=request)),
            ("Holidays", reverse('leave-api:holiday-api-v1', request=request)),
            ("Cancel Leave Request", reverse('leave-api:cancel-leave-request-api-v1', request=request, kwargs={'pk': 1})),
        ])),

    ]))
