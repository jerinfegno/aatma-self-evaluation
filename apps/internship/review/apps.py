from django.apps import AppConfig


class InternshipReviewConfig(AppConfig):
    name = 'internship_review'
