import pusher
from django.contrib.auth import get_user_model, login

from rest_framework import authentication, status
from rest_framework.authtoken.models import Token
from rest_framework.generics import UpdateAPIView, RetrieveUpdateAPIView

from .serializers import QRCodeSerialzier
from rest_framework.generics import CreateAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from apps.authentication import CSRFExemptSessionAuthentication, ScannedTokenBackend
from apps.user.serializers import UserSerializer

from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.contrib.auth import login, authenticate
from rest_framework.decorators import api_view
from .pusher import AuthPush
from django.conf import settings
User = get_user_model()


class UserAPIView(RetrieveUpdateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    authentication_classes = [CSRFExemptSessionAuthentication, ]

    def get_object(self):
        return self.request.user


@api_view(['POST'])
def scanned_login(request):
    # import pdb;pdb.set_trace()
    user = authenticate(request,     **request.data)
    if user is None: return Response({'error': 'Unauthenticated!'}, status=status.HTTP_401_UNAUTHORIZED)
    login(request, user)
    return Response({'response': 'Success'})


class QRCodeView(GenericAPIView):
    """
    Flow :
        The application wants to get the login for a mobile app user in the web without credentials.
        1. Web renders with a UUID contained webpage.
        2. Any mobile application with a valid session can read this UUID;
        3. When App shares this UUID as a logged-in user,
            the server pushes a token for this user to the browser which holded this UUID.
        4. Browser uses this token to authenticate behalf of mobile app session.

    ----------------|---------------------------|---------------------
        Mobile      |         Browser           |      Backend
    (Authenticated) |      (Needs Login)        |
    ----------------|---------------------------|---------------------
                    |             O <<<<<<<<<<<<<<<<<<<<<< O                    # 1. UUID ON SERVER SIDE RENDERING
          O <<<<<<<<<<<<<<<<<<<<< O             |                               # 2. MOBILE APP SCANS TO GET UUID
          0 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 0                    # 3. SEND UUID WITH SESSION FROM MOBILE TO SERVER
                    |             O <<<<<<<<<<<<<<<<<< O                        # 4. SERVER PUSHES TOKEN AND USERNAME TO WEB
                    |             O >>>>>>>>>>>>>>>>>> O                        # 5. WEB SHARES THIS TOKEN AND USERNAME TO GET A VALID SESSION
                    |                           |
    ----------------|---------------------------|---------------------

    """
    queryset = None
    serializer_class = QRCodeSerialzier
    authentication_classes = [CSRFExemptSessionAuthentication, ]
    permission_classes = [IsAuthenticated, ]

    def post(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        serializer = self.get_serializer(request.data, data=request.data, partial=partial)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        login_token_obj, _ = Token.objects.get_or_create(user=self.request.user)
        _uuid_data = serializer.data['uuid']
        username = self.request.user.username

        AuthPush().push_token(login_token_obj, _uuid_data, username)

        return Response({
            'response': 'success'
        })
