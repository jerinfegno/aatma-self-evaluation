import json

from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView, UpdateView

from apps.generic.index import UserProfileForm
from apps.user.form import UserProfileUpdateForm
from apps.user.models import OrganizationTree, Department, User


class UserProfileUpdateView(UpdateView):
    model = User
    field = "__all__"
    template_name = 'user/user_profile.html'
    form_class = UserProfileUpdateForm

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse('dashboard-index')






