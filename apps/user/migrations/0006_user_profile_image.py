# Generated by Django 3.1.3 on 2021-04-09 10:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0005_auto_20210407_1533'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='profile_image',
            field=models.ImageField(blank=True, null=True, upload_to='profile-image/', verbose_name='Profile Image'),
        ),
    ]
