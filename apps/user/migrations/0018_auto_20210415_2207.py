# Generated by Django 3.1.3 on 2021-04-15 16:37

import apps.generic.models.address
from django.db import migrations, models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0017_auto_20210415_1709'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='address_line_1',
            field=models.TextField(blank=True, help_text='Billing Address, Line 1, (Office Name)', null=True, verbose_name='Line 1'),
        ),
        migrations.AddField(
            model_name='user',
            name='address_line_2',
            field=models.TextField(blank=True, help_text='Billing Address, Line 2, (Street Name )', null=True, verbose_name='Line 2'),
        ),
        migrations.AddField(
            model_name='user',
            name='address_line_3',
            field=models.TextField(blank=True, help_text='Billing Address, Line 3, (City, [PinCode/Zipcode])', null=True, verbose_name='Line 3'),
        ),
        migrations.AddField(
            model_name='user',
            name='address_line_4',
            field=models.TextField(blank=True, help_text='Billing Address, Line 4 (State (State Code), Country)', null=True, verbose_name='Line 4'),
        ),
        migrations.AddField(
            model_name='user',
            name='contact_01',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None),
        ),
        migrations.AddField(
            model_name='user',
            name='contact_01_holder_name',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='contact_02',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None),
        ),
        migrations.AddField(
            model_name='user',
            name='contact_02_holder_name',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='name',
            field=models.CharField(max_length=256, null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='pincode',
            field=models.CharField(blank=True, max_length=6, null=True, validators=[apps.generic.models.address.pincode_validator]),
        ),
    ]
