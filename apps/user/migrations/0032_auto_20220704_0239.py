# Generated by Django 3.1.3 on 2022-07-03 21:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0031_auto_20220704_0235'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='college_grade',
            field=models.CharField(blank=True, max_length=12, null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='college_pass_out_year',
            field=models.PositiveIntegerField(default=2000),
        ),
        migrations.AddField(
            model_name='profile',
            name='degree_grade',
            field=models.CharField(blank=True, max_length=12, null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='degree_pass_out_year',
            field=models.PositiveIntegerField(default=2000),
        ),
        migrations.AddField(
            model_name='profile',
            name='tenth_grade',
            field=models.CharField(blank=True, max_length=12, null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='tenth_pass_out_year',
            field=models.PositiveIntegerField(default=2000),
        ),
    ]
