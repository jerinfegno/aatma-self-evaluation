# Generated by Django 3.1.3 on 2020-12-02 07:28

import apps.user.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_auto_20201202_1251'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Role',
            new_name='Designation',
        ),
        migrations.AddField(
            model_name='user',
            name='designation',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='user.designation'),
        ),
        migrations.AddField(
            model_name='user',
            name='reporting_to',
            field=models.ForeignKey(null=True, on_delete=apps.user.models.default_to_primary_user, to=settings.AUTH_USER_MODEL),
        ),
    ]
