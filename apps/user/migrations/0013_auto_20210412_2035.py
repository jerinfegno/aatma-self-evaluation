# Generated by Django 3.1.3 on 2021-04-12 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0012_auto_20210412_1606'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='advance_deduction',
        ),
        migrations.RemoveField(
            model_name='user',
            name='advance_payment',
        ),
        migrations.AlterField(
            model_name='user',
            name='basic_pay',
            field=models.FloatField(default=0.0, help_text='Basic Payment', verbose_name='Basic Pay'),
        ),
        migrations.AlterField(
            model_name='user',
            name='dearness_allowance',
            field=models.FloatField(default=0.0, help_text='Dearness Allowance (20% of Basic Pay)', verbose_name='DA'),
        ),
        migrations.AlterField(
            model_name='user',
            name='house_rent_allowance',
            field=models.FloatField(default=0.0, help_text='House Rent Allowance,  Section 10(13A) of the Income Tax Act, (20% of Basic Pay)', verbose_name='HRA'),
        ),
        migrations.AlterField(
            model_name='user',
            name='travelling_allowance',
            field=models.FloatField(default=0.0, help_text='Fees and Travelling Allowances Act 1951, (20% of Basic Pay)', verbose_name='TA'),
        ),
    ]
