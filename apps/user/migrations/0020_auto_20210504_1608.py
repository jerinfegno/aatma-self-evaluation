# Generated by Django 3.1.3 on 2021-05-04 10:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0019_auto_20210416_1023'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_hr',
            field=models.BooleanField(default=False, help_text='Do He / she want to access Employee Data?'),
        ),
        migrations.AddField(
            model_name='user',
            name='is_in_business',
            field=models.BooleanField(default=False, help_text='do he handles Business? Accounts, Projects, Clients e.t.c.; '),
        ),
        migrations.AddField(
            model_name='user',
            name='is_in_operations',
            field=models.BooleanField(default=False, help_text='Do he handles Operations? '),
        ),
        migrations.AlterField(
            model_name='user',
            name='is_manager',
            field=models.BooleanField(default=False, help_text='Shows weather payslips have to be generated or not! Includes only Manoj and Anoop.'),
        ),
    ]
