import requests


class ZohoOneAuth(object):
    base_url = 'https://accounts.zoho.com/oauth/v2'
    auth_url = f'{base_url}/auth'
    token_url = f'{base_url}/token'
    client_id = None
    response_type = 'code'
    redirect_uri = ''
    scope = 'AaaServer.profile.READ'

    params = {
        'response_type': 'subscribe',
        'client_id': '1000.68UW3IMZGXT9S4WQUOQ2OC7GCU4UHH',
        'scope': scope,
        'redirect_uri': 'https://fims.fegno.com/auth/zoho-oneauth',
        'prompt': 'concent'
    }

    def __init__(self, ):
        pass

    def request_for_authorization(self):
        response = requests.get(self.auth_url, params=self.params)

    def request_for_token(self):
        pass


z = ZohoOneAuth()
print(z.request_for_authorization())
