
FIELDS = (
    'contact_01', 'contact_01_holder_name', 'contact_02', 'contact_02_holder_name',

    'address_line_1', 'address_line_2', 'address_line_3', 'address_line_4', 'pincode',

    'is_previous_company_verified', 'previous_company_name', 'previous_company_reference_name_and_designation',
    'previous_company_reference_contact_number', 'previous_company_reference_email',

    'is_second_recommendation_verified', 'second_recommendation_name', 'second_recommendation_reference_name_and_designation',
    'second_recommendation_reference_contact_number', 'second_recommendation_reference_email',

    'school_name', 'college_name',
)


def copy_fields(instance1, instance2, fields=FIELDS):
    for field in fields:
        if hasattr(instance1, field) and hasattr(instance2, field):
            setattr(instance2, field, getattr(instance1, field))


def forwards_func(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    User = apps.get_model("user", "User")
    # Profile = apps.get_model("user", "Profile")
    db_alias = schema_editor.connection.alias
    user_qs = User.objects.using(db_alias).all()
    profile_list = []
    Profile = apps.get_model("user", "Profile")
    for user in user_qs:
        profile = Profile(user=user)
        copy_fields(user, profile)
    Profile.objects.using(db_alias).bulk_create(profile_list)


def reverse_func(apps, schema_editor):
    # forwards_func() creates two Country instances,
    # so reverse_func() should delete them.
    User = apps.get_model("user", "User")
    Profile = apps.get_model("user", "Profile")
    db_alias = schema_editor.connection.alias
    all_profiles = Profile.objects.using(db_alias).select_related('user')
    for profile in all_profiles:
        copy_fields(profile, profile.user)
    User.objects.using(db_alias).bulk_update([p.user for p in all_profiles], fields=FIELDS)
    Profile.objects.using(db_alias).delete()
