from typing import Optional

from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin
from django.db import models
from django.template import Context, Template
from django.utils.safestring import mark_safe
from import_export.admin import ExportMixin, ImportExportModelAdmin

from apps.user.models import User, BCard, Profile


class ProfileInline(admin.StackedInline):
    model = Profile


class CustomUserAdmin(ImportExportModelAdmin, UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'email', 'profile_image', 'gender')}),
        ('New Candidate', {'fields': ('is_non_employee_candidate', 'new_candidate_form_is_in_draft_mode')}),
        ('Address', {"fields": ('address_line_1', 'address_line_2', 'address_line_3', 'address_line_4', 'pincode')}),
        ('Contact Points', {"fields": ('official_mobile', 'official_email', 'contact_01', 'contact_01_holder_name', 'contact_02', 'contact_02_holder_name')}),
        ('Organization', {'fields': ('employee_id', 'designation', 'department', 'reporting_to', 'is_manager', 'is_hr', 'is_in_business', 'is_in_operations', 'is_devops',)}),
        ('Salary Payments and Deductions', {'fields': (
            'basic_pay', 'dearness_allowance', 'house_rent_allowance', 'travelling_allowance',
            'provident_fund', 'employee_state_insurance', 'tax_deducted_at_source', 'net_pay',
        )}),
        ('Permissions', {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        ('Important dates', {'fields': ('last_login', 'date_joined', 'dob', 'dom', 'doj', 'dot')}),
        # ('Careers', {'fields': (
        #     'is_previous_company_verified', 'previous_company_name', 'previous_company_reference_name_and_designation', 'previous_company_reference_contact_number', 'previous_company_reference_email',
        #     'is_second_recommendation_verified', 'second_recommendation_name', 'second_recommendation_reference_name_and_designation', 'second_recommendation_reference_contact_number', 'second_recommendation_reference_email',
        # )}),
        # ('Education', {'fields': ('school_name', 'college_name', 'highest_education_degree',)}),
        ('Leave info', {'fields': ('get_current_leave_status',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2'),
        }),
    )
    inlines = [ProfileInline, ]
    def get_readonly_fields(self, request, obj=None):
        return super(CustomUserAdmin, self).get_readonly_fields(request, obj) + ('get_current_leave_status', )

    def get_current_leave_status(self, obj: Optional[User] = None):
        if obj is None:
            return ""
        return obj.get_current_leave_status()

    get_current_leave_status.allow_tags = True
    get_current_leave_status.short_description = "Leave Summery for User"


admin.site.register(User, CustomUserAdmin)
admin.site.register(BCard)






