from django.apps import AppConfig
from django.db.utils import ProgrammingError

class UserConfig(AppConfig):
    name = 'apps.user'

    def ready(self):
        try:
            from django.contrib.auth.models import Group
            Group.objects.get_or_create(name='Employee')
        except ProgrammingError:
            print('bypassed for initial project setup')
