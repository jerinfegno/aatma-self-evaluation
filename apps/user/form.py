from django.forms import ModelForm

from apps.user.models import User


class UserProfileUpdateForm(ModelForm):
    class Meta:
        model = User
        fields = [
            'first_name', 'last_name', 'email',
            'contact_01', 'contact_01_holder_name', 'contact_02', 'contact_02_holder_name',
            'address_line_1', 'address_line_2', 'address_line_3', 'address_line_4', 'pincode']
