from typing import Optional

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import pre_delete, post_save
from django.template import Context, Template
from django.urls import reverse_lazy
from django.utils.datetime_safe import date
from django.utils.functional import cached_property
from django.utils.safestring import mark_safe
from django.utils.text import slugify
from encrypted_model_fields.fields import EncryptedCharField
from phonenumber_field.modelfields import PhoneNumberField
from treebeard.mp_tree import MP_Node

from apps.generic.models.address import AddressAbstract
from apps.generic.models.salary import SalaryAbstractModel
from apps.utils.date import next_occurrence


class BCard(models.Model):
    username = models.CharField(max_length=32)
    image = models.ImageField(upload_to='bcard-profile-image/', null=True, blank=True)
    designation = models.CharField(null=True, max_length=128)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    facebook_handle = models.CharField(max_length=256, null=True, blank=True)
    instagram_handle = models.CharField(max_length=256, null=True, blank=True)
    linkedin_handle = models.CharField(max_length=256, null=True, blank=True)
    twitter_handle = models.CharField(max_length=256, null=True, blank=True)
    github_handle = models.CharField(max_length=256, null=True, blank=True)

    def __str__(self):
        return self.username

    @property
    def avatar_url(self):
        if self.image:
            return self.image.url
        return self.user.avatar_url

    @property
    def slug(self):
        return slugify(self.username)


class Designation(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name

    @property
    def slug(self):
        return slugify(self.name)


class Degree(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name

    @property
    def slug(self):
        return slugify(self.name)


class Department(models.Model):
    name = models.CharField(max_length=32)
    head = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=False, on_delete=models.SET_NULL, related_name='heading_departments')

    @property
    def slug(self):
        return slugify(self.name)

    def __str__(self):
        return self.name
        return self.name


def default_to_primary_user(collector, field, sub_objs, using):
    user = field.model.objects.filter(is_manager=True, is_active=True).order_by('username').first()
    collector.add_field_update(field, user, sub_objs)


class User(SalaryAbstractModel, AddressAbstract, AbstractUser):
    """
    Each user will be an employee
    """
    MALE = "Male"
    FEMALE = "Female"
    TRANS = "Trans"
    RATHER_NOT_TO_SAY = "-"
    employee_id = models.CharField(max_length=12, null=True, unique=True, blank=False)
    dob = models.DateField(null=True, verbose_name="Date of Birth")
    dom = models.DateField(null=True, blank=True, verbose_name="Date of Marriage")
    doj = models.DateField(null=True, verbose_name="Date of Joining")
    dot = models.DateField(null=True, blank=True, verbose_name="Date of Termination")
    profile_image = models.ImageField(null=True, blank=True, verbose_name="Profile Image", upload_to='profile-image/')
    designation = models.ForeignKey('user.Designation', null=True, on_delete=models.SET_NULL, related_name='employees')
    department = models.ForeignKey('user.Department', null=True, on_delete=models.SET_NULL, related_name='employees')
    reporting_to = models.ForeignKey('self', on_delete=default_to_primary_user, null=True, blank=True)
    mobile = PhoneNumberField(null=True, blank=True)
    official_mobile = PhoneNumberField(null=True, blank=True, help_text="Fegno's official, mobile number if exists.")
    official_email = models.EmailField(null=True, blank=True, help_text="Your <username>@fegno.com email address. ")
    is_manager = models.BooleanField(verbose_name="Is Managing Director?", default=False, help_text="Shows weather payslips have to be generated or not! "
                                                                                                    "Includes only Manoj and Anoop.")
    is_in_operations = models.BooleanField(default=False, help_text="Do he handles Operations? ")
    is_in_business = models.BooleanField(default=False, help_text="do he handles Business? Accounts, Projects, "
                                                                  "Clients e.t.c.; ")
    is_hr = models.BooleanField(default=False, help_text="Do He / she want to access Employee Data?")
    is_sop_handler = models.BooleanField(default=False, help_text="Do He / she can manage SOP?")
    is_devops = models.BooleanField(default=False, help_text="Do He / she work as a DevOps?")
    is_non_employee_candidate = models.BooleanField(
        default=False, help_text="If this field is True, a new non employee candidate will get a form "
                                 "to fill and update personal details before joining the firm.")
    new_candidate_form_is_in_draft_mode = models.BooleanField(
        default=False, help_text="If this field is True, a new non employee candidate will get a form "
                                 "to fill and update personal details before joining the firm.")

    gender = models.CharField(choices=[
        (MALE, MALE),
        (FEMALE, FEMALE),
        (TRANS, TRANS),
        (RATHER_NOT_TO_SAY, RATHER_NOT_TO_SAY),
    ], default=RATHER_NOT_TO_SAY, max_length=12)

    def __str__(self):
        return f"{self.get_full_name()} ({self.username})"

    def dob_next_occurrence(self):
        return next_occurrence(self.dob)

    def doj_next_occurrence(self):
        return next_occurrence(self.doj)

    def dom_next_occurrence(self):
        return next_occurrence(self.dom)

    def dob_nth_anniversary(self):
        return date.today().year - self.dob.year

    def doj_nth_anniversary(self):
        return date.today().year - self.doj.year

    def dom_nth_anniversary(self):
        return date.today().year - self.dom.year

    @property
    def avatar_url(self):
        if self.profile_image:
            return self.profile_image.url
        if self.gender == self.FEMALE:
            return settings.DEFAULT_FEMALE_PROFILE_IMAGE
        return settings.DEFAULT_MALE_PROFILE_IMAGE

    def save(self, *args, **kwargs):
        if self.reporting_to == self:
            self.reporting_to = None
        super(User, self).save(*args, **kwargs)

    def get_this_leaves(self):
        pass

    def get_current_leave_status_for_hr(self):
        return self.get_current_leave_status('hr')

    def get_current_leave_status_for_self(self, points_to='admin'):
        return self.get_current_leave_status('self')

    def get_current_leave_status(self, points_to='admin'):
        obj = self
        from apps.leave.models import LeaveRequest
        used_leaves = LeaveRequest.objects.filter(user=obj).values('leave_type')
        approved_leaves_expression = models.Sum(models.Case(models.When(
            approval_status=LeaveRequest.APPROVED, then=models.F('leave_approved_in_days'),
        ), default=0.0,  output_field=models.FloatField()))
        pending_leave_expression = models.Sum(models.Case(models.When(
            approval_status=LeaveRequest.WAITING_FOR_APPROVAL, then=models.F('leave_requested_in_days'),
        ), default=0.0,  output_field=models.FloatField()))
        used_leaves = used_leaves.annotate(
            leave_type_name=models.F('leave_type__name'),
            total_leave=models.F('leave_type__no_of_days'),
            approved_leaves=approved_leaves_expression,
            pending_leaves=pending_leave_expression,
            remaining_leaves=models.ExpressionWrapper(models.F('leave_type__no_of_days') - approved_leaves_expression,
                                                      output_field=models.FloatField()),
        )
        if points_to == 'admin':
            url_template = "{% url 'admin:leave_leaverequest_change' object_id=lr.pk %}"

        elif points_to == 'hr':
            url_template = "{% url 'leave:detail' pk=lr.pk %}"

        else:
            url_template = "{% url 'leave:view' pk=lr.pk %}"

        template_string = Template("""
                    <div class="responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Leave Type</th><th>Total.</th><th>Remaining Days.</th>
                                    <th>Consumed.</th><th>Pending.</th>
                                </tr>
                            </thead>
                            <tbody>
                                {% for leave in used_leaves  %}
                                <tr>
                                    <td>{{ leave.leave_type_name }}</td><td>{{ leave.total_leave }}</td>
                                    <td>{{ leave.remaining_leaves }}</td>
                                    <td>{{ leave.approved_leaves }}</td><td>{{ leave.pending_leaves }}</td>
                                </tr>
                                {% endfor %}
                            </tbody>
                        </table>
                    </div>
                    <div >
                        <b> Resent Requests. </b>
                        <ol>
                           {% for lr in leave_requests %} 
                            <li> 
                                <a href=""" + url_template + """> 
                                    {{ lr.reason_to_request }} - {{ lr.from_date }} to {{ lr.to_date }}
                                </a>
                            </li>
                           {% endfor %}
                        </ol>
                    </ div>

                    </div>
        """)
        leave_requests_ = LeaveRequest.objects.filter(user=obj).exclude(
            approval_status__in=(LeaveRequest.CANCELLED, LeaveRequest.REJECTED)).order_by('-id')[:15]

        context = Context({"used_leaves": used_leaves, 'leave_requests': leave_requests_ })
        return mark_safe(template_string.render(context))

    get_current_leave_status.allow_tags = True
    get_current_leave_status.short_description = "Leave Summery for User"

    @cached_property
    def profile_completion_percentage(self):
        fields = [
            'employee_id', 'dob', 'doj', 'profile_image',  'mobile', 'email',
            'previous_company_name', 'previous_company_reference_name_and_designation', 'previous_company_reference_contact_number', 'previous_company_reference_email',
            'second_recommendation_name', 'second_recommendation_reference_name_and_designation', 'second_recommendation_reference_contact_number', 'second_recommendation_reference_email',
            'school_name', 'college_name', 'highest_education_degree',
            'contact_01_holder_name', 'contact_01',
            'contact_02_holder_name', 'contact_02',
            'address_line_1', 'address_line_2', 'address_line_3', 'address_line_4', 'pincode',
        ]
        score = 0
        from apps.documents.models import UserDocumentType
        net_fields_count = len(fields) + UserDocumentType.objects.filter(is_required_for_new_candidates=True).count()
        hasprofile = hasattr(self, 'profile')
        for _field in fields:
            score += bool(hasattr(self, _field) and getattr(self, _field))
            score += bool(hasprofile and hasattr(self.profile, '_field') and getattr(self, _field))
        score += self.userdocument_set.count()
        return score * 100 // net_fields_count


Employee = User


def no_at_in_front(value):
    if value:
        return value.lstrip('@')
    if '/' in value:
        raise ValidationError('Please do not share the whole url! Just Username / handle is enough!')


class Profile(AddressAbstract, models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    experience_while_joining = models.FloatField(default=0.0)
    is_previous_company_verified = models.BooleanField(default=False)
    previous_company_name = models.CharField(max_length=64, null=True, blank=True)
    previous_company_reference_name_and_designation = models.CharField(max_length=256, null=True, blank=True)
    previous_company_reference_contact_number = PhoneNumberField(null=True, blank=True)
    previous_company_reference_email = PhoneNumberField(null=True, blank=True)

    is_second_recommendation_verified = models.BooleanField(default=False)
    second_recommendation_name = models.CharField(max_length=64, null=True, blank=True)
    second_recommendation_reference_name_and_designation = models.CharField(max_length=256, null=True, blank=True)
    second_recommendation_reference_contact_number = PhoneNumberField(null=True, blank=True)
    second_recommendation_reference_email = PhoneNumberField(null=True, blank=True)

    school_name = models.CharField(max_length=256, null=True, blank=True)
    college_name = models.CharField(max_length=256, null=True, blank=True)
    university = models.CharField(max_length=256, null=True, blank=True)
    degree_course = models.CharField(max_length=256, null=True, blank=True)
    highest_education_degree = models.ForeignKey(Degree, on_delete=models.SET_NULL, null=True, blank=True)
    degree_pass_out_year = models.PositiveIntegerField(default=2000)
    tenth_pass_out_year = models.PositiveIntegerField(default=2000)

    degree_grade = models.CharField(max_length=12, null=True, blank=True)
    tenth_grade = models.CharField(max_length=12, null=True, blank=True)

    previous_annual_ctc = models.FloatField(max_length=256, null=True, blank=True, help_text="Annual Cost to company, excluding deductions.")
    pf_account_number = models.CharField(max_length=256, null=True, blank=True)
    esi_number = models.CharField(max_length=256, null=True, blank=True, help_text="Also known as UAN number")
    bank_account_number = EncryptedCharField(max_length=256, null=True, blank=True, help_text="Trust! This data is encrypted in database. ")
    bank_name = models.CharField(max_length=256, null=True, blank=True, help_text="")
    bank_ifsc_code = models.CharField(max_length=256, null=True, blank=True)

    fegno_salary_bank_account_number = EncryptedCharField(max_length=256, null=True, blank=True, help_text="Trust! This data is encrypted in database. ")
    fegno_salary_bank_name = models.CharField(max_length=256, null=True, blank=True, help_text="")
    fegno_salary_bank_ifsc_code = models.CharField(max_length=256, null=True, blank=True)

    facebook_handle = models.CharField(max_length=256, null=True, blank=True, validators=[no_at_in_front], help_text="like 'fegnotech' instead of 'https://www.facebook.com/fegnotech' ")
    instagram_handle = models.CharField(max_length=256, null=True, blank=True, validators=[no_at_in_front],  help_text="like 'fegnotech' instead of 'https://www.instagram.com/fegnotech' ")
    linkedin_handle = models.CharField(max_length=256, null=True, blank=True, validators=[no_at_in_front], help_text="like 'fegnotechnologies' instead of 'https://www.linkedin.com/in/fegnotechnologies/' ")
    twitter_handle = models.CharField(max_length=256, null=True, blank=True, validators=[no_at_in_front], help_text="like 'fegnot' instead of 'https://www.twitter.com/fegnot' ")
    github_handle = models.CharField(max_length=256, null=True, blank=True, validators=[no_at_in_front], help_text="like 'fegno' instead of 'https://www.hithub.com.com/fegno' ")
    fegno_github_handle = models.CharField(max_length=256, null=True, blank=True, validators=[no_at_in_front], help_text="like 'fegno' instead of 'https://www.hithub.com.com/fegno' ")

    @property
    def facebook_url(self):
        if self.facebook_handle:
            return f'https://www.facebook.com/{self.facebook_handle}'

    @property
    def instagram_url(self):
        if self.instagram_handle:
            return f'https://www.facebook.com/{self.instagram_handle}/'

    @property
    def linkedin_url(self):
        if self.linkedin_handle:
            return f'https://www.linkedin.com/in/{self.linkedin_handle}/'

    @property
    def twitter_url(self):
        if self.twitter_handle:
            return f'https://www.twitter.com/{self.twitter_handle}/'

    @property
    def github_url(self):
        if self.github_handle:
            return f'https://www.github.com/{self.github_handle}/'

    @property
    def fegno_github_url(self):
        if self.fegno_github_handle:
            return f'https://www.github.com/{self.fegno_github_handle}/'


class OrganizationTree(MP_Node):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='organization_instance')


def get_org_instance(user: User) -> Optional[OrganizationTree]:
    if not user:
        return None
    if not hasattr(user, 'organization_instance'):
        reporting_user: Optional[User] = user.reporting_to
        if reporting_user:
            return get_org_instance(reporting_user).add_child(user=user)
        else:
            return OrganizationTree.add_root(user=user)
    return user.organization_instance


def generate_profile(sender: type, instance: User, created: bool, **kwargs: dict):
    if created:
        Profile.objects.get_or_create(user=instance)


def generate_reporting_manager(sender: type, instance: User, created: bool, **kwargs: dict):
    """ Root Managers either point themself to them or they wont have a mapping at all.  """
    # variable org_... stands for OrganizationTree Instance
    # variable ..._rm stands for reporting_manager

    is_root = not instance.reporting_to or instance.reporting_to == instance
    if created:
        if is_root:
            """ Added as root """
            return OrganizationTree.add_root(user=instance)
        else:
            """ Added as child """
            org_instance: Optional[OrganizationTree] = get_org_instance(instance)
            org_rm = get_org_instance(instance.reporting_to)
            org_instance.move(org_rm, pos='last-child')
    else:
        org_instance: Optional[OrganizationTree] = get_org_instance(instance)
        if is_root:
            """ Move to root """
            org_instance.move(org_instance.get_root(), pos='last-sibling')
        else:
            """ Move as child """
            org_rm = get_org_instance(instance.reporting_to)
            org_instance.move(org_rm, pos='last-sibling')


def remap_members_to_parent(sender: type, instance: OrganizationTree, **kwargs: dict):
    if instance.numchild > 0:
        if instance.is_root():
            new_reporting_manager = OrganizationTree.get_root_nodes().exclude(pk=instance.pk).order_by('user_id').first()
            if not new_reporting_manager:
                raise Exception("Cannot remove root user.")
        else:
            new_reporting_manager = instance.get_parent()
        for child in instance.get_children():
            child.move(target=new_reporting_manager, pos='last-child')


pre_delete.connect(remap_members_to_parent, sender=OrganizationTree)
post_save.connect(generate_reporting_manager, sender=User)
post_save.connect(generate_profile, sender=User)

