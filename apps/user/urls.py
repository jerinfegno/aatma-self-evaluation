from django.conf import settings
from django.urls import path, include, reverse


from apps.user.api import UserAPIView, QRCodeView, scanned_login
from apps.user.views import UserProfileUpdateView

app_name = "user"


api_urlpatterns = [
    path('user-profile/', UserAPIView.as_view(), name="user-profile"),
    path('qr_code/', QRCodeView.as_view(), name="qrcode"),
    path('login-with-scan/', scanned_login, name="scanned_login"),

]


urlpatterns = [
    path('api/v1/', include(api_urlpatterns)),
    path('user-profile-update/<int:pk>/', UserProfileUpdateView.as_view(), name='user_profile_update'),
]

