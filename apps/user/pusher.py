from django.conf import settings
import pusher


class AuthPush(object):

    def push_token(self, token, uuid, username):
        # import pdb;pdb.set_trace()
        login_token_obj = token
        _uuid_data = uuid
        payload = {
            'token': login_token_obj.key,
            'username': username
        }

        pusher_client = pusher.Pusher(
            app_id=u'{}'.format(settings.PUSHER_APP_ID),
            key=u'{}'.format(settings.PUSHER_APP_KEY),
            secret=u'{}'.format(settings.PUSHER_SECRET),
            cluster=u'{}'.format(settings.PUSHER_CLUSTER),

        )

        # pusher = requests.url("http://api-CLUSTER.pusher.com")
        _id = str(_uuid_data)
        event = settings.PUSHER_LOGIN_EVENT_NAME.format(_id)

        try:
            pusher_client.trigger(settings.PUSHER_LOGIN_CHANNEL_NAME, event, payload)
        except Exception as e:
            print(e)
        print(settings.PUSHER_LOGIN_CHANNEL_NAME, event, payload)
        print(pusher_client.channels_info(settings.PUSHER_LOGIN_CHANNEL_NAME))

