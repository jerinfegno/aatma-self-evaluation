from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.user.models import Designation, Department


class QRCodeSerialzier(serializers.Serializer):
    uuid = serializers.UUIDField()


class UserSerializer(serializers.ModelSerializer):
    designation = serializers.SlugRelatedField(slug_field='name', queryset=Designation.objects.all())
    department = serializers.SlugRelatedField(slug_field='name', queryset=Department.objects.all())
    reporting_to = serializers.SlugRelatedField(slug_field='first_name', queryset=get_user_model().objects.all())

    class Meta:
        model = get_user_model()
        fields = (
            'first_name', 'last_name', 'employee_id', 'email', 'mobile',
            'dob', 'dom', 'doj', 'dot',
            'profile_image', 'designation', 'department',
            'reporting_to', 'mobile', 'gender',
            'is_manager', 'is_in_operations', 'is_in_business', 'is_hr', 'is_devops',
        )
        read_only_fields = (
            'dob', 'dom', 'doj', 'dot', 'designation', 'department', 'email',
            'gender', 'reporting_to', 'is_in_business',
            'is_manager', 'is_in_operations', 'is_hr', 'is_devops',)
