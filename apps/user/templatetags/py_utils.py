from django import template


register = template.Library()


@register.simple_tag
def getattribute(obj, *attrs):
    for attr in attrs:
        if type(obj) is dict:
            obj = obj.get(attr)
        else:
            obj = getattr(obj, attr)
    return obj


@register.filter
def is_consumed_restricted_leave(leave, user):
    return False
    # return leave.is_consumed_by(user)


@register.simple_tag()
def no_of_pending_leaves():
    from apps.leave.models import LeaveRequest
    return LeaveRequest.objects.all().filter(approval_status=LeaveRequest.WAITING_FOR_APPROVAL).count()

