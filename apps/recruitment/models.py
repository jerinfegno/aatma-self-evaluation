from tinymce.models import HTMLField
from solo.models import SingletonModel


class RecruitmentStory(SingletonModel):
    content = HTMLField()

    def __str__(self):
        return "RecruitmentStory"

