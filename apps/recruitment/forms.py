from django import forms
from django.contrib.auth import get_user_model

from apps.user.models import User, Profile
from apps.utils.forms import BootstrapModelForm


class CandidateProfileForm(BootstrapModelForm):

    class Meta:
        fields = [
            'first_name', 'last_name', 'gender',
            'dob', 'dom', 'profile_image',
            'mobile', 'email',
        ]
        model = get_user_model()

    def __init__(self, *args, **kwargs):
        super(CandidateProfileForm, self).__init__( *args, **kwargs)
        self.fields['dob'].widget = forms.DateInput(format="%d-%m-%Y")
        self.fields['dom'].widget = forms.DateInput(format="%d-%m-%Y")


class CandidateProfileUpdateForm(BootstrapModelForm):

    class Meta:
        fields = [
            'experience_while_joining',
            'previous_company_name', 'previous_company_reference_name_and_designation',
            'previous_company_reference_contact_number',
            'previous_company_reference_email',
            'second_recommendation_name', 'second_recommendation_reference_name_and_designation',
            'second_recommendation_reference_contact_number',
            'second_recommendation_reference_email',

            'school_name', 'tenth_pass_out_year', 'tenth_grade',

            'college_name', 'highest_education_degree',
            'university', 'degree_course', 'degree_pass_out_year',
             'degree_grade',

            'previous_annual_ctc', 'pf_account_number', 'esi_number',
            'bank_account_number', 'bank_name', 'bank_ifsc_code',

            'contact_01', 'contact_01_holder_name',
            'contact_02', 'contact_02_holder_name',

            'address_line_1',
            'address_line_2',
            'address_line_3',
            'address_line_4',
            'pincode',
        ]
        model = Profile

    def __init__(self, *args, **kwargs):
        super(CandidateProfileUpdateForm, self).__init__( *args, **kwargs)
        # self.fields['dob'].widget = forms.DateInput(format="%d-%m-%Y")
        # self.fields['dom'].widget = forms.DateInput(format="%d-%m-%Y")


