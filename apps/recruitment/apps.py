from django.apps import AppConfig


class RecruitmentConfig(AppConfig):
    name = 'apps.recruitment'
