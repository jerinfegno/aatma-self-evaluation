from solo.admin import SingletonModelAdmin
from django.contrib import admin
from apps.recruitment.models import RecruitmentStory


class ChangedSingletonModelAdmin(SingletonModelAdmin):
    pass


admin.site.register(RecruitmentStory, ChangedSingletonModelAdmin)


