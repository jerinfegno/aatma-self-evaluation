{% extends 'leave/email-templates/email-base.txt' %}

{% block main_content %}
Hello {{ leave_request.user.get_full_name }},

You have ben charged an LOP due to the late reporting of working time on {{ leave_request.lateattendance.date|date }}.

Reason: {{ leave_request.lateattendance.reason }}

{% endblock %}

