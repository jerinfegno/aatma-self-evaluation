{% extends 'leave/email-templates/email-base.txt' %}

{% block main_content %}
    {{ leave_request.user.get_full_name }}[EMP ID: {{ leave_request.user.employee_id }}] has cancelled his {{ leave_request.leave_type.name }} request which was applied for {{ leave_request.leave_requested_in_days }} days from {{ leave_request.date_as_string }}.
{% endblock %}

