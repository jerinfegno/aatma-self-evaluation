{% extends 'leave/email-templates/wfh-email-base.txt' %}

{% block main_content %}
Hello {{ leave_request.user.get_full_name }},

Your Work From Home request for {{ leave_request.date_as_string }} days has been REJECTED by {{ leave_request.approved_by.get_full_name }}.
{% endblock %}

