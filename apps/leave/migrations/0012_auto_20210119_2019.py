# Generated by Django 3.1.3 on 2021-01-19 14:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('leave', '0011_auto_20210104_1810'),
    ]

    operations = [
        migrations.CreateModel(
            name='RestrictedHolidayGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('description', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='holiday',
            name='restricted_group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='leave.restrictedholidaygroup'),
        ),
    ]
