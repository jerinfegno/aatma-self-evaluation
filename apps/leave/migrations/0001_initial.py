# Generated by Django 3.1.3 on 2020-11-27 09:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='LeaveType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32)),
                ('allocation_type', models.CharField(choices=[('PRE ALLOCATED', 'PRE ALLOCATED'), ('MONTHLY ALLOCATED', 'MONTHLY ALLOCATED')], max_length=32)),
                ('no_of_days', models.PositiveSmallIntegerField(help_text='If monthly allocatable, it must be multiple of 6.')),
                ('payment_type', models.CharField(choices=[('PAID', 'PAID'), ('LOSS OF PAY', 'LOSS OF PAY')], default='LOSS OF PAY', max_length=32)),
                ('description', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='LeaveRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('from_date', models.DateField()),
                ('from_slot', models.CharField(choices=[('Morning', 'Morning'), ('Evening', 'Evening')], default='Morning', max_length=24)),
                ('reason', models.DateField()),
                ('leave_type', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='leave.leavetype')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
