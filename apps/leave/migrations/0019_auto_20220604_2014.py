# Generated by Django 3.1.3 on 2022-06-04 14:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('leave', '0018_auto_20211001_1202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='leaverequest',
            name='leave_approved_in_days',
            field=models.FloatField(default=0.0, help_text='Number of days without Loss Of Pay.', verbose_name='No. of Paid Leaves'),
        ),
        migrations.AlterField(
            model_name='leaverequest',
            name='leave_lop_in_days',
            field=models.FloatField(default=0.0, help_text='Number of days with Loss Of Pay ', verbose_name='No. of Loss of Pay'),
        ),
    ]
