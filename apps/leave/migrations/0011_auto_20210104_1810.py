# Generated by Django 3.1.3 on 2021-01-04 12:40

import apps.leave.models
from django.db import migrations, models
import django.utils.datetime_safe


class Migration(migrations.Migration):

    dependencies = [
        ('leave', '0010_auto_20201230_0956'),
    ]

    operations = [
        migrations.AddField(
            model_name='leaverequest',
            name='leave_lop_in_days',
            field=models.FloatField(default=0.0, help_text='Number of days with Loss Of Pay '),
        ),
        migrations.AlterField(
            model_name='leaverequest',
            name='from_date',
            field=models.DateField(default=django.utils.datetime_safe.date.today, validators=[apps.leave.models.is_working_day]),
        ),
        migrations.AlterField(
            model_name='leaverequest',
            name='leave_approved_in_days',
            field=models.FloatField(default=0.0, help_text='Number of days without Loss Of Pay.'),
        ),
        migrations.AlterField(
            model_name='leaverequest',
            name='leave_requested_in_days',
            field=models.FloatField(default=0.0, help_text='No of days, Leave Requested.'),
        ),
        migrations.AlterField(
            model_name='leaverequest',
            name='to_date',
            field=models.DateField(default=django.utils.datetime_safe.date.today, validators=[apps.leave.models.is_working_day]),
        ),
    ]
