from datetime import timedelta

from django.db.models import F, Sum, FloatField, ExpressionWrapper
from django.utils.functional import cached_property
from apps.leave.models import Holiday, WorkingDay, LeaveType, LeaveRequest


class LeaveTypeValidation(object):

    def __init__(self, leave_request):
        self.from_date = leave_request.from_date
        self.to_date = leave_request.to_date
        self.leave_type = None
        self.days = None
        self.from_slot = leave_request.from_slot
        self.to_slot = leave_request.to_slot

    def validate(self, leave_type, days):

        self.leave_type = leave_type
        self.days = days
        all_leave_types = LeaveType.objects.all()

        if self.leave_type in all_leave_types:
            if self.to_slot == LeaveRequest.TO_MORNING and self.days < 1:
                data = {
                    "leave_type": self.leave_type,
                    "leave_requested_in_days": self.days,
                    "is_halfday": True

                }
            elif self.from_slot == LeaveRequest.FROM_AFTERNOON and self.days < 1:
                data = {
                    "leave_type": self.leave_type,
                    "leave_requested_in_days": self.days,
                    "is_halfday": True

                }
            else:
                data = {
                    "leave_type": self.leave_type,
                    "leave_requested_in_days": self.days,
                    "is_halfday": False

                }
            return data


class AbstractLeaveCalculator(object):
    """
        You can create multiple leave policies such as sandwich leave policy, regular leave policy,
        weekend leave policy e.t.c. policies like sandwich leave policy needs to be applied conditionally only. They
        can be listed with higher priority. Probably, "Regular Leave Policy" will be default and might be the one
        to be at the bottom of the list.

        Usage:-
                leave_policy = AbstractLeaveCalculator(<apps.leave.models.LeaveRequest>)
                if leave_policy.is_applicable:
                    leave_policy.working_days()
    """

    def __init__(self, leave_request):
        self.start_date = leave_request.from_date
        self.end_date = leave_request.to_date
        self.user = leave_request.user
        self.leave_type = leave_request.leave_type
        self.from_slot = leave_request.from_slot
        self.to_slot = leave_request.to_slot
        self.date = leave_request.created_at
    weekdays = [
        'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'
    ]

    __holidays_in_range = None
    __extra_working_days_in_range = None

    @cached_property
    def no_of_days(self):
        return (self.end_date - self.start_date).days + 1

    @property
    def is_applicable(self):
        """
        Is applicable when checked from middleware. if applicable, this leave will be considered and will be ignored.
        """
        raise NotImplementedError(f'You have to implement "is_applicable" property to {self.__class__.__name__}')

    def holidays_in_range(self):
        if self.__holidays_in_range is None:
            self.__holidays_in_range = set(Holiday.objects.filter(
                date__gte=self.start_date, date__lte=self.end_date).values_list('date', flat=True))
        return self.__holidays_in_range

    def extra_working_days_in_range(self):
        if self.__extra_working_days_in_range is None:
            self.__extra_working_days_in_range = set(WorkingDay.objects.filter(
                date__gte=self.start_date, date__lte=self.end_date).values_list('date', flat=True))
        return self.__extra_working_days_in_range

    def working_days(self) -> set:
        raise NotImplementedError(f"implement method 'working_days() -> set' when you use <{self.__class__.__name__}>")

    @classmethod
    def leaves_left(cls, user) -> dict:
        return LeaveRequest.objects.filter(
            approval_status=LeaveRequest.APPROVED, user=user
        ).values('leave_type__name').annotate(
            consumable=F('leave_type__no_of_days'),
            consumed=Sum('leave_approved_in_days'),
            lop=Sum('leave_lop_in_days'),
            left=ExpressionWrapper(F('leave_type__no_of_days')-Sum('leave_approved_in_days'), output_field=FloatField())
        )


class RegularLeaveCalculator(AbstractLeaveCalculator):

    @property
    def is_applicable(self):
        # return self.no_of_days <= 2
        return True

    def working_days(self) -> set:
        _working_days = []
        holidays = self.holidays_in_range()
        extra_working_days = self.extra_working_days_in_range()

        for day_delta in range(0, self.no_of_days):
            date = self.start_date + timedelta(days=day_delta)
            if date.strftime('%A') in self.weekdays and date not in holidays:
                _working_days.append(date)
            if date in extra_working_days:
                _working_days.append(date)
        return set(_working_days)


class SandWichLeaveCalculator(AbstractLeaveCalculator):

    @property
    def is_applicable(self):
        """ not implemented. and should not be called."""
        return False


"""
    Keep this in ascending order or priority. First 'applicable leave policy will be considered first.
"""
applicators = [
    SandWichLeaveCalculator,
    RegularLeaveCalculator
]


class LeaveApplicator(object):

    leave_days = None

    def __init__(self, leave_request):
        self.leave_request = leave_request

    def get_leave_days(self) -> set:
        if self.leave_days is None:
            for leave_policy_applicator in applicators:
                leave_policy = leave_policy_applicator(self.leave_request)
                if leave_policy.is_applicable:
                    self.leave_days = leave_policy.working_days()
                    break
        return self.leave_days

    def get_leave_count(self) -> float:
        if not self.leave_days:
            self.leave_days = self.get_leave_days()
        days = self.leave_days and len(self.leave_days) or 0
        if self.leave_request.from_slot == self.leave_request.FROM_AFTERNOON and self.leave_request.from_date in self.leave_days:
            days -= 0.5
        if self.leave_request.to_slot == self.leave_request.TO_MORNING and self.leave_request.to_date in self.leave_days:
            days -= 0.5
        return max(days, 0)



