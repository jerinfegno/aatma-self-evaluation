from django.db.models import F, When, Case, Q, Sum
from rest_framework import serializers

from apps.leave.models import LeaveRequest, Holiday


class LeaveRequestSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
    )

    def validate(self, attrs):
        lr = LeaveRequest(**attrs)

        LeaveRequest.is_able_to_take_leave(
            leave_approved_in_days=lr.leave_applicator.get_leave_count(),
            **attrs, error=serializers.ValidationError, instance=self.instance,
        )
        return attrs

    class Meta:
        model = LeaveRequest
        fields = (
            'id', 'leave_type', 'user', 'reason_to_request', 'from_date', 'to_date', 'from_slot', 'to_slot',
        )


class HolidaySerializer(serializers.ModelSerializer):

    class Meta:
        model = Holiday
        fields = (
            'date', 'name', 'description', 'is_restricted_holiday', 'restricted_group'
        )


class LeaveRequestDisplaySerializer(serializers.ModelSerializer):
    approved_by = serializers.SerializerMethodField()
    leave_type_name = serializers.SerializerMethodField()

    def get_approved_by(self, instance):
        return instance.approved_by and instance.approved_by.get_full_name()

    def get_leave_type_name(self, instance):
        return instance.leave_type and instance.leave_type.name

    class Meta:
        model = LeaveRequest
        fields = (
            'id', 'leave_type', 'leave_type_name', 'reason_to_request',
            'from_date', 'to_date', 'from_slot', 'to_slot',
            'approval_status', 'approved_by', 'reason_for_action', 'created_at',
            'leave_requested_in_days', 'leave_approved_in_days', 'leave_lop_in_days',
        )



