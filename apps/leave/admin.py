from datetime import timedelta
from functools import lru_cache
from typing import Optional

from django.contrib import admin
from django.template import Template, Context
from django.utils.safestring import mark_safe
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ImportExportMixin
from import_export.fields import Field

from apps.leave.models import *
from datetime import date

class LeaveTypeAdmin(ImportExportModelAdmin):
    list_display = ('name', 'no_of_days', 'payment_type', 'from_date', 'to_date')


class RestrictedHolidayGroupAdmin(ImportExportModelAdmin):
    list_display = ('__str__', 'name',)


class HolidayAdmin(ImportExportModelAdmin):
    list_display = ('__str__', 'name', 'is_restricted_holiday', 'restricted_group')

    def get_queryset(self, request):
        qs = super(HolidayAdmin, self).get_queryset(request)
        return qs.filter(date__year=date.today().year)



class WorkingDayAdmin(ImportExportModelAdmin):
    list_display = ('__str__', 'name', 'description')


class LeaveRequestResource(resources.ModelResource):
    leave_type = Field()
    employee = Field()
    emp_id = Field()
    from_date = Field()
    from_slot = Field()
    to_date = Field()
    to_slot = Field()
    approved_by = Field()

    def dehydrate_leave_type(self, leave):
        return leave.leave_type and leave.leave_type.name

    def dehydrate_employee(self, leave):
        return leave.user and leave.user.get_full_name()

    def dehydrate_approved_by(self, leave):
        return leave.approved_by and leave.approved_by.get_full_name()

    def dehydrate_emp_id(self, leave):
        return leave.user and leave.user.employee_id

    def dehydrate_from_date(self, leave):
        return leave.from_date.strftime("%d %b, %Y")

    def dehydrate_to_date(self, leave):
        return leave.to_date.strftime("%d %b, %Y")

    def dehydrate_from_slot(self, leave):
        return leave.from_slot_name

    def dehydrate_to_slot(self, leave):
        return leave.to_slot_name

    class Meta:
        model = LeaveRequest
        fields = (
            'id', 'leave_type', 'employee', 'emp_id', 'reason_to_request',
            'from_date', 'from_slot', 'to_date', 'to_slot',
            'approval_status', 'approved_by', 'reason_for_action',
            'leave_requested_in_days', 'leave_approved_in_days', 'leave_lop_in_days'
        )


class LeaveRequestAdmin(ImportExportModelAdmin):
    resource_class = LeaveRequestResource
    list_display = ('user', 'leave_type', 'get_date_as_string', 'get_approval_status',
                    'get_days', )
    list_filter = ('user', 'leave_type', 'approval_status')
    readonly_fields = [
        'leave_requested_in_days', 'get_current_leave_status', 'reason_to_request', 'leave_type',
        'created_at', 'approved_by',
    ]
    date_hierarchy = 'created_at'

    def get_queryset(self, request):
        qs = super(LeaveRequestAdmin, self).get_queryset(request)
        qs = qs.select_related('user', 'leave_type', )
        return qs

    def get_readonly_fields(self, request, obj: Optional[LeaveRequest] = None):
        fields_before_approval = ['reason_for_action', 'from_date', 'from_slot', 'to_date', 'to_slot']
        other_fields = ['approval_status', 'leave_approved_in_days', 'leave_lop_in_days']
        conditional_fields = ['get_period'] if obj and obj.pk else []

        if obj and obj.approval_status == obj.SYSTEM_GENERATED:
            return [*self.readonly_fields, *fields_before_approval, *other_fields, ] + conditional_fields

        if obj and obj.approval_status in obj.FINALIZED_LEAVE_STATUS:
            return [*self.readonly_fields, *fields_before_approval] + conditional_fields
        return self.readonly_fields + conditional_fields

    def has_delete_permission(self, request, obj=None):
        return False

    def get_date_as_string(self, obj: Optional[LeaveRequest] = None):
        return mark_safe(obj.date_as_string + f'<br/><b>REASON : <b/> {obj.reason_to_request}')

    get_date_as_string.short_description = "Summery"

    def get_approval_status(self, obj: Optional[LeaveRequest] = None):
        return mark_safe(obj.approval_status + f'<br/><b>HR : <b/> {obj.reason_for_action}')

    def get_days(self, obj: Optional[LeaveRequest] = None):
        if not obj or not obj.pk:
            return '-'
        return mark_safe(f"""
        Requested: {obj.leave_requested_in_days} <br/>
        Leaves Approved: {obj.leave_approved_in_days} <br/>
        LOP: {obj.leave_lop_in_days} """)

    @lru_cache()
    def get_period(self, obj: Optional[LeaveRequest] = None):
        if not obj or not obj.pk:
            return '-'
        days = []
        set_days_ordered = sorted(obj.find_days or ())
        is_single_day = obj.to_date == obj.from_date
        if is_single_day:
            is_half_day = obj.to_slot == obj.TO_MORNING or obj.from_slot == obj.FROM_AFTERNOON
            return obj.from_date.strftime("%d, %b") + 'Half Day' if is_half_day else 'Full Day'

        for dt in set_days_ordered:
            if obj.from_date == dt:
                is_half_day = obj.to_slot == obj.TO_MORNING or obj.from_slot == obj.FROM_AFTERNOON
                frm_dt = obj.from_date.strftime("%d, %b") + (' Half Day' if is_half_day else ' Full day')
                days.append(frm_dt)
            elif obj.to_date == dt:
                is_half_day = obj.to_slot == obj.TO_MORNING or obj.from_slot == obj.FROM_AFTERNOON
                to_dt = obj.to_date.strftime("%d, %b") + (' Half Day' if is_half_day else ' Full day')
                days.append(to_dt)
            else:
                days.append(dt.strftime("%d, %b"))
        return mark_safe("<br />".join(days))

    get_period.short_description = "Period of leave"

    def get_current_leave_status(self, obj=None):

        if obj is None:
            return ""

        used_leaves = LeaveRequest.objects.filter(user=obj.user).values('leave_type')
        approved_leaves_expression = models.Sum(models.Case(models.When(
            approval_status=LeaveRequest.APPROVED, then=models.F('leave_approved_in_days'),
        ), default=0.0, output_field=models.FloatField()))
        approved_leaves_lop_expression = models.Sum(models.Case(models.When(
            approval_status=LeaveRequest.APPROVED, then=models.F('leave_lop_in_days'),
        ), default=0.0, output_field=models.FloatField()))
        pending_leave_expression = models.Sum(models.Case(models.When(
            approval_status=LeaveRequest.WAITING_FOR_APPROVAL, then=models.F('leave_requested_in_days'),
        ), default=0.0, output_field=models.FloatField()))
        used_leaves = used_leaves.annotate(
            leave_type_name=models.F('leave_type__name'),
            total_leave=models.F('leave_type__no_of_days'),
            approved_leaves=approved_leaves_expression,
            approved_leaves_with_lop=approved_leaves_lop_expression,
            pending_leaves=pending_leave_expression,
            remaining_leaves=models.ExpressionWrapper(models.F('leave_type__no_of_days') - approved_leaves_expression,
                                                      output_field=models.FloatField()),
        )
        template_string = Template("""
                    <div class="responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Leave Type</th><th>Total.</th><th>Remaining Days.</th>
                                    <th>Consumed.</th><th>LOP.</th><th>Pending.</th>
                                </tr>
                            </thead>
                            <tbody>
                                {% for leave in used_leaves  %}
                                <tr>
                                    <td>{{ leave.leave_type_name }}</td><td>{{ leave.total_leave }}</td>
                                    <td>{{ leave.remaining_leaves }}</td>
                                    <td>{{ leave.approved_leaves }}</td><td>{{ leave.approved_leaves_with_lop }}</td>
                                    <td>{{ leave.pending_leaves }}</td>
                                </tr>
                                {% endfor %}
                            </tbody>
                        </table>
                    </div>
        """)
        context = Context({"used_leaves": used_leaves})
        return mark_safe(template_string.render(context))

    get_current_leave_status.allow_tags = True
    get_current_leave_status.short_description = "Leave Summery for User"

    def save_form(self, request, form, change):
        form.instance.approved_by = request.user

        return form.save(commit=False)


admin.site.register(LeaveType, LeaveTypeAdmin)
admin.site.register(Holiday, HolidayAdmin)
admin.site.register(RestrictedHolidayGroup, RestrictedHolidayGroupAdmin)
admin.site.register(WorkingDay, WorkingDayAdmin)
admin.site.register(LeaveRequest, LeaveRequestAdmin)
admin.site.register(LateAttendance, ImportExportModelAdmin)



