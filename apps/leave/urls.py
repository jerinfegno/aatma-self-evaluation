from django.urls import path, include

from apps.leave.views import *

app_name = 'leave'


urlpatterns = [
    path('leaves/', include([
        path('', LeaveRequestListView.as_view(), name="list"),
        path('<int:pk>/view/', LeaveRequestDetailView.as_view(), name="view"),
        path('holidays/', HolidayListView.as_view(), name="holiday-list"),
        path('monthly-user-leaves/', UserLeaveView.as_view(), name="user-leave"),
        path('annual-user-leaves/', UserAnnualLeaveView.as_view(), name="annual-user-leave"),
        path('new-requests/', NewLeaveRequestsListView.as_view(), name="new-leave-requests"),
        path('new-requests/<int:pk>/modify/', NewLeaveRequestsDetailView.as_view(), name="detail"),

    ])),
    path('wfh/', include([
        path('', WFHRequestListView.as_view(), name="wfh-list"),

        path('requests/', WFHListView.as_view(), name="wfh-hr-list"),
        path('requests/<int:pk>/approve/', WFHUpdateView.as_view(), name="wfh-hr-update"),
    ])),
    path('late-attendance/', include([
        path('', LateAttendanceListView.as_view(), name="late-attendance-list"),
    ])),
]


