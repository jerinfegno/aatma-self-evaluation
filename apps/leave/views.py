import pdb
from collections import defaultdict

from django import forms
from django.contrib import messages
from django.db.models import Sum, Case, When, F, FloatField, ExpressionWrapper, Q
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.datetime_safe import date
from django.utils.safestring import mark_safe
from django.views.generic import ListView, ArchiveIndexView, TemplateView, DetailView
from django.views.generic.edit import ModelFormMixin, UpdateView

from apps.leave.forms import LeaveAddForm, LeaveRequestUpdaterAdvanced, LeaveRequestUpdater, WFHAddForm
from apps.leave.models import LeaveRequest, LeaveType, Holiday, RestrictedHolidayGroup, LateAttendance, WFHRequest
from apps.user.models import User
from apps.utils.permissions import HRRequired
import datetime


class HolidayListView(ListView):
    model = Holiday
    # template_name = 'leave/holiday_list.html'

    def get_queryset(self):
        today = date.today()
        return super(HolidayListView, self).get_queryset().filter(date__year=today.year).select_related('restricted_group').order_by('date')

    def get_context_data(self, *, object_list=None, **kwargs):
        kwargs['holiday_groups'] = RestrictedHolidayGroup.objects.all()
        kwargs['yr'] = date.today().year
        return super(HolidayListView, self).get_context_data(object_list=object_list, **kwargs)


class LeaveRequestListView(ModelFormMixin, ListView):
    model = LeaveRequest
    # template_name = 'leave/leaverequest_list.html'
    object = None
    form_class = LeaveAddForm
    paginate_by = 40

    def get_success_url(self):
        return reverse('leave:list')

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        if hasattr(self, 'object'):
            kwargs.update({'instance': self.object})
        kwargs.update({'request': self.request})
        return kwargs

    def get_queryset(self):
        year = date.today().year
        return self.model.objects.filter(user=self.request.user, leave_type__from_date__year=year).order_by('-id')

    def get_context_data(self, **kwargs):
        self.object_list = self.get_queryset()
        year = date.today().year
        used_leaves = LeaveRequest.objects.filter(user=self.request.user, leave_type__from_date__year=year).values('leave_type')
        approved_leaves_expression = Sum(Case(When(
            approval_status=LeaveRequest.APPROVED, then=F('leave_approved_in_days'),
        ), default=0.0,  output_field=FloatField()))
        pending_leave_expression = Sum(Case(When(
            approval_status=LeaveRequest.WAITING_FOR_APPROVAL, then=F('leave_requested_in_days'),
        ), default=0.0,  output_field=FloatField()))
        used_leaves = used_leaves.annotate(
            leave_type_name=F('leave_type__name'),
            total_leave=F('leave_type__no_of_days'),
            approved_leaves=approved_leaves_expression,
            pending_leaves=pending_leave_expression,
            remaining_leaves=ExpressionWrapper(F('leave_type__no_of_days') - approved_leaves_expression, output_field=FloatField()),
        )
        lrs = [l for l in used_leaves]
        lt_set = [{
            'leave_type': lt.id,
            'leave_type_name': lt.name,
            'total_leave': lt.no_of_days,
            'approved_leaves': 0.0,
            'pending_leaves': 0.0,
            'remaining_leaves': lt.no_of_days,
        } for lt in LeaveType.objects.exclude(id__in=[l['leave_type'] for l in lrs]).filter(from_date__year=year)]

        kwargs['used_leaves'] = lrs + lt_set
        return super(LeaveRequestListView, self).get_context_data(now=timezone.now(), **kwargs)

    def get_form_kwargs(self):
        context = super().get_form_kwargs()
        context['request'] = self.request
        return context

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and thclass LeaveRequestListView(ModelFormMixin, ListView):
            model = LeaveRequest
            # template_name = 'leave/leaverequest_list.html'
            object = None
            form_class = LeaveAddForm
            paginate_by = 40
        en check if it's valid.
        """
        form = self.get_form()
        # import pdb;pdb.set_trace()
        if form.is_valid():
            messages.success(request, "Your leave request is forwarded! Please Wait for reply")
            return self.form_valid(form)
        else:
            messages.error(request, "Please Check the form!")
            return self.form_invalid(form)

    # PUT is a valid HTTP verb for creating (with a known URL) or editing an
    # object, note that browsers only support POST for now.
    def put(self, *args, **kwargs):
        return self.post(*args, **kwargs)


class WFHListView(HRRequired, ListView):
    model = WFHRequest
    paginate_by = 40
    template_name = 'leave/hr/whfrequest_list.html'

    def get_queryset(self):
        return self.model.objects.filter().order_by('-id')


class WFHUpdateView(HRRequired, UpdateView):
    model = WFHRequest
    paginate_by = 40
    template_name = 'leave/hr/whfrequest_form.html'
    fields = ('approval_status', 'reason_for_action')
    success_url = reverse_lazy("leave:wfh-hr-list")

    def get_context_data(self, **kwargs):
        kwargs['object_list'] = self.get_queryset()
        return super(WFHUpdateView, self).get_context_data(**kwargs)

    def get_queryset(self):
        return self.model.objects.filter().order_by('-id')

    def form_valid(self, form):
        form.instance.approved_by = self.request.user
        messages.success(self.request, "Work From Home Request Status has been updated!")
        return super(WFHUpdateView, self).form_valid(form)


class WFHRequestListView(ModelFormMixin, ListView):
    model = WFHRequest
    # template_name = 'leave/whfrequest_list.html'
    object = None
    form_class = WFHAddForm
    paginate_by = 40

    def get_success_url(self):
        return reverse('leave:wfh-list')

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        if hasattr(self, 'object'):
            kwargs.update({'instance': self.object})
        kwargs.update({'request': self.request})
        return kwargs

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user).order_by('-id')

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and thclass LeaveRequestListView(ModelFormMixin, ListView):
            model = LeaveRequest
            # template_name = 'leave/leaverequest_list.html'
            object = None
            form_class = LeaveAddForm
            paginate_by = 40
        en check if it's valid.
        """
        self.object_list = self.get_queryset()
        form = self.get_form()
        if form.is_valid():
            messages.success(request, "Your Work From Home request is forwarded! Please Wait for reply")
            return self.form_valid(form)
        else:
            messages.error(request, "Please Check the form!")
            return self.form_invalid(form)

    # PUT is a valid HTTP verb for creating (with a known URL) or editing an
    # object, note that browsers only support POST for now.
    def put(self, *args, **kwargs):
        return self.post(*args, **kwargs)


class UserLeaveView(HRRequired, TemplateView):
    template_name = 'leave/user-leave.html'
    paginate_by = 15
    date_field = 'created_at'
    ordering = 'created_at'
    allow_empty = True
    object = None
    today = date.today()
    is_annual = False

    def get_queryset(self):
        user_qs = User.objects.all().filter(is_manager=False, is_active=True)
        year = self.get_year()

        qs = LeaveRequest.objects.all().filter(
            approval_status__in=[LeaveRequest.SYSTEM_GENERATED, LeaveRequest.APPROVED],
            user__in=user_qs,
            leave_type__from_date__year=year
        ).select_related('leave_type')
        range_date = (self.get_first_date(), self.get_last_date())
        qs = qs.filter((Q(from_date__range=range_date)|Q(to_date__range=range_date)))
        # TODO: Cross border leave substraction
        """
        TODO: CASE 
        When a leave is took on jan 31st morning to feb 1st noon; 
        Only one leave should shown at month of january and half should shown on feb.
        """
        intermediate_data = defaultdict(list)
        for leave in qs:
            intermediate_data[leave.user].append(leave)
        out = defaultdict(dict)
        for user, leaves in intermediate_data.items():
            for leave in leaves:
                if 'lop' not in out[user]:
                    out[user]['lop'] = 0
                if 'paid' not in out[user]:
                    out[user]['paid'] = 0
                if 'lp' not in out[user]:
                    out[user]['lp'] = []
                if 'table' not in out[user]:
                    out[user]['table'] = []
                if leave.leave_type not in out[user]:
                    out[user][leave.leave_type] = []
                out[user]['paid'] += leave.leave_approved_in_days
                out[user]['lop'] += leave.leave_lop_in_days
                out[user]['table'].append(leave)
                out[user][leave.leave_type].append(leave)
        for user, leaves in intermediate_data.items():
            out[user]['table_data'] = {}
            for leave in out[user]['table']:
                if leave.leave_type not in out[user]['table']:
                    out[user]['table_data'][leave.leave_type] = {'lop': 0, 'paid': 0}
                out[user]['table_data'][leave.leave_type]['lop'] += leave.leave_lop_in_days
                out[user]['table_data'][leave.leave_type]['paid'] += leave.leave_approved_in_days
        for user in user_qs:
            if user not in out:
                out[user]['lop'] = 0
                out[user]['paid'] = 0
                out[user]['total'] = 0
                out[user]['description'] = '-'
            else:
                out[user]['total'] = out[user]['lop'] + out[user]['paid']
                # out[user]['description'] = "<br />".join([f"{leave.leave_type.name} - {leave.leave_lop_in_days + leave.leave_approved_in_days}" for leave in out[user]['table_data']])
                out[user]['description'] = "<br />".join([f"{leave_type.name} - PAID: {val['paid']}, LOP: {val['lop']}" for leave_type, val in out[user]['table_data'].items()])
            out[user]['user'] = user
        return out.values()

    _tm = None
    _tmnext = None

    def get_first_date(self):
        if self._tm is None:
            self._tm = date(day=1, month=self.get_month(), year=self.get_year())
        return self._tm

    def get_first_of_next_month(self):
        if self._tmnext is None:
            month = self.get_month()
            year = self.get_year()
            next_month = date(day=28, month=month, year=year) + datetime.timedelta(days=10)
            next_month_starting = date(day=1, month=next_month.month, year=next_month.year)
            self._tmnext = next_month_starting
        return self._tmnext

    def get_last_date(self):
        next_first = self.get_first_of_next_month()
        return next_first - datetime.timedelta(days=1)

    def get_month(self):
        return int(self.request.GET.get('month', self.today.month))

    def get_year(self):
        return int(self.request.GET.get('year', self.today.year))

    def get_context_data(self, *, object_list=None, **kwargs):
        month = self.get_month()
        year = self.get_year()
        kwargs['MONTH'] = str(month)
        kwargs['YEAR'] = str(year)
        kwargs['YEAR_INT'] = year
        kwargs['IS_ANNUAL'] = self.is_annual
        kwargs['SELECTED_MONTH'] = self.get_first_date()
        kwargs['TODAY'] = date.today()
        kwargs['year_range'] = range(2020, kwargs['TODAY'].year + 1)
        self.object_list = self.get_queryset()
        kwargs['objects_list'] = sorted(self.object_list, key=lambda x: x['user'].pk)
        return super(UserLeaveView, self).get_context_data(**kwargs, )


class UserAnnualLeaveView(HRRequired, TemplateView):
    template_name = 'leave/annual-user-leave.html'
    paginate_by = 15
    date_field = 'created_at'
    ordering = 'created_at'
    allow_empty = True
    object = None
    today = date.today()
    is_annual = True

    def get_year(self):
        return self.request.GET.get('year', date.today().year)

    def get_queryset(self):
        user_qs = User.objects.all().filter(is_manager=False, is_active=True)
        qs = LeaveRequest.objects.all().filter(
            approval_status__in=[LeaveRequest.SYSTEM_GENERATED, LeaveRequest.APPROVED],
            user__in=user_qs,
            leave_type__from_date__year=self.get_year()
        ).select_related('leave_type')
        return qs

    def get_context_data(self, **kwargs):
        kwargs = super(UserAnnualLeaveView, self).get_context_data(**kwargs)
        qs = self.get_queryset()
        out = {}
        all_leave_types = LeaveType.objects.filter(to_date__year=self.get_year())
        for lr in qs:
            if lr.user not in out:
                out[lr.user] = dict()
                for lt in all_leave_types:
                    out[lr.user][lt] = {
                        'lop': 0, 'paid': 0
                    }
            if lr.leave_type not in out[lr.user]:
                out[lr.user][lr.leave_type] = {
                    'lop': 0, 'paid': 0
                }

            out[lr.user][lr.leave_type]['paid'] += lr.leave_approved_in_days
            out[lr.user][lr.leave_type]['lop'] += lr.leave_lop_in_days

        kwargs['out'] = sorted([(user, [{'lt': leave, 'dic': dic} for leave, dic in out[user].items()]) for user in out], key=lambda data: data[0].username)
        kwargs['year_range'] = range(2021, date.today().year + 1)
        return kwargs


class LateAttendanceListView(HRRequired, ModelFormMixin, ListView):
    model = LateAttendance
    object = None
    objects_list = []
    queryset = LateAttendance.objects.filter(date__gt=(datetime.date.today() - datetime.timedelta(weeks=52))).order_by('-date')
    fields = ['user', 'date', 'reason']
    # template_name = 'leave/lateattendance_list.html'
    success_url = reverse_lazy('leave:late-attendance-list')
    paginate_by = 40

    def get_form(self, form_class=None):

        form = super(LateAttendanceListView, self).get_form(form_class=form_class)
        form.fields['user'].queryset = User.objects.filter(is_active=True, is_manager=False).order_by('id')
        return form

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        self.objects_list = self.get_queryset()
        form = self.get_form()
        if form.is_valid():
            messages.success(request, "Your leave request is forwarded! Please Wait for reply")
            return self.form_valid(form)
        else:
            messages.error(request, "Please Check the form!")
            return self.form_invalid(form)


class LeaveRequestDetailView(DetailView):
    model = LeaveRequest
    template_name = 'leave/leave-requests-detail.html'
    context_object_name = 'leave'

    def get_queryset(self):
        return super(LeaveRequestDetailView, self).get_queryset().filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object(self.get_queryset())
        self.object.approval_status = LeaveRequest.CANCELLED
        self.object.save()
        return redirect(reverse_lazy('leave:list'))


class NewLeaveRequestsListView(HRRequired, ListView):
    template_name = 'leave/new-leave-requests-list.html'
    ordering = '-from_date'
    allow_empty = True
    object = None
    post_errors = {}
    paginate_by = 45

    def get_context_data(self, *, object_list=None, **kwargs):
        qs = LeaveRequest.objects.all().select_related('user', 'leave_type').order_by(self.ordering)
        return super(NewLeaveRequestsListView, self).get_context_data(
            object_list=object_list,
            unresponded_list=qs.exclude(approval_status__in=LeaveRequest.FINALIZED_LEAVE_STATUS),
            responded_list=qs.filter(approval_status__in=LeaveRequest.FINALIZED_LEAVE_STATUS),
            **kwargs,
            errors=self.post_errors)

    def get_queryset(self):
        dt_b4_45 = date.today() - datetime.timedelta(days=45)
        return LeaveRequest.objects.all().filter(from_date__gte=dt_b4_45).order_by(self.ordering).select_related('user', 'leave_type')

    def post(self, request, *args, **kwargs):
        self.post_errors = {}
        lr = LeaveRequest.objects.filter(id=request.POST.get('id')).select_related('user', 'leave_type').first()
        if not lr:
            self.post_errors['id'] = "The Primary key does not Exist!"
        if lr:
            if not request.POST.get('approval_status') in [l[0] for l in LeaveRequest.LEAVE_STATUS_CHOICES]:
                self.post_errors['approval_status'] = "The Choice is not valid"
            if not request.POST.get('leave_approved_in_days'):
                self.post_errors['leave_approved_in_days'] = "How many days leaves are approved?"
            if not request.POST.get('leave_lop_in_days'):
                self.post_errors['leave_lop_in_days'] = "How many days LOP's are proceeded!?"

        if len(self.post_errors) > 0:
            return self.get(request, *args, **kwargs)

        lr.approval_status = request.POST.get('approval_status')
        lr.reason_for_action = request.POST.get('reason_for_action')
        lr.leaves_approved_in_days = request.POST.get('leave_approved_in_days')
        lr.leaves_lop_in_days = request.POST.get('leave_lop_in_days')
        # import pdb;pdb.set_trace()

        lr.save()
        messages.success(request, mark_safe(f"Updated <b>{str(lr)}</b> to {lr.approval_status} Successfully"))
        return redirect('leave:new-leave-requests')


class NewLeaveRequestsDetailView(HRRequired, UpdateView):
    template_name = 'leave/leave-requests-update.html'
    form_class = LeaveRequestUpdater
    model = LeaveRequest

    def get_form_class(self):
        if self.request.GET.get('need_advanced_form'):
            return LeaveRequestUpdaterAdvanced
        return LeaveRequestUpdater

    def get_form_kwargs(self):
        cxt = super().get_form_kwargs()
        cxt['request'] = self.request
        return cxt

    def get_context_data(self, *, object_list=None, **kwargs):
        cxt = super().get_context_data(object_list=object_list, **kwargs)
        cxt['leave'] = cxt.get('object')
        return cxt

    def get_success_url(self):
        if self.object.approval_status in self.object.CONFIRMED_LEAVE_STATUS:
            messages.success(self.request, f"Leave Request has been {self.object.approval_status}")
        elif self.object.approval_status in self.object.FINALIZED_LEAVE_STATUS:
            messages.warning(self.request, f"Leave Request has been {self.object.approval_status}")
        return reverse('leave:new-leave-requests')





