from apps.leave.notifications.email_notifications import LeaveEmailNotification, WFHEmailNotification


class NotificationEventHandler(object):
    __subject = None
    __template = None
    channels = ()

    def __init__(self, instance):
        self.model = instance
        for channel in self.channels:
            channel.set_instance(instance)

    def on_new_request(self):
        for channel in self.channels:
            channel.on_new_request()

    def on_cancellation(self):
        for channel in self.channels:
            channel.on_cancel()

    def on_confirm(self):
        for channel in self.channels:
            channel.on_confirm()

    def on_rejection(self):
        for channel in self.channels:
            channel.on_rejection()

    def on_autogenerated(self):
        for channel in self.channels:
            channel.on_autogenerated()


class LeaveNotificationEventHandler(NotificationEventHandler):
    channels = [
        LeaveEmailNotification()
    ]


class WfhNotificationEventHandler(NotificationEventHandler):
    channels = [
        WFHEmailNotification()
    ]


class NotificationEventHandlerBuilder(object):

    @classmethod
    def get_notification_handle(cls, mode, instance):
        """
        model should be "WFH" or "LEAVE" as of now
        """
        if mode == "WFH":
            config = WfhNotificationEventHandler(instance)

        elif mode == "LEAVE":
            config = LeaveNotificationEventHandler(instance)
        else:
            raise Exception(f"Notification handle for {mode} is not available at {cls}")
        return config


