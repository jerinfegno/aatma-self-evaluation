from datetime import date, timedelta, datetime

from django import forms
from django.conf import settings
from django.db.models import Case, When, F, Sum, Q
from django.utils import datetime_safe

from apps.leave.models import LeaveRequest, LeaveType, WFHRequest


class WFHAddForm(forms.ModelForm):
    wfh_date = forms.DateField(input_formats=['%d-%m-%Y'])

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(WFHAddForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    def save(self, commit=True):
        self.instance.user = self.request.user
        return super(WFHAddForm, self).save(commit=commit)

    def clean_reason_to_request(self):
        if len((self.request.POST.get('reason_to_request', '') or '').split()) < 10:
            raise forms.ValidationError("You should be more descriptive to your descriptions.")
        return self.request.POST['reason_to_request']

    class Meta:
        model = WFHRequest
        fields = ['wfh_date', 'reason_to_request',  ]


class LeaveAddForm(forms.ModelForm):
    from_date = forms.DateField(widget=forms.HiddenInput(), input_formats=['%d-%m-%Y'])
    to_date = forms.DateField(widget=forms.HiddenInput(), input_formats=['%d-%m-%Y'])

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(LeaveAddForm, self).__init__(*args, **kwargs)
        # self.fields['from_date'].initial = datetime_safe.date.today().strftime("%Y-%m-%d")
        # self.fields['to_date'].initial = datetime_safe.date.today().strftime("%Y-%m-%d")

        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

        self.fields['leave_type'].queryset = LeaveType.objects.filter(
            from_date__lte=date.today(), to_date__gte=date.today(), is_public=True)

    def save(self, commit=True):
        self.instance.user = self.request.user
        return super(LeaveAddForm, self).save(commit=commit)

    def clean_from_date(self):
        if self.request.POST['from_date']:
            return datetime.strptime(self.request.POST['from_date'], "%d-%m-%Y")

    def clean_to_date(self):
        if self.request.POST['to_date']:
            return datetime.strptime(self.request.POST['to_date'], "%d-%m-%Y")

    def clean_reason_to_request(self):
        if len((self.request.POST.get('reason_to_request', '') or '').split()) < 10:
            raise forms.ValidationError("You should be more descriptive to your descriptions.")
        return self.request.POST['reason_to_request']

    def clean(self, **kwargs):
        # from_date = self.cleaned_data['from_date'] = self.clean_from_date()
        # from_slot = self.cleaned_data['from_slot']
        # to_date = self.cleaned_data['to_date'] = self.clean_to_date()
        # to_slot = self.cleaned_data['to_slot']
        # leave_type = self.cleaned_data['leave_type']
        # import pdb;pdb.set_trace()
        user = self.request.user
        lr = LeaveRequest(**self.cleaned_data)
        LeaveRequest.is_able_to_take_leave(
            leave_approved_in_days=lr.leave_applicator.get_leave_count(),
            **self.cleaned_data, error=forms.ValidationError, instance=self.instance, user=user
        )

        return self.cleaned_data

    class Meta:
        model = LeaveRequest
        fields = ['leave_type', 'reason_to_request', 'from_date', 'from_slot', 'to_date', 'to_slot', ]
        widgets = {'from_date': forms.HiddenInput(), 'to_date': forms.HiddenInput()}


class LeaveRequestUpdater(forms.ModelForm):

    class Meta:
        model = LeaveRequest
        fields = (
            'approval_status', 'leave_approved_in_days', 'leave_lop_in_days', 'reason_for_action',
            # "leave_type", "reason_to_request", "from_date", "to_date", "from_slot", "to_slot"
        )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')

        super(LeaveRequestUpdater, self).__init__(*args, **kwargs)

    def clean(self):
        leave_type = self.instance.leave_type
        if self.cleaned_data['approval_status'] in LeaveRequest.CONFIRMED_LEAVE_STATUS:
            net_days = self.cleaned_data['leave_approved_in_days'] + self.cleaned_data['leave_lop_in_days']
            if net_days < self.instance.leave_requested_in_days:
                message = "You have to supply either leave_approved_in_days or leave_lop_in_days or" \
                          " both to have total no of leaves."
                raise forms.ValidationError(message)
            if leave_type.track_leave_count:
                leaves__sum = LeaveRequest.objects.annotate(
                    leaves=F('leave_approved_in_days')
                ).filter(
                    approval_status=LeaveRequest.APPROVED,
                    user=self.instance.user, leave_type=leave_type
                ).exclude(pk=self.instance.pk).aggregate(Sum('leaves'))['leaves__sum'] or 0

                _leave_approved_in_days = self.cleaned_data.get('leave_approved_in_days', 0) or 0
                _leave_lop_in_days = self.cleaned_data.get('leave_lop_in_days', 0) or 0
                if leave_type.no_of_days < (leaves__sum + _leave_approved_in_days):
                    raise forms.ValidationError(
                        f'{self.instance.user} already have consumed all the leaves from {leave_type.name}.')
        else:
            if len((self.cleaned_data.get('reason_for_action', '') or '').split(' ')) < 2:
                message = "You have to be more specific on updating leave status as "
                raise forms.ValidationError(message + self.cleaned_data['approval_status'])
        return self.cleaned_data


class LeaveRequestUpdaterAdvanced(LeaveRequestUpdater):
    class Meta:
        model = LeaveRequest
        fields = (
            'approval_status', 'leave_approved_in_days', 'leave_lop_in_days', 'reason_for_action',
            "leave_type", "reason_to_request", "from_date", "to_date", "from_slot", "to_slot"
        )


