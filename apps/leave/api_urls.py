from django.urls import path

from apps.leave.apis import leave_api_list, LeaveRequestAPI, leave_api_cancel, HolidayAPI, leave_request_api

app_name = 'leave-api'


urlpatterns = [
    path('leave-types/', leave_api_list, name='leave-type-api'),
    path('leave-request/', LeaveRequestAPI.as_view(), name='leave-request-api-v1'),
    path('holidays/', HolidayAPI.as_view(), name='holiday-api-v1'),
    path('leave-request/<int:pk>/cancel/', leave_api_cancel, name='cancel-leave-request-api-v1'),
    path('leave-request/display/', leave_request_api, name='leave-request-display-api-v1'),

]
