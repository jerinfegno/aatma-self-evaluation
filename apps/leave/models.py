import logging
from typing import Optional

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.datetime_safe import date

from apps.accounts.models import CompanyManagementConfig
from apps.leave.notifications.handler import LeaveNotificationEventHandler, NotificationEventHandlerBuilder

logger = logging.getLogger(__name__)


class LeaveType(models.Model):
    PAID = "PAID"
    UNPAID = "LOSS OF PAY"

    name = models.CharField(max_length=32)
    no_of_days = models.PositiveSmallIntegerField(help_text="If monthly allocatable, it must be multiple of 6.")
    payment_type = models.CharField(max_length=16, default=UNPAID, choices=[
        (PAID, PAID), (UNPAID, UNPAID),
    ])
    description = models.CharField(max_length=256)
    track_leave_count = models.BooleanField(
        default=True,
        help_text="If Track Leave Count is True, The User wont be able to take leave more than allotted count!")
    only_once_per_month = models.BooleanField(
        default=True,
        help_text="User Can take only One Leave from this category per month! ")
    is_public = models.BooleanField(
        default=True,
        help_text="Employee Can apply for this leave directly!")
    from_date = models.DateField(default=date.today)
    to_date = models.DateField(default=date.today)

    def __str__(self):
        return f"{self.name}"

    @property
    def payment_type_slug(self):
        return self.payment_type.replace(' ', '_')


weekends = ['Saturday', 'Sunday']


def is_working_day(value):
    if value.strftime('%A') in weekends and not WorkingDay.objects.filter(date=value).exists():
        raise ValidationError(
            '"%(value)s" is a non-working weekend. Leave Request Starting date and Ending date must be working days!',
            params={'value': value.strftime('%F, %A')})

    elif Holiday.objects.filter(date=value, is_restricted_holiday=False).exists():
        raise ValidationError(
            '"%(value)s" is not a working day. Leave Request Starting date and Ending date must be working days!',
            params={'value': value.strftime('%F, %A')})


def is_weekend(value):
    if value.strftime('%A') not in weekends:
        raise ValidationError(
            '"%(value)s" is not a weekend. Only days from weekends [Saturday-Sunday] can be added!',
            params={'value': value.strftime('%F, %A')})


class RestrictedHolidayGroup(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return f"{self.name}"


class Holiday(models.Model):
    date = models.DateField(
        default=date.today,
        # validators=[is_working_day]
    )
    name = models.CharField(max_length=64)
    description = models.TextField(null=True, blank=True)
    is_restricted_holiday = models.BooleanField(default=False)
    restricted_group = models.ForeignKey(RestrictedHolidayGroup, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return f"{self.date} {self.date.strftime('%A')} - {self.name}"

    def is_past(self):
        return self.date < date.today()

    def is_consumed_by(self, user):
        return user.leaverequest_set.filter(
            approval_status__in=[LeaveRequest.APPROVED, LeaveRequest.WAITING_FOR_APPROVAL],
            from_date__gte=self.date, to_date__lte=self.date
        ).exists()

    @property
    def day_strip(self):
        index = self.date.strftime("%w")
        css_class = ('restricted-' if self.restricted_group_id else '') + 'holiday'
        return f'''
          <ul class="weekdays {index}">
            <li class="day {(index == '0' and css_class) or 'weekend'}">&nbsp;</li>
            <li class="day {(index == '1' and css_class) or ''}">&nbsp;</li>
            <li class="day {(index == '2' and css_class) or ''}">&nbsp;</li>
            <li class="day {(index == '3' and css_class) or ''}">&nbsp;</li>
            <li class="day {(index == '4' and css_class) or ''}">&nbsp;</li>
            <li class="day {(index == '5' and css_class) or ''}">&nbsp;</li>
            <li class="day {(index == '6' and css_class) or 'weekend'}">&nbsp;</li>
          </ul>
        '''


class WorkingDay(models.Model):
    date = models.DateField(
        default=date.today,
        # validators=[is_weekend]
    )
    name = models.CharField(max_length=64)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return f"{self.date} {self.date.strftime('%A')} - {self.name}"


class WFHRequest(models.Model):
    APPROVED = "APPROVED"
    REJECTED = "REJECTED"
    CANCELLED = "CANCELLED"

    WAITING_FOR_APPROVAL = "WAITING FOR APPROVAL"
    LEAVE_STATUS_CHOICES = [
        (APPROVED, APPROVED),
        (CANCELLED, CANCELLED),
        (REJECTED, REJECTED),
    ]
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, )
    reason_to_request = models.TextField(null=True)
    wfh_date = models.DateField(default=date.today, validators=[is_working_day, ])
    approval_status = models.CharField(max_length=24, default=WAITING_FOR_APPROVAL,
                                       choices=LEAVE_STATUS_CHOICES, )
    approved_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
        null=True, blank=True,  related_name='approved_wfh')
    reason_for_action = models.CharField(null=True, blank=True, max_length=256)
    created_at = models.DateTimeField(default=timezone.now, blank=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._meta.get_field('approval_status').empty_label = "Please Select One"
        self.initial_approval_status = self.approval_status

    @property
    def status_finalized(self):
        return self.approval_status in [self.APPROVED, self.CANCELLED, self.REJECTED]


    @property
    def date_as_string(self):
        return f"{self.wfh_date}"

        # if self.is_half_day:
        #     out = f"{self.from_date} {self.from_slot_name} to {self.to_slot_name} - Half day"
        # else:
        #     out = (
        #             f"{self.from_date} {self.from_slot_name if self.has_half_day else ''} to {self.to_date} "
        #             + f"{self.to_slot_name if self.has_half_day else ''} - {self.leave_requested_in_days} Days"
        #     )
        # return out

    def send_email_on_request(self):
        if self.approval_status == self.WAITING_FOR_APPROVAL:
            if not self.user: return logger.error(f'Trying to send WFH Request mail on leave request <{self}> without user!')
            handle = NotificationEventHandlerBuilder.get_notification_handle(mode='WFH', instance=self)
            handle.on_new_request()

    def send_email_on_confirm(self):
        if not self.user: return logger.error(f'Trying to send WFH Confirmation mail on leave request <{self}> without user!')
        handle = NotificationEventHandlerBuilder.get_notification_handle(mode='WFH', instance=self)
        handle.on_confirm()

    def send_email_on_rejection(self):
        if not self.user: return logger.error(f'Trying to send Leave Rejection mail on leave request <{self}> without user!')
        handle = NotificationEventHandlerBuilder.get_notification_handle(mode='WFH', instance=self)
        handle.on_rejection()

    def send_email_on_cancellation(self):
        if not self.user: return logger.error(f'Trying to send Leave Rejection mail on leave request <{self}> without user!')
        handle = NotificationEventHandlerBuilder.get_notification_handle(mode='WFH', instance=self)
        handle.on_cancellation()

    def send_email_on_system_auto_update(self):
        if not self.user: return logger.error(f'Trying to send System auto Update mail on leave request <{self}> without user!')
        handle = NotificationEventHandlerBuilder.get_notification_handle(mode="WFH", instance=self)
        handle.on_autogenerated()


class LeaveRequest(models.Model):
    FROM_MORNING = "09:30 AM"
    FROM_AFTERNOON = "02:30 PM"

    TO_MORNING = "01:30 PM"
    TO_EVENING = "06:30 PM"

    SYSTEM_GENERATED = "SYSTEM_GENERATED"
    APPROVED = "APPROVED"
    REJECTED = "REJECTED"
    CANCELLED = "CANCELLED"

    WAITING_FOR_APPROVAL = "WAITING FOR APPROVAL"
    LEAVE_STATUS_CHOICES = [
        (APPROVED, APPROVED),
        (CANCELLED, CANCELLED),
        (REJECTED, REJECTED),
        (SYSTEM_GENERATED, SYSTEM_GENERATED),
    ]

    FINALIZED_LEAVE_STATUS = [APPROVED, REJECTED, CANCELLED, SYSTEM_GENERATED]
    CONFIRMED_LEAVE_STATUS = [APPROVED, SYSTEM_GENERATED]
    leave_type = models.ForeignKey(LeaveType, on_delete=models.CASCADE, null=True, blank=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, )
    reason_to_request = models.TextField(null=True)

    from_date = models.DateField(default=date.today, validators=[is_working_day])
    from_slot = models.CharField(
        max_length=12, default=FROM_MORNING,
        choices=[(FROM_MORNING, 'Morning'), (FROM_AFTERNOON, 'After Noon')])

    to_date = models.DateField(default=date.today, validators=[is_working_day])
    to_slot = models.CharField(
        max_length=12, default=TO_MORNING,
        choices=[(TO_MORNING, 'Noon'), (TO_EVENING, 'Evening')])

    is_halfday = models.BooleanField(default=False)  # this will be marked based on from_slot and to_slot

    approval_status = models.CharField(max_length=24, default=WAITING_FOR_APPROVAL,
                                       choices=LEAVE_STATUS_CHOICES, )
    approved_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
        null=True, blank=True,  related_name='approved_leaves')
    reason_for_action = models.CharField(null=True, blank=True, max_length=256)
    created_at = models.DateTimeField(default=timezone.now, blank=True)

    leave_requested_in_days = models.FloatField(default=0.0, help_text="No of days, Leave Requested.")
    leave_approved_in_days = models.FloatField(verbose_name="No. of Paid Leaves", default=0.0, help_text="Number of days without Loss Of Pay.")
    leave_lop_in_days = models.FloatField(verbose_name="No. of Loss of Pay",default=0.0, help_text="Number of days with Loss Of Pay ")

    def __init__(self, *args, **kwargs):
        super(LeaveRequest, self).__init__(*args, **kwargs)
        self._meta.get_field('user').empty_label = "Please Select One"
        self._meta.get_field('leave_type').empty_label = "Please Select One"
        self._meta.get_field('approval_status').empty_label = "Please Select One"

        from apps.leave.leave_type_calculator import LeaveApplicator, LeaveTypeValidation
        self.leave_validate = LeaveTypeValidation(self)
        self.leave_applicator = LeaveApplicator(self)
        self.initial_approval_status = self.approval_status

    @property
    def from_slot_name(self):
        return (self.from_slot == self.FROM_MORNING and 'Morning') or 'After Noon'

    @property
    def to_slot_name(self):
        return (self.to_slot == self.TO_EVENING and 'Evening') or 'Noon'

    @property
    def calc_days(self):
        return self.leave_applicator.get_leave_count()

    @property
    def find_days(self):
        return self.leave_applicator.get_leave_days()

    def clean(self):
        if not self.leave_type:
            raise ValidationError('Leave Type is required for a leave request.')
        leave_type = self.leave_type
        if leave_type.track_leave_count:
            leaves__sum = LeaveRequest.objects.annotate(
                leaves=models.Case(
                    models.When(approval_status=LeaveRequest.APPROVED,
                                then=models.F('leave_approved_in_days')
                                ),
                    default=models.F('leave_requested_in_days'))
            ).filter(
                approval_status__in=[LeaveRequest.APPROVED, LeaveRequest.WAITING_FOR_APPROVAL],
                user=self.user, leave_type=leave_type).aggregate(models.Sum('leaves'))['leaves__sum']
            if not self.pk and leaves__sum and leave_type.no_of_days < leaves__sum:
                raise ValidationError(f'You already have consumed all the leaves from {leave_type.name}.')

    def save(self, **kwargs):
        if not self.created_at:
            self.created_at = timezone.now()
        if not (self.approval_status == self.SYSTEM_GENERATED):
            # self.leave_requested_in_days = self.calc_days
            data = self.leave_validate.validate(self.leave_type, self.calc_days)
            self.__dict__.update(data)
        super(LeaveRequest, self).save(**kwargs)

    @property
    def is_upcoming(self):
        return self.approval_status == self.APPROVED and date.today() <= self.from_date

    @property
    def is_consumed(self):
        return self.approval_status == self.APPROVED and date.today() > self.from_date

    @property
    def is_rejected(self):
        return self.approval_status == self.REJECTED

    @property
    def is_waiting(self):
        return not self.approval_status or self.approval_status == self.WAITING_FOR_APPROVAL

    @property
    def is_cancelled(self):
        return self.approval_status == self.CANCELLED

    @property
    def is_approved(self):
        return self.approval_status == self.APPROVED

    @property
    def is_in_other_rejection(self):
        return not self.is_approved and date.today() > self.from_date


    @property
    def has_half_day(self):
        return self.from_slot == self.FROM_AFTERNOON or self.to_slot == self.TO_MORNING

    @property
    def is_half_day(self):
        return self.has_half_day and self.from_date == self.to_date

    @property
    def date_as_string(self):
        if self.is_half_day:
            out = f"{self.from_date} {self.from_slot_name} to {self.to_slot_name} - Half day"
        else:
            out = (
                    f"{self.from_date} {self.from_slot_name if self.has_half_day else ''} to {self.to_date} "
                    + f"{self.to_slot_name if self.has_half_day else ''} - {self.leave_requested_in_days} Days"
            )
        return out

    @classmethod
    def is_able_to_take_leave(cls, user, leave_type, from_date, from_slot, to_date, to_slot,
                              leave_approved_in_days,
                              error, instance=None, **kwargs):
        if from_date > to_date:
            raise error("Starting Date must be above 'Ending Date'")
        if from_date == to_date and (
                from_slot == LeaveRequest.FROM_AFTERNOON and to_slot == LeaveRequest.FROM_MORNING):
            raise error("To time slot' must come after 'from time slot'")
        if not leave_type:
            raise error('Leave Type is required for a leave request.')
        my_leaves_in_leave_type = cls.objects.filter(
            approval_status__in=[cls.APPROVED, cls.WAITING_FOR_APPROVAL], user=user, leave_type=leave_type
        )
        "================================================================================="
        #for handling leave that has been cancelled previously
        # new_leave = cls.objects.filter(approval_status__in=[cls.REJECTED, cls.CANCELLED], user=user, leave_type=leave_type)
        # if not my_leaves_in_leave_type:
        #     my_leaves_in_leave_type = new_leave
        "================================================================================="
        leave_qs = my_leaves_in_leave_type.annotate(
            leaves=models.Case(
                models.When(approval_status=LeaveRequest.APPROVED, then=models.F('leave_approved_in_days')),
                default=models.F('leave_requested_in_days'))
        )

        if leave_type.only_once_per_month and leave_qs.filter(
                models.Q(from_date__year=from_date.year, from_date__month=from_date.month, )
                | models.Q(to_date__year=to_date.year, to_date__month=to_date.month)).exists():
            leave_request = leave_qs.filter(leave_requested_in_days__lte=1).values_list('from_date__month', flat=True)
            if len(leave_request) != len(set(leave_request)):
                raise error(f'You already have applied for another {leave_type.name} in this month.')
        if leave_type.track_leave_count:
            if instance is not None and instance.pk:
                # in case of update, ignore count from current leave.
                leave_qs = leave_qs.exclude(pk=instance.pk)
            # overall leave user had taken for current leave_type
            leaves__sum_from_db = leave_qs.aggregate(models.Sum('leaves'))['leaves__sum'] or 0
            # if no.of leaves for the current leave_type reaches the limit
            if leave_type.no_of_days < leaves__sum_from_db + leave_approved_in_days:
                raise error(f'You already have consumed all the leaves from {leave_type.name}.')

    def send_email_on_request(self):
        if self.approval_status == self.WAITING_FOR_APPROVAL:
            if not self.user: return logger.error(
                f'Trying to send WFH Request mail on leave request <{self}> without user!')
            handle = NotificationEventHandlerBuilder.get_notification_handle(mode='LEAVE', instance=self)
            handle.on_new_request()

    def send_email_on_confirm(self):
        if not self.user: return logger.error(
            f'Trying to send WFH Confirmation mail on leave request <{self}> without user!')
        handle = NotificationEventHandlerBuilder.get_notification_handle(mode='LEAVE', instance=self)
        handle.on_confirm()

    def send_email_on_rejection(self):
        if not self.user: return logger.error(
            f'Trying to send Leave Rejection mail on leave request <{self}> without user!')
        handle = NotificationEventHandlerBuilder.get_notification_handle(mode='LEAVE', instance=self)
        handle.on_rejection()

    def send_email_on_cancellation(self):
        if not self.user: return logger.error(
            f'Trying to send Leave Rejection mail on leave request <{self}> without user!')
        handle = NotificationEventHandlerBuilder.get_notification_handle(mode='LEAVE', instance=self)
        handle.on_cancellation()

    def send_email_on_system_auto_update(self):
        if not self.user: return logger.error(
            f'Trying to send System auto Update mail on leave request <{self}> without user!')
        handle = NotificationEventHandlerBuilder(mode="LEAVE", instance=self)
        handle.on_autogenerated()


class LateAttendance(models.Model):
    date = models.DateField()
    reason = models.CharField(max_length=128)
    associated_leave = models.OneToOneField(LeaveRequest, on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, )

    def generate_leave(self):
        cmc = CompanyManagementConfig.get_solo()
        lr = LeaveRequest()
        lr.leave_type = cmc.late_attendance_lop_leave
        lr.user = self.user
        lr.approval_status = lr.SYSTEM_GENERATED
        lr.from_date = self.date
        lr.to_date = self.date
        lr.from_slot = lr.FROM_MORNING
        lr.to_slot = lr.TO_MORNING
        lr.approved_by = cmc.hr
        lr.reason_for_request = "LOP due to the late reporting of working time"
        lr.reason_for_action = "LOP due to the late reporting of working time"
        lr.leave_requested_in_days = 0.0
        lr.leave_approved_in_days = 0.0
        lr.leave_lop_in_days = 0.5
        lr.lateattendance = self
        lr.save()
        lr.send_email_on_system_auto_update()
        self.associated_leave = lr
        self.save()
        return lr


def create_leave(sender: type, instance: LateAttendance, created: bool, **kwargs):
    if not instance.associated_leave:
        instance.generate_leave()


def update_status(sender: type, instance: LeaveRequest, created: bool, **kwargs):
    if created:
        instance.send_email_on_request()
    elif instance.initial_approval_status != instance.approval_status:
        if instance.approval_status == instance.APPROVED:
            instance.send_email_on_confirm()
        elif instance.approval_status == instance.REJECTED:
            instance.send_email_on_rejection()
        elif instance.approval_status == instance.CANCELLED:
            instance.send_email_on_cancellation()
        elif instance.approval_status == instance.SYSTEM_GENERATED:
            instance.send_email_on_system_auto_update()
        instance.initial_approval_status = instance.approval_status


post_save.connect(update_status, sender=LeaveRequest)
post_save.connect(update_status, sender=WFHRequest)
post_save.connect(create_leave, sender=LateAttendance)



