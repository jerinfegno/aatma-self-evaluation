from django.db.models import ExpressionWrapper, F, FloatField, When, Case, Sum
from rest_framework import authentication
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.generics import ListCreateAPIView, get_object_or_404, ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.authentication import CSRFExemptSessionAuthentication
from apps.leave.models import LeaveType, LeaveRequest, Holiday
from django.utils.datetime_safe import date

from apps.leave.serializers import LeaveRequestSerializer, HolidaySerializer, LeaveRequestDisplaySerializer

jan_1st = date(day=1, month=1, year=date.today().year)
dec_31st = date(day=31, month=12, year=date.today().year)


@api_view(['GET'])
def leave_api_list(request):
    if request.user.is_anonymous:
        return Response({
            'response': "you need to be authenticated"
        })
    qs = LeaveRequest.objects.all().exclude(leave_type__isnull=True)
    qs = qs.filter(from_date__gte=jan_1st, to_date__lte=dec_31st)
    used_leaves = qs.filter(user=request.user).values('leave_type')
    approved_leaves_expression = Sum(Case(When(
        approval_status=LeaveRequest.APPROVED, then=F('leave_approved_in_days'),
    ), default=0.0, output_field=FloatField()))
    pending_leave_expression = Sum(Case(When(
        approval_status=LeaveRequest.WAITING_FOR_APPROVAL, then=F('leave_requested_in_days'),
    ), default=0.0, output_field=FloatField()))
    used_leaves = used_leaves.annotate(
        leave_type_name=F('leave_type__name'),
        total_leave=F('leave_type__no_of_days'),
        approved_leaves=approved_leaves_expression,
        pending_leaves=pending_leave_expression,
        remaining_leaves=ExpressionWrapper(F('leave_type__no_of_days') - approved_leaves_expression,
                                           output_field=FloatField()),
    )
    lrs = [l for l in used_leaves]
    lt_set = [{
        'leave_type': lt.id,
        'leave_type_name': lt.name,
        'total_leave': lt.no_of_days,
        'approved_leaves': 0.0,
        'pending_leaves': 0.0,
        'remaining_leaves': lt.no_of_days,
    } for lt in LeaveType.objects.exclude(id__in=[l['leave_type'] for l in lrs])]

    out = {
        'leave_types': lrs + lt_set,
        'leave_statuses': dict(LeaveRequest.LEAVE_STATUS_CHOICES),
    }
    return Response(out)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def leave_api_cancel(request, pk):
    obj = get_object_or_404(LeaveRequest.objects.filter(user=request.user), pk=pk)
    obj.approval_status = LeaveRequest.CANCELLED
    obj.approved_by = request.user
    obj.save()
    out = {
        'message': "Leave request have been cancelled.",
    }
    return Response(out)


class HolidayAPI(ListAPIView):
    queryset = Holiday.objects.all().order_by('-id').filter(date__gte=jan_1st, date__lte=dec_31st)
    serializer_class = HolidaySerializer
    authentication_classes = [CSRFExemptSessionAuthentication, ]
    permission_classes = [IsAuthenticated, ]


class LeaveRequestAPI(ListCreateAPIView):
    authentication_classes = [CSRFExemptSessionAuthentication, ]
    queryset = LeaveRequest.objects.all().order_by('-id')
    serializer_class = LeaveRequestSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)


@api_view()
@permission_classes([IsAuthenticated])
def leave_request_api(request):
    qs = LeaveRequest.objects.filter(user=request.user, to_date__gte=date(day=1, month=1, year=date.today().year))
    up_comming = []
    approved = []
    others = []
    for leave in qs:
        if leave.is_upcoming or leave.is_waiting:
            up_comming.append(leave)
        elif leave.is_consumed:
            approved.append(leave)
        else:
            others.append(leave)
    ser = LeaveRequestDisplaySerializer
    cxt = {'request': request}
    return Response({
        'up_comming': [ser(instance, context=cxt).data for instance in up_comming],
        'approved': [ser(instance, context=cxt).data for instance in approved],
        'others': [ser(instance, context=cxt).data for instance in others],
    })


