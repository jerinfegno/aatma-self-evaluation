from abc import ABC
from random import randint
import factory
from factory.django import DjangoModelFactory
from django.core.management import BaseCommand

from apps.leave.models import LeaveRequest, LeaveType
from apps.user.models import User


class LRFactory(DjangoModelFactory):
    class Meta:
        model = LeaveRequest  # Equivalent to ``model = myapp.models.User``


class Command(BaseCommand):

    def handle(self, *args, **options):
        admin = User.objects.get(username="ft0001")
        for u in User.objects.all():
            for r in LeaveType.objects.all():
                for a in range(2):
                    print(LRFactory(user=u, leave_type=r))




