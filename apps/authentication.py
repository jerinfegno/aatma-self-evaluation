from django.contrib.auth import get_user_model
from django.contrib.auth.backends import BaseBackend
from rest_framework.authentication import SessionAuthentication
from rest_framework.authtoken.models import Token


class CSRFExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        pass


'django.contrib.auth.backends.ModelBackend'
UserModel = get_user_model()


class ScannedTokenBackend(BaseBackend):

    def authenticate(self, request, username=None, token=None, **kwargs):
        if username is None or token is None:
            return

        token = Token.objects.filter(user__username=request.data['username']).select_related('user').first()
        if token is None or token.user is None or token.user.is_anonymous:
            return
        if self.user_can_authenticate(token.user):
            return token.user

    def get_user(self, user_id):
        try:
            user = UserModel._default_manager.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None
        return user if self.user_can_authenticate(user) else None

    def user_can_authenticate(self, user):
        is_active = getattr(user, 'is_active', None)
        return is_active or is_active is None






