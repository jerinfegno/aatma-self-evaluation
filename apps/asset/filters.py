from apps.user.models import User
from django_filters import FilterSet, ModelChoiceFilter

from apps.asset.models import Component


class AssetSearchFilter(FilterSet):
    """Here  queryset is to list the users for filtering"""
    ownership__user = ModelChoiceFilter(queryset=User.objects.filter(is_active=True).all(),  method='user_filter',
                                        label='User')

    class Meta:
        model = Component
        fields = {
            'code': ['icontains'],
            'category': ['exact'],
            'ownership__user': ['exact']
        }

    def user_filter(self, queryset, name, value):
        comp = Component.objects.filter(ownership__user=value).values_list('id', flat=True)
        return queryset.filter(id__in=comp)

