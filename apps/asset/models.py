import datetime

from django.db import models


# Create your models here.
from django.conf import settings
from django.db import models


class AssetCategory(models.Model):
    """
    AssetCategory model consist of type or category of an asset
    """
    name = models.CharField(max_length=100)
    count = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class Component(models.Model):
    """
    Component model consists of details about the assets
    ----------------------------------------------------
    defected_by is used to indicate component has been defected by which user
    and is foreign key to User model..

    name: name of the component or asset
    code:code for each component it should contain a unique code
    _is_vacant :component is assigned or unassigned
    category: category of the component eg:system,pendrive,harddisk
    _is_defected :component is destroyed or not
    defected_by :specifies the user which damaged or defected the product

    """
    name = models.CharField(max_length=200, blank=True, null=True)
    code = models.CharField(max_length=10, blank=True, null=True,unique=True)
    price = models.FloatField(null=True, blank=True)

    _is_vacant = models.BooleanField(default=True)  # component is assigned or unassigned
    category = models.ForeignKey(AssetCategory, on_delete=models.CASCADE)
    _is_defected = models.BooleanField(default=False)  # component is destroyed or not
    defected_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Components'
        ordering = ['-id']

    @property
    def is_vacant(self):
        return self._is_vacant  # to get the value of private variable _is_vacant

    def assign_to(self, user, fromdate):
        self.unassign_component()
        # assign to some user
        self._is_vacant = False
        self.save()
        Ownership.objects.create(user=user, from_date=fromdate, component=self)

    def unassign_component(self, defected_by=None):
        """
        unassign_component is function to unassign assets or components,
        we can also specify whether the asset has been defected or not ,
        if defected, we can specify the user who destroyed it .
        """
        ownership = Ownership.get_ownership(component=self)
        if ownership:
            ownership.to_date = datetime.datetime.now(tz=datetime.timezone.utc)
            ownership.save()
        self.defected_by = defected_by
        self._is_vacant = True
        self.save()

    @property
    def is_defected(self):
        return self._is_defected

    def __str__(self):
        return f'{self.name} ({self.code})'

    def mark_damaged_assets(self, user):

        ownership = Ownership.objects.get(component=self)
        print("dsssssssssss", self, ownership)
        self._is_defected = True
        ownership.defected = True
        self.unassign_component(user)
        self.save()

    def assign_to(self, user):
        self.unassign()
        # assign to some user
        # set _is_vaccent = False
        pass

    def unassign(self, defected_by=None):
        ownership = Ownership.get_ownership(component=self)
        if ownership:
            ownership.to_date = datetime.now()
            # ownership.defected = defected_by and ownership.user == defected_by
            ownership.save()
        self.defected_by = defected_by
        self.save()


class Ownership(models.Model):
    """
    Ownership model contains details about ownership of the component or asset
    --------------------------------------------------------------------------
    from_date: Date from which asset is assigned
    to_date: Date upto which asset is unassigned
    user: foreignkey variable which user is assigned this asset
    component: foreignkey variable which asset is assigned

    """
    from_date = models.DateTimeField(default=datetime.datetime.now)
    to_date = models.DateTimeField(null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    component = models.ForeignKey(Component, on_delete=models.CASCADE)
    defected = models.BooleanField(default=False)
    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.user.username

    @classmethod
    def get_ownership(cls, component):

        """    get the ownership details of recently assigned owner   """
        return Ownership.objects.filter(component=component, to_date=None).order_by('-id').first()
