from django.forms import ModelForm, DateTimeInput, DateTimeField

from apps.asset.models import Ownership, AssetCategory


class AssignForm(ModelForm):
    class Meta:
        model = Ownership
        fields = ['from_date', 'user', 'component']

    from_date = DateTimeField(
        label='From date', label_suffix=" : ",
        required=True, disabled=True,
        input_formats=["%d-%m-%Y %H:%M"],
        widget=DateTimeInput(attrs={'class': 'form-control'}),
        error_messages={'required': "This field is required."})

    to_date = DateTimeField(label='To Date', label_suffix=" : ",
                            required=False, disabled=False,
                            input_formats=["%d-%m-%Y %H:%M"],
                            widget=DateTimeInput(attrs={'class': 'form-control'}),
                            error_messages={'required': "This field is required."})
