from django.urls import path, include

from .views import *

app_name = 'asset'

urlpatterns = [
    path('asset/', include([
        path('asset-list/', AssetListView.as_view(), name='asset_list'),
        path('asset-add/', AssetCreateView.as_view(), name='asset_add'),
        path('asset-update/<int:pk>/', AssetUpdateView.as_view(), name='asset_update'),
        path('asset-remove/<int:pk>/', AssetDeleteView.as_view(), name='asset_remove'),
        path('asset-add-category/', AssetCategoryCreateView.as_view(), name='asset_add_category'),
        path('asset-category-list/', AssetCategoryListView.as_view(), name='asset_category_list'),
        path('asset-category-update/<int:pk>/', AssetCategoryUpdateView.as_view(), name='asset_category_update'),
        path('asset-damage/', AssetAssignListView.as_view(), name='asset_assign'),
        path('asset-assign-create/<int:pk>/', AssetAssign.as_view(), name='asset_assign_create'),
        path('asset-unassign/', ajax_asset_unassign, name='asset_unassign'),
        path('asset-assign-ownership/', ajax_asset_assign, name='asset_assign_ownership'),
        path('asset-damaged-assets/', ajax_asset_defected, name='asset_damaged_assets'),

        # below is the url for the user perspective assetview
        path('user-asset-list/', UserAssetListView.as_view(), name='user_asset_list'),

    ]))
]
