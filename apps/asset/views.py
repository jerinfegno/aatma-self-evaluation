import datetime

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Count, Q
from django.shortcuts import redirect

# Create your views here.
from django.urls import reverse, reverse_lazy

from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView
from django.views.generic.edit import DeleteView

from apps.asset.forms import AssignForm
from apps.asset.models import Ownership, Component, AssetCategory
from apps.user.models import User
from apps.asset.filters import AssetSearchFilter
from apps.utils.permissions import HRRequired

"""
Below Four(4) viewa describes the category details that are managed
"""


class AssetCategoryCreateView(HRRequired, CreateView):
    """For Creating New Categories"""
    template_name = "asset/asset_add_category.html"
    model = AssetCategory
    fields = ('name', 'count')

    def form_valid(self, form):
        return super(AssetCategoryCreateView, self).form_valid(form)

    def form_invalid(self, form):
        return super(AssetCategoryCreateView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('asset:asset_category_list')


class AssetCategoryListView(HRRequired, ListView):
    """For listing already available categories"""
    template_name = "asset/asset_category_list.html"
    model = AssetCategory
    fields = ('name', 'count')
    paginate_by = 20


class AssetCategoryUpdateView(HRRequired, UpdateView):
    """For editing the Category details"""
    template_name = "asset/asset_category_update.html"
    model = AssetCategory
    fields = ('name', 'count')

    def get_context_data(self, **kwargs):
        return super(AssetCategoryUpdateView, self).get_context_data(**kwargs)

    def form_valid(self, form):
        return super(AssetCategoryUpdateView, self).form_valid(form)

    def form_invalid(self, form):
        return super(AssetCategoryUpdateView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('asset:asset_category_list')


class AssetCategoryDeleteView(HRRequired, DeleteView):
    """For Removing Existing Assets"""
    model = AssetCategory

    def get_success_url(self):
        return reverse_lazy('asset:asset_list')


"""
Below Three(3) viewa describes the Asset details that are managed
"""


class AssetCreateView(HRRequired, CreateView):
    """For Creating New Assets"""
    template_name = "asset/asset_add.html"
    model = Component
    fields = ('category', 'name', 'code', 'price')

    def form_valid(self, form):
        return super(AssetCreateView, self).form_valid(form)

    def form_invalid(self, form):
        return super(AssetCreateView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('asset:asset_list')


class AssetUpdateView(HRRequired, UpdateView):
    """For modifying existing Assets"""
    template_name = "asset/asset_update.html"
    model = Component
    fields = ('name', 'code', 'price', 'category')

    def get_context_data(self, **kwargs):
        return super(AssetUpdateView, self).get_context_data(**kwargs)

    def form_valid(self, form):
        return super(AssetUpdateView, self).form_valid(form)

    def form_invalid(self, form):
        return super(AssetUpdateView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('asset:asset_list')


class AssetDeleteView(HRRequired, DeleteView):
    """For Removing Existing Assets"""
    model = Component

    def get_success_url(self):
        return reverse_lazy('asset:asset_list')


"""
Below Four(4) viewa describes the Asset ownership details that are managed
"""


class AssetListView(HRRequired, ListView):
    """ ListView for Assets that are created   """
    template_name = "asset/asset_list.html"
    model = Component
    allow_empty = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filterdata'] = AssetSearchFilter(self.request.GET, Component.objects.
                                                  select_related('category').order_by('_is_defected', '-_is_vacant').all())
        page = self.request.GET.get('page', 1)

        paginator = Paginator(context['filterdata'].qs, 20)
        try:
            page_obj = paginator.page(page)
        except PageNotAnInteger:
            page_obj = paginator.page(1)
        except EmptyPage:
            page_obj = paginator.page(paginator.num_pages)
        context['filter'] = page_obj
        context['result'] = User.objects.filter(is_active=True).values('id', 'username')
        context['count_data'] = Component.objects.aggregate(unassign_count=Count('_is_vacant',
                                                                                 filter=Q(_is_vacant=True,
                                                                                          _is_defected=False)),
                                                            assign_count=Count('_is_vacant',
                                                                               filter=Q(_is_vacant=False,
                                                                                        _is_defected=False)),
                                                            damage_count=Count('_is_defected',
                                                                               filter=Q(_is_defected=True)))
        return context


class AssetAssign(HRRequired, UpdateView):
    """ UpdateView for assets to editing asset details"""
    template_name = "asset/asset_assign_list.html"
    model = Ownership
    form_class = AssignForm


@csrf_exempt
def ajax_asset_assign(request):
    """ Manage Assets to be Assigned """
    if request.method == "POST":
        asset_id = request.POST.get('component')
        user_id = request.POST.get('value')
        user = User.objects.get(id=user_id)
        asset_obj = Component.objects.get(id=asset_id)
        fromdate = datetime.datetime.now(tz=datetime.timezone.utc)
        asset_obj.assign_to(user, fromdate)
    return redirect('asset:asset_list')


@csrf_exempt
def ajax_asset_unassign(request):
    """ Manage Assets to be damaged """
    if request.method == "POST":
        asset_id = request.POST.get('element')
        asset_obj = Component.objects.get(id=asset_id)
        asset_obj.unassign_component()
    return redirect('asset:asset_list')


class AssetAssignListView(HRRequired, ListView):
    """
    ListView for Assets that are assigned to users
    Don't forgot to change paginate_by
    """
    template_name = "asset/asset_damaged.html"
    model = Ownership
    queryset = Ownership.objects.filter(defected=False).select_related('user', 'component__defected_by', 'component')
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['result'] = User.objects.filter(is_active=True).values('id', 'username')
        return context


@csrf_exempt
def ajax_asset_defected(request):
    """Marks assets as damaged"""
    if request.method == "POST":
        asset_id = request.POST.get('component')
        user_id = request.POST.get('value')
        user = User.objects.get(id=user_id)
        asset_obj = Component.objects.get(id=asset_id)
        asset_obj.mark_damaged_assets(user)
    return redirect('asset:asset_assign')


"""
Below is the function for defining the  
"""


class UserAssetListView(ListView):
    model = Ownership
    template_name = "asset/asset_user_listview.html"

    def get_queryset(self):
        queryset = Ownership.objects.select_related('component', 'user').filter(user=self.request.user.id)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['data'] = self.get_queryset()
        return context

