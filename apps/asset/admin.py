from django.contrib import admin
from . models import *
# Register your models here.

from import_export.admin import ImportExportModelAdmin, ExportActionMixin


class CommonAdmin(ImportExportModelAdmin):
    pass


admin.site.register(AssetCategory, CommonAdmin)
admin.site.register(Ownership, CommonAdmin)
admin.site.register(Component, CommonAdmin)

