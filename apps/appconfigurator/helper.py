

class ConfigGenerator:

    def __init__(self, model_class):
        self.model_class = model_class
        self.queryset = model_class.objects.all()
        self.list_view = []
        self.create_view_fields = []
        self.update_view_fields = []
        self.read_only_fields = []
        self.can_delete = True
        self.can_update = True

    def get_headers(self, ):
        return self.headers

    def get_queryset(self, page=1, page_size=25):
        from_obj = (page-1) * page_size
        to_obj = (page) * page_size
        return self.queryset[from_obj:to_obj]

    def get_serialized_list(self, page=1, page_size=25):
        qs = self.get_queryset(page, page_size)
        dt = []
        for instance in qs:
            row = []
            for field in self.list_view:
                if hasattr(instance, field):
                    dt[field] = getattr(instance, field, None)
                else:
                    dt[field] = '-'
        return dt

    def get_fields(self):
        return {
                field.name: {
                    "name": field._verbose_name,
                    "help_text": field.help_text,
                    "type": field.get_internal_type(),
                    "choices": field.get_choices(),
                    "max_length": getattr(field, 'max_length')
                } for field in self.model_class()._meta.fields
            }

    def get_data(self):
        return {
            "fields": self.get_fields(),
            'can_delete': self.can_delete,
            'can_update': self.can_update,
            'create': self.create_view_fields,
            'update': self.update_view_fields,
            'table': {
                "fields": self.list_view,
                "headers": self.get_headers(),
                "content": [],
            },
            "list_url": self.get_list_url(),
            "detail_url": {
                "template": self.get_detail_url(),
                "replacement": {
                    "/:pk/": 'pk'
                }
            }
        }

    list_url = None
    detail_url = None

    def get_detail_url(self):
        pass

    def get_list_url(self):
        pass

