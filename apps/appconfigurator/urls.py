from django.urls import path, include


api_urlpatterns = [
    path('', configurator, name="user-profile"),

]


urlpatterns = [
    path('api/v1/configurator/', include(api_urlpatterns))
]

