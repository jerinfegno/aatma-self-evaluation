# Generated by Django 3.1.3 on 2022-07-14 10:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_auto_20220708_1322'),
    ]

    operations = [
        migrations.CreateModel(
            name='Branch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='InflowOutflow',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('net_expense', models.FloatField(help_text='in Rupees.')),
                ('net_billed_amount', models.FloatField(help_text='in Rupees.')),
                ('net_amount_converted', models.FloatField(help_text='in Rupees.')),
                ('net_inflow_from_aop', models.FloatField(help_text='in Rupees.')),
                ('no_leads_collected', models.PositiveIntegerField(default=0)),
                ('no_positive_leads', models.PositiveIntegerField(default=0)),
                ('no_convertions', models.PositiveIntegerField(default=0)),
                ('no_client_visits', models.PositiveIntegerField(default=0)),
                ('no_premises_clinet_meetings', models.PositiveIntegerField(default=0)),
                ('no_expected_deliveries', models.PositiveIntegerField(default=0)),
                ('no_done_deliveries', models.PositiveIntegerField(default=0)),
                ('branch', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.branch')),
            ],
        ),
    ]
