from django.utils.datetime_safe import date

from django.core.exceptions import ValidationError

from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import datetime_safe
from gst_field.modelfields import GSTField, PANField
from num2words import num2words
from phonenumber_field.modelfields import PhoneNumberField
from solo.models import SingletonModel

from apps.generic.models.address import AddressAbstract, pincode_validator

from apps.utils.fields import TextFieldAsChar


class CompanyConfig(AddressAbstract, SingletonModel):
    name = models.CharField(max_length=32, default="Fegno Technologies LLP")
    email = models.EmailField(default="hello@fegno.com")
    gst_in = GSTField(default="32AAGFF2957D1ZQ")
    pan = PANField(default="AAGFF2957D")
    llp_in = models.CharField(max_length=10, default="AAN-8916", verbose_name="LLP Identification Number")

    contact_01 = PhoneNumberField(default="+918129464522")
    contact_01_holder_name = models.CharField(max_length=256, default="Manoj Hari")
    contact_02 = PhoneNumberField(default="+919539605605")
    contact_02_holder_name = models.CharField(max_length=256, default="Anoop M.V.")

    address_line_1 = models.TextField(
        null=True, blank=True,
        help_text="Billing Address, Line 1, (Office Name)", verbose_name="Line 1",
        default="1st Floor, Geo Infopark"
    )
    address_line_2 = models.TextField(
        null=True, blank=True,
        help_text="Billing Address, Line 2, (Street Name )", verbose_name="Line 2",
        default="Plot 16, KINFRA Park"
    )
    address_line_3 = models.TextField(
        null=True, blank=True,
        help_text="Billing Address, Line 3, (City, [PinCode/Zipcode])", verbose_name="Line 3",
        default="Infopark PO, Kochi"
    )
    address_line_4 = models.TextField(
        null=True, blank=True,
        help_text="Billing Address, Line 4 (State (State Code), Country)", verbose_name="Line 4",
        default="Kerala, India."
    )
    pincode = models.CharField(max_length=6, default="682042", validators=[pincode_validator])

    bank_account_name = models.CharField(max_length=256, default="Fegno Technologies LLP")
    bank_account_number = models.CharField(max_length=256, default="918020112473932")
    bank_ifsc = models.CharField(max_length=256, default="UTIB0001647")
    bank_swift = models.CharField(max_length=256, default="AXISINBB081")

    facebook_handle = models.CharField(max_length=256, null=True, blank=True)
    instagram_handle = models.CharField(max_length=256, null=True, blank=True)
    linkedin_handle = models.CharField(max_length=256, null=True, blank=True)
    twitter_handle = models.CharField(max_length=256, null=True, blank=True)
    github_handle = models.CharField(max_length=256, null=True, blank=True)


class CompanyManagementConfig(SingletonModel):
    hr = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    late_attendance_lop_leave = models.ForeignKey('leave.LeaveType', on_delete=models.SET_NULL, null=True)


class Tax(models.Model):
    name: str = models.CharField(max_length=128)
    value: float = models.FloatField(default=18.0)

    def __str__(self):
        return self.name


class Currency(models.Model):
    name: str = models.CharField(max_length=3, primary_key=True)
    prepend_text: str = models.CharField(max_length=3)
    prepend_text_for_pdf: str = models.CharField(max_length=3, null=True,
                                                 help_text="PDF's usually wont support Cyrillic fonts ")

    def __str__(self):
        return f"{self.name} {('(' + self.prepend_text + ')') if self.prepend_text else ''}"


class ProformaInvoiceLine(models.Model):
    description: str = models.CharField(max_length=256, verbose_name="Description")
    total_amount: float = models.FloatField(default=0.0, verbose_name="Total Taxable Amount")
    proforma = models.ForeignKey('accounts.ProformaInvoice', on_delete=models.CASCADE, related_name='lines')

    @property
    def proforma_as_txt(self):
        if self.pk:
            return "PF" + (str(self.proforma_id).zfill(6))
        return "-"

    def __str__(self):
        return f"{self.description} against {self.proforma_as_txt}"

    class Meta:
        ordering = ('id', )


class ProformaInvoice(models.Model):
    project = models.ForeignKey('project.Project', on_delete=models.SET_NULL, null=True)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    tax = models.ForeignKey(Tax, on_delete=models.CASCADE)
    subtotal_taxable_price = models.FloatField(null=True, blank=True, verbose_name="Taxable Amount, (Excl Tax.)")
    total_amount = models.FloatField(null=True, blank=True, verbose_name="Total Amount")
    description = TextFieldAsChar(null=True, blank=True, verbose_name="Description")
    is_frozen = models.BooleanField(default=False, verbose_name="Is Finalized")
    created_at = models.DateTimeField(auto_now=True)
    submitted_at = models.DateTimeField(null=True, blank=True)

    @property
    def proforma_as_txt(self):
        if self.pk:
            return "PF" + (str(self.pk).zfill(6))
        return "-"
    
    @property
    def tax_amount(self):
        if self.pk:
            return self.total_amount - self.subtotal_taxable_price
        return '-'
    
    def _calculate_total(self):
        self.subtotal_taxable_price = self.lines.all().aggregate(
            sum=models.Sum('total_amount', output_field=models.FloatField())
        )['sum'] or 0.0
        tax_ratio = 0
        if self.tax:
            tax_ratio = (100 + self.tax.value) / 100
        self.total_amount = self.subtotal_taxable_price * tax_ratio

    @property
    def to_word(self):
        return num2words(self.total_amount, lang='en_IN')

    def save(self, **kwargs):
        if self.is_frozen and not self.submitted_at:
            self.submitted_at = datetime_safe.datetime.now()
        self._calculate_total()
        super(ProformaInvoice, self).save(**kwargs)

    def __str__(self):
        return f"{self.proforma_as_txt} {' [DRAFT]' if (self.is_frozen is False) else ''}"


class GSTInvoice(models.Model):
    project = models.ForeignKey('project.Project', on_delete=models.SET_NULL, null=True)
    invoice = models.FileField(upload_to='sale-tax-invoices/')
    amount = models.FloatField(default=0.0)


@receiver(post_save, sender=ProformaInvoiceLine, dispatch_uid="update_value_after_create")
def update_value_after_create(sender, instance, **kwargs):
    if instance.proforma:
        subtotal_taxable_price = instance.proforma.subtotal_taxable_price
        instance.proforma._calculate_total()
        if subtotal_taxable_price != instance.proforma.subtotal_taxable_price:
            instance.proforma.save()


class Branch(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

MONTHS = (
    (1, 'January'),
    (2, 'February'),
    (3, 'March'),
    (4, 'April'),
    (5, 'May'),
    (6, 'June'),
    (7, 'July'),
    (8, 'August'),
    (9, 'September'),
    (10, 'October'),
    (11, 'November'),
    (12, 'December'),
)


def year_validation(value):
    if type(value) is str:
        value = int(value)
    if not (date.today().year <= value <= date.today().year+1):
        raise ValidationError(f"Year must be between {date.today().year} and {date.today().year+1}")
    return value


def month_validation(value):
    if type(value) is str:
        value = int(value)
    if not (1 <= value <= 12):
        raise ValidationError(f"Month must  be between 1 and 12")
    return value


class InflowOutflow(models.Model):
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE, null=True, blank=True, default=0)
    net_expense = models.FloatField(help_text="in Rupees.", null=True, blank=True, default=0)
    net_billed_amount = models.FloatField(help_text="in Rupees.", null=True, blank=True, default=0)
    net_amount_converted = models.FloatField(help_text="in Rupees.", null=True, blank=True, default=0)
    net_inflow_from_aop = models.FloatField(help_text="in Rupees.", null=True, blank=True, default=0)

    no_leads_collected = models.PositiveIntegerField(default=0)
    no_positive_leads = models.PositiveIntegerField(default=0)
    no_convertions = models.PositiveIntegerField(default=0)
    no_client_visits = models.PositiveIntegerField(default=0)
    no_premises_client_meetings = models.PositiveIntegerField(default=0)

    no_expected_deliveries = models.PositiveIntegerField(default=0)
    no_done_deliveries = models.PositiveIntegerField(default=0)

    month: int = models.PositiveSmallIntegerField(choices=MONTHS, validators=[month_validation], null=True, blank=True)
    year: int = models.PositiveSmallIntegerField(validators=[year_validation], null=True, blank=True)

    @property
    def month_name(self) -> str:
        from apps.payslip.utils.calender import CALENDER
        return CALENDER.month_index_reverse(self.month)
