from django import forms
from extra_views import InlineFormSetFactory

from apps.accounts.models import ProformaInvoice, ProformaInvoiceLine


class ProformaInvoiceForm(forms.ModelForm):

    class Meta:
        model = ProformaInvoice
        fields = ['project', 'currency', 'tax', 'is_frozen']


class ProformaInvoiceLineForm(forms.ModelForm):
    class Meta:
        model = ProformaInvoiceLine
        fields = ['description', 'total_amount']


class ProformaInvoiceLineInline(InlineFormSetFactory):
    model = ProformaInvoiceLine
    form_class = ProformaInvoiceLineForm

    def get_factory_kwargs(self):
        kwargs = super(ProformaInvoiceLineInline, self).get_factory_kwargs()
        kwargs['extra'] = 3
        # kwargs['can_delete'] = False
        return kwargs

    # def construct_formset(self):
    #     formset = super(ProformaInvoiceLineInline, self).construct_formset()
    #     formset.queryset = formset.queryset.order_by('user_id')
    #     return formset
    #


