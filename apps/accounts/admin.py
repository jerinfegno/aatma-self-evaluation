from django.contrib import admin
from solo.admin import SingletonModelAdmin

from apps.accounts.models import *

from import_export.admin import ImportExportModelAdmin, ExportActionMixin


class ProformaInvoiceLineInline(admin.StackedInline):
    model = ProformaInvoiceLine


class ProformaInvoiceAdmin(ImportExportModelAdmin):
    inlines = (ProformaInvoiceLineInline, )
    pass


admin.site.register(Tax)
admin.site.register(Currency)
admin.site.register(ProformaInvoice, ProformaInvoiceAdmin)


admin.site.register(CompanyManagementConfig, SingletonModelAdmin)
admin.site.register(CompanyConfig, SingletonModelAdmin)





