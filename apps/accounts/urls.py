from django.urls import path

from apps.accounts.views import ProformaCreateView, ProformaUpdateView, ProformaListView, ProformaPreView

app_name = "accounts"

urlpatterns = [
    path('proforma/', ProformaListView.as_view(), name="proforma-list"),
    path('proforma/create/', ProformaCreateView.as_view(),
         name="proforma-create"),
    path('proforma/<int:pk>/update/', ProformaUpdateView.as_view(),
         name="proforma-update"),
    path('proforma/<int:pk>/document/', ProformaPreView.as_view(),
         name="proforma-preview"),
]

