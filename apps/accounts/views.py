from crispy_forms.helper import FormHelper
from django.urls import reverse
from django.views.generic import ListView, DetailView
from easy_pdf.rendering import render_to_pdf_response
from extra_views import UpdateWithInlinesView, CreateWithInlinesView

from apps.accounts.forms import ProformaInvoiceForm, ProformaInvoiceLineInline
from apps.accounts.models import ProformaInvoice
from apps.utils.permissions import BusinessEmployeeRequired


class ProformaPreView(BusinessEmployeeRequired, DetailView):
    queryset = ProformaInvoice.objects.all().select_related('project', 'project__client', 'tax', 'currency')
    template_name = 'accounts/proforma-invoice-template.html'

    def get_pdf_filename(self):
        return "proforma-invoice-{o.proforma_as_txt}-{o.project.short_code}.pdf".format(o=self.object)

    def get_pdf_kwargs(self):
        kwargs = super(ProformaPreView, self).get_pdf_kwargs()
        kwargs['pagesize'] = 'A4'
        kwargs['content_type'] = 'application/pdf'
        return kwargs

    def get(self, request, *args, **kwargs):
        kwargs['object'] = self.get_object(self.get_queryset())
        kwargs['pagesize'] = 'A4'
        return render_to_pdf_response(request, self.template_name, kwargs)
        # return render(request, self.template_name, kwargs)
        # return render_to_pdf(self.template_name, kwargs)
        # return super(ProformaPreView, self).get(request, *args, **kwargs)


class ProformaListView(BusinessEmployeeRequired, ListView):
    queryset = ProformaInvoice.objects.order_by('-id')
    template_name = 'accounts/proformainvoice_list.html'


class ProformaCreateView(BusinessEmployeeRequired, CreateWithInlinesView):
    model = ProformaInvoice
    inlines = [ProformaInvoiceLineInline, ]

    form_class = ProformaInvoiceForm
    template_name = 'accounts/proformainvoice_form.html'

    def get_success_url(self):
        return reverse('accounts:proforma-update', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        kwargs = super(ProformaCreateView, self).get_context_data(**kwargs)
        for formset in kwargs['inlines']:
            formset.helper = FormHelper()
            formset.helper.form_show_labels = False
        kwargs['form'] = self.get_form(self.get_form_class())
        return kwargs


class ProformaUpdateView(BusinessEmployeeRequired, UpdateWithInlinesView):
    model = ProformaInvoice
    inlines = [ProformaInvoiceLineInline, ]

    form_class = ProformaInvoiceForm
    template_name = 'accounts/proformainvoice_form.html'

    def get_success_url(self):
        return reverse('accounts:proforma-update', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        kwargs = super(ProformaUpdateView, self).get_context_data(**kwargs)
        for formset in kwargs['inlines']:
            formset.helper = FormHelper()
            formset.helper.form_show_labels = False
        kwargs['form'] = self.get_form(self.get_form_class())
        return kwargs





