from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from . models import Notification


class CommonAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Notification, CommonAdmin)

