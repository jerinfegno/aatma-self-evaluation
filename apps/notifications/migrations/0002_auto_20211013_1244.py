# Generated by Django 3.1.3 on 2021-10-13 07:14

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0011_circular_is_new'),
        ('payslip', '0015_monthlypaymentsummery_generated_at'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('notifications', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='circular',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='documents.circular'),
        ),
        migrations.AlterField(
            model_name='notification',
            name='payslip',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='payslip.attendancedata'),
        ),
        migrations.AlterField(
            model_name='notification',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]
