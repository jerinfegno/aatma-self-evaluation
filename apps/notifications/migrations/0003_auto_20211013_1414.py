# Generated by Django 3.1.3 on 2021-10-13 08:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('notifications', '0002_auto_20211013_1244'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='notification',
            name='circular',
        ),
        migrations.RemoveField(
            model_name='notification',
            name='payslip',
        ),
        migrations.AddField(
            model_name='notification',
            name='content_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='contenttypes.contenttype'),
        ),
        migrations.AddField(
            model_name='notification',
            name='is_generic',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='notification',
            name='object_id',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='notification',
            name='read_by',
            field=models.IntegerField(null=True),
        ),
    ]
