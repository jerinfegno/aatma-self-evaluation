from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_save
from apps.user.models import User
from apps.documents.models import Circular
from apps.payslip.models import AttendanceData
from django.dispatch import receiver


# Create your models here.
from django.conf import settings


class Notification(models.Model):
    # circular = models.ForeignKey(Circular, on_delete=models.SET_NULL, null=True)
    # payslip = models.ForeignKey(AttendanceData, on_delete=models.SET_NULL, null=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.SET_NULL, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    label = models.TextField(null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    is_generic = models.BooleanField(default=True)
    read_by = models.IntegerField(null=True)


@receiver(post_save, sender=AttendanceData)
def create_notification_for_payslip(created, instance, *args, **kwargs):
    if created:
        Notification.objects.create(content_object=instance, object_id=instance.id, label=f"Hey {instance.user},your payslip have been generated !!"
                                    , user=instance.user)


@receiver(post_save, sender=Circular)
def create_notification_for_circular(created, instance, *args, **kwargs):
    if created:
        Notification.objects.create(content_object=instance, object_id=instance.id, label=f"Hey there,a new circular have been generated for you!!")


