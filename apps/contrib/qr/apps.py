from django.apps import AppConfig


class QrConfig(AppConfig):
    name = 'apps.contrib.qr'
