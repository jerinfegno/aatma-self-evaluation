from django.apps import AppConfig


class SopConfig(AppConfig):
    name = 'apps.sop'
