from django.conf import settings
from django.db import models
from django_extensions.db.models import TitleSlugDescriptionModel

from apps.user.models import Department, Designation


class SOP(TitleSlugDescriptionModel):
    """
    Standard Operating Procedures
    """
    file = models.FileField()
    is_draft = models.BooleanField(default=True)
    version = models.CharField(max_length=6, default='v1.0')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='created_sops')
    managed_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='managed_sops')
    departments = models.ManyToManyField(Department, blank=True)
    designations = models.ManyToManyField(Designation, blank=True)
    is_applicable_for_all = models.BooleanField(default=False)

    def __str__(self):
        return self.title

