from django.urls import path
from django.views.generic import ListView

from apps.sop.models import SOP

urlpatterns = [
    path('', ListView.as_view(model=SOP, queryset=SOP.objects.filter(is_draft=True))),
    # path('manage/', SopAdminListView.as_view()),

]

