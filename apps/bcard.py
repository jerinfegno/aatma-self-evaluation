from django.conf.urls import url
from django.urls import path
from rest_framework import generics, serializers

from apps.user.models import BCard
from rest_framework.pagination import PageNumberPagination

class BcardResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000


class BCardSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    def get_user(self, instance):
        mob = (instance.user.official_mobile and instance.user.official_mobile) or (instance.user.mobile and instance.user.mobile)
        if mob: mob = "+" + str(mob.country_code) + str( mob.national_number)
        return instance.user and {
            'full_name': instance.user.get_full_name(),
            'avatar': self.context['request'].build_absolute_uri(instance.avatar_url),
            'designation': instance.designation or instance.user.designation.name,
            'contact': mob,
            'email': instance.user.official_email or instance.user.email,
        }

    class Meta:
        model = BCard
        fields = ('username', "user", "github_handle", "twitter_handle", "linkedin_handle", "facebook_handle", "instagram_handle")


class BCardListView(generics.ListCreateAPIView):
    queryset = BCard.objects.all().select_related('user', 'user__designation')
    serializer_class = BCardSerializer
    pagination_class = BcardResultsSetPagination
    

urlpatterns = [
    path('', BCardListView.as_view(), name="api-bcard-home"),
]





