from django.db.models import Sum
from django.utils.safestring import mark_safe
from django import template


register = template.Library()


@register.simple_tag
def in_hand_petty_amount():
    from apps.petty.models import Petty

    amount = Petty.objects.aggregate(total=Sum('amount'))['total']
    if amount > 1000:
        color = "success"
        direction = "down"
    elif amount > 0:
        color = "warning"
        direction = "down"
    else:
        color = "danger"
        direction = "up"
    return mark_safe(f"""
        <span style="font-size:24px;font-weight:600" class="text-{color}">
            <i class="fa fa-arrow-{direction}" aria-hidden="true"></i> {amount} INR </span>
            <span class="text-muted" >(IN HAND)</span>""")
