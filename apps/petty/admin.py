from django import forms
from django.contrib import admin

# Register your models here.
from django.contrib.auth import get_user_model
from django.utils.datetime_safe import date
from django.utils.safestring import mark_safe

from apps.petty.models import Petty
from import_export.admin import ImportExportModelAdmin, ExportActionMixin


class CommonAdmin(ImportExportModelAdmin):
    pass


User = get_user_model()


class PettyAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PettyAdminForm, self).__init__( *args, **kwargs)
        if 'created_by' in self.fields.keys():
            self.fields['created_by'].queryset = User.objects.filter(is_superuser=True).exclude(username__in=('admin', ))
        self.fields['description'].widget.attrs.update({'rows': 4})
        for field in self.fields.keys():
            self.fields[field].widget.attrs.update({'class': 'form-control w-75'})

    def clean_amount(self):
        if self.cleaned_data['direction'] == Petty.OUT:
            amount = self.cleaned_data['amount']
            petty_amt = Petty.petty_amount() or 0
            if petty_amt < amount:
                raise forms.ValidationError(mark_safe(f"Amount of <b> Rs. {amount}</b > could not be with drawn. <br /> Available amount: <b> Rs. {petty_amt}</b> "))
        return self.cleaned_data['amount']

    class Meta:
        model = Petty
        exclude = ('cash_in_hand', )


class PettyAdmin(CommonAdmin):
    list_display = ('amount_display', 'direction', 'description', 'created_at', )
    list_filter = ('direction', 'created_by')
    date_hierarchy = 'created_at'
    form = PettyAdminForm
    readonly_fields = ('cash_in_hand', )
    fields = ('direction', 'amount', 'description', 'created_at', 'created_by',
              'paid_to', 'voucher_number', 'file_01', 'file_02', 'file_03')

    def get_readonly_fields(self, request, obj=None):
        if request.user.username in ['admin']:
            return ()
        return ('created_by', )

    def amount_display(self, obj):
        dir = {Petty.IN: "down", Petty.OUT: "up"}[obj.direction]
        
        color = {Petty.IN: "success", Petty.OUT: "danger"}[obj.direction]
        return mark_safe(f"""
            <span class="text-{color}">
                <i class="fa fa-arrow-{dir}" aria-hidden="true"></i> {str(obj)}
            </span>""")

    def save_model(self, request, obj, form, change):
        if not obj.created_by:
            obj.created_by = request.user
        obj.cash_in_hand = Petty.petty_amount()
        obj.save()


admin.site.register(Petty, PettyAdmin)














