from datetime import date, timedelta

from django.conf import settings
from django.db.models import Sum
from django.urls import reverse
from django.utils import timezone
from django.views.generic import ArchiveIndexView, UpdateView, ListView
from django.views.generic.edit import ModelFormMixin

from apps.petty.admin import PettyAdminForm
from apps.petty.models import Petty
import xlwt
from django.http import HttpResponse


class PettyList(ModelFormMixin, ListView):
    model = Petty
    form_class = PettyAdminForm
    date_field = 'created_at'
    ordering = 'created_at'
    allow_empty = True
    object = None
    today = date.today()

    def get_queryset(self):
        if self.request.method == 'GET':
            range_date = (self.get_first_date(), self.get_first_of_next_month())
            return super(PettyList, self).get_queryset().filter(created_at__range=range_date)
        return super(PettyList, self).get_queryset()

    _tm = None
    _tmnext = None

    def get_first_date(self):
        if self._tm is None:
            self._tm = date(day=1, month=self.get_month(), year=self.get_year())
        return self._tm

    def get_first_of_next_month(self):
        if self._tmnext is None:
            month = self.get_month()
            year = self.get_year()
            next_month = date(day=28, month=month, year=year) + timedelta(days=10)
            next_month_starting = date(day=1, month=next_month.month, year=next_month.year)
            self._tmnext = next_month_starting
        return self._tmnext

    def get_last_date(self):
        next_first = self.get_first_of_next_month()
        return next_first - timedelta(days=1)

    def get_month(self):
        return int(self.request.GET.get('month', self.today.month))

    def get_year(self):
        return int(self.request.GET.get('year', self.today.year))

    def get_context_data(self, *, object_list=None, **kwargs):
        month = self.get_month()
        year = self.get_year()
        kwargs['MONTH'] = str(month)
        kwargs['YEAR'] = str(year)
        kwargs['SELECTED_MONTH'] = self.get_first_date()
        kwargs['TODAY'] = date.today()
        kwargs['year_range'] = range(2020, kwargs['TODAY'].year + 1)
        kwargs['CASH_IN_HAND'] = Petty.petty_amount() or 0
        kwargs['NET_CREDIT'] = Petty.net_credit(month, year) or 0
        kwargs['NET_DEBIT'] = Petty.net_debit(month, year) or 0
        kwargs['BROUGHT_FORWARD'] = Petty.brought_forward(month, year) or 0
        kwargs['CLOSING_BALANCE'] = Petty.closing_balance(month, year) or 0
        self.object_list = self.get_queryset()
        kwargs['CREDIT_LIST'] = self.object_list.filter(direction=Petty.IN)
        kwargs['DEBIT_LIST'] = self.object_list.filter(direction=Petty.OUT)
        return super(PettyList, self).get_context_data(**kwargs, )

    def get_success_url(self):
        return reverse('petty:dashboard-petty') + '?month=' + str(self.get_month()) + '&year=' + str(self.get_year())

    def post(self, request, *args, **kwargs):
        form = self.get_form(self.get_form_class())
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class PettyUpdate(UpdateView):
    model = Petty
    fields = '__all__'


def export_petty_xls(request, month, year):
    month_name = {
        1: 'January',
        2: 'February',
        3: 'March',
        4: 'April',
        5: 'May',
        6: 'June',
        7: 'July',
        8: 'august',
        9: 'September',
        10: 'October',
        11: 'November',
        12: 'December',
    }
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = f'attachment; filename="Petty-{month_name[month]}-{year}-Fegno-Technologies.xls"'
    sheet_name = f'Petty {month_name[month]} {year}'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(sheet_name)

    # Sheet header, first row
    row_num = 0

    title_font_style = xlwt.XFStyle()
    title_font_style.font.bold = True
    title_font_style.alignment.horz = title_font_style.alignment.HORZ_CENTER

    title_font_bordered = xlwt.XFStyle()
    title_font_bordered.alignment.horz = title_font_bordered.alignment.HORZ_CENTER
    title_font_bordered.borders.top = title_font_bordered.borders.THIN
    title_font_bordered.borders.bottom = title_font_bordered.borders.THIN
    title_font_bordered.font.bold = True

    ws.write_merge(1, 1, 0,  11, 'Fegno Technologies', title_font_bordered)
    ws.write_merge(2, 2, 0,  11, f'PETTY CASH CLAIM NO 1 - MONTHLY STATEMENT REPORT - PETTY EXPENSES {month_name[month]} {year}', title_font_bordered)

    def write_with_style(rowid, r, bold=False, border_top=False, border_bottom=False):
        style = xlwt.XFStyle()
        style.borders.top = style.borders.THIN if border_top else style.borders.NO_LINE
        style.borders.bottom = style.borders.THIN if border_bottom else style.borders.NO_LINE

        style_left = xlwt.XFStyle()
        style_left.borders.top = style.borders.top
        style_left.borders.bottom = style.borders.bottom
        style_left.borders.left = style.borders.THIN

        style_right = xlwt.XFStyle()
        style_right.borders.top = style.borders.top
        style_right.borders.bottom = style.borders.bottom
        style_right.borders.right = style.borders.THIN

        style.font.bold = bold
        style_left.font.bold = bold
        style_right.font.bold = bold

        ws.write(rowid, 0, r[0], style_left)    # DATE
        ws.write(rowid, 1, r[1], style)         # RECIEPT
        ws.write(rowid, 2, r[2], style)         # AMT
        ws.write(rowid, 3, r[3], style_left)         # Attachment
        ws.write(rowid, 4, r[4], style_left)        # Date
        ws.write(rowid, 5, r[5], style)             # Description
        ws.write(rowid, 6, r[6], style)             # Voucher NO
        ws.write(rowid, 7, r[7], style)             # Cash
        ws.write(rowid, 8, r[8], style)             # Paid to
        ws.write(rowid, 9, r[9], style_left)            # Att 01
        ws.write(rowid, 10, r[10], style)               # att 02
        ws.write(rowid, 11, r[11], style_right)         # att 03

        title_font_style.font.bold = False

    headings = ['DATE', 'RECEIPT DETAILS', 'AMOUNT', 'Attachment 01',
                'DATE', 'PAYMENT DESCRIPTION', 'Voucher No', 'CASH', 'Paid To',
                'Attachment 01', 'Attachment 02', 'Attachment 03']

    write_with_style(rowid=3, r=headings, bold=True)
    title_font_style.borders.top = title_font_style.borders.NO_LINE

    petty_credit_for_month = Petty.petty_credit_for_month(month, year).values(
        'amount', 'description', 'created_at',
        'file_01', 'file_02', 'file_03',
    )
    petty_debit_for_month = Petty.petty_debit_for_month(month, year).values(
        'amount', 'description', 'created_at', 'paid_to',
        'voucher_number', 'file_01', 'file_02', 'file_03',
    )
    max_len = max(len(petty_credit_for_month), len(petty_debit_for_month))

    for row_index in range(max_len):
        row_num = 4 + row_index
        cdt = petty_credit_for_month[row_index] if len(petty_credit_for_month) > row_index else {}
        dbt = petty_debit_for_month[row_index] if len(petty_debit_for_month) > row_index else {}
        _ = lambda name: request.build_absolute_uri(settings.MEDIA_URL + dbt.get(name, '')) if dbt.get(name, None) else None
        print(cdt.get('created_at', '-'))
        row = [
            #  credit 1-4
            cdt.get('created_at', ''),
            cdt.get('description', ''),
            cdt.get('amount', ''),
            _('file_01'),

            #  debit 5-11
            dbt.get('created_at', ''),
            dbt.get('description', ''),
            dbt.get('voucher_number', ''),
            dbt.get('amount', ''),
            dbt.get('paid_to', ''),

            _('file_01'),
            _('file_02'),
            _('file_03'),

        ]
        write_with_style(rowid=row_num, r=row)
    write_with_style(rowid=max_len + 4, r=[' ' for __ in range(12)], bold=False)
    write_with_style(rowid=max_len + 5, r=[' ' for __ in range(12)], bold=False)
    write_with_style(rowid=max_len + 6, r=[' ' for __ in range(12)], bold=False)
    write_with_style(rowid=max_len + 7, r=[' ' for __ in range(12)], bold=False)
    write_with_style(rowid=max_len + 8, r=[' ' for __ in range(12)], bold=False)
    write_with_style(rowid=max_len + 9, r=[' ' for __ in range(12)], bold=False)
    write_with_style(rowid=max_len + 10, r=[' ' for __ in range(12)], bold=False)
    write_with_style(rowid=max_len + 11, r=[' ' for __ in range(12)], bold=False)

    footer_style = xlwt.XFStyle()
    footer_style.borders.top = footer_style.borders.THIN
    footer_style.borders.bottom = footer_style.borders.THIN
    footer_style.font.bold = True

    row_num = max_len + 11
    credit_sum = Petty.petty_credit_for_month(month, year).aggregate(tot=Sum('amount'))['tot'] or 0
    debit_sum = Petty.petty_debit_for_month(month, year).aggregate(tot=Sum('amount'))['tot'] or 0

    total_row = [
        '', 'Total', str(credit_sum), '',
        '', 'Total', '', str(debit_sum), '', '', '', '', '', '', '', '', '', '',
    ]
    write_with_style(rowid=row_num + 1, r=total_row, bold=True, border_top=True, border_bottom=True)

    title_font_style.borders.bottom = title_font_style.borders.THIN
    total_row = [
        '', 'Petty Cash Closing Balance', str(Petty.closing_balance(month, year)), '',
        '', '', '', '', '', '', '', '', '', '', '', '', '', '',
    ]
    write_with_style(rowid=row_num + 2, r=total_row, bold=True, border_top=True, border_bottom=True)

    wb.save(response)
    return response
