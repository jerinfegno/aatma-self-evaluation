# Generated by Django 3.1.3 on 2020-12-10 10:48

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('petty', '0003_auto_20201204_1731'),
    ]

    operations = [
        migrations.AlterField(
            model_name='petty',
            name='created_at',
            field=models.DateField(blank=True, default=django.utils.timezone.now),
        ),
    ]
