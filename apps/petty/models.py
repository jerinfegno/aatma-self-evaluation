from datetime import timedelta

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.datetime_safe import date

today = date.today()


class Petty(models.Model):
    OUT = "Debit"
    IN = "Credit"
    direction = models.CharField(default=OUT, choices=[(OUT, OUT), (IN, IN)], max_length=10,
                                 verbose_name="Transaction Type")
    amount = models.FloatField(default=0)
    cash_in_hand = models.FloatField(default=0)
    description = models.TextField(null=True, verbose_name="Particulars")
    created_at = models.DateField(default=timezone.now, blank=True, db_index=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
    paid_to = models.CharField(max_length=32, null=True, blank=True)
    voucher_number = models.CharField(max_length=32, null=True, blank=True)
    file_01 = models.FileField(upload_to='petty/', null=True, blank=True)
    file_02 = models.FileField(upload_to='petty/', null=True, blank=True)
    file_03 = models.FileField(upload_to='petty/', null=True, blank=True)

    def save(self, **kwargs):
        self.amount = abs(self.amount)
        if self.direction == self.OUT:
            self.amount *= -1
        if self.cash_in_hand is None:
            qs = Petty.objects
            if self.pk:
                qs = qs.filter(id__lte=self.pk)
            self.cash_in_hand = qs.aggregate(total=models.Sum('amount'))['total'] or 0
        self.amount = (-1 if self.direction == self.OUT else 1) * abs(self.amount)
        super(Petty, self).save(**kwargs)

    def __str__(self):
        return f"{self.direction} of Rs. {self.amount} on {self.created_at}"

    @classmethod
    def closing_balance(cls, month=today.month, year=today.year):
        next_month = date(day=28, month=month, year=year) + timedelta(days=10)
        next_month_starting = date(day=1, month=next_month.month, year=next_month.year)
        return Petty.objects.filter(created_at__lt=next_month_starting).aggregate(total=models.Sum('amount'))['total'] or 0

    @classmethod
    def closing_balance_before_date(cls, day=today.day,  month=today.month, year=today.year):
        selected_date = date(day=day, month=month, year=year) + timedelta(days=10)
        return Petty.objects.filter(created_at__lt=selected_date).aggregate(total=models.Sum('amount'))['total'] or 0

    @classmethod
    def petty_amount(cls):
        return Petty.objects.aggregate(total=models.Sum('amount'))['total']

    @classmethod
    def brought_forward(cls, month, year):
        first = date(day=1, month=month, year=year)
        return Petty.objects.filter(created_at__lt=first).aggregate(total=models.Sum('amount'))['total'] or 0

    @classmethod
    def net_credit(cls, month, year):
        return Petty.objects.filter(created_at__month=month, created_at__year=year, direction=Petty.IN).aggregate(total=models.Sum('amount'))['total'] or 0

    @classmethod
    def net_debit(cls, month, year):
        return Petty.objects.filter(created_at__month=month, created_at__year=year, direction=Petty.OUT).aggregate(total=models.Sum('amount'))['total'] or 0

    @classmethod
    def brought_forward_this_month(cls):
        td = date.today()
        return cls.brought_forward(td.month, td.year)

    @classmethod
    def petty_credit_for_month(cls, month, year):
        return cls.objects.filter(direction=cls.IN, created_at__month=month, created_at__year=year)

    @classmethod
    def petty_debit_for_month(cls, month, year):
        return cls.objects.filter(direction=cls.OUT, created_at__month=month, created_at__year=year)

    @classmethod
    def petty_credit_for_this_month(cls):
        td = date.today()
        return cls.petty_credit_for_month(td.month, td.year)

    @classmethod
    def petty_debit_for_this_month(cls):
        td = date.today()
        return cls.petty_debit_for_month(td.month, td.year)

    class Meta:
        # app_label = 'apps.petty'
        ordering = ('created_at', )






