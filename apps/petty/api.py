from datetime import date, timedelta

from django.db.models import Sum
from django.shortcuts import get_object_or_404
from django.utils import datetime_safe
from rest_framework import viewsets, serializers
from rest_framework.response import Response

from apps.authentication import CSRFExemptSessionAuthentication
from apps.petty.models import Petty
from apps.user.models import User


class PettySerializer(serializers.ModelSerializer):
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())
    added_by = serializers.SerializerMethodField()

    class Meta:
        model = Petty
        fields = ('id', 'direction', 'amount', 'description', 'created_at', 'created_by', 'added_by', 'paid_to',
                  'voucher_number', 'file_01', 'file_02', 'file_03')

    def validate_created_at(self, created_at):
        return created_at or datetime_safe.date.today()

    def validate(self, attrs):
        if attrs['direction'] == Petty.OUT:
            petty_then = Petty.closing_balance_before_date(day=attrs['created_at'].day,
                                                           month=attrs['created_at'].month,
                                                           year=attrs['created_at'].year)
            if attrs['amount'] > petty_then:
                raise serializers.ValidationError(f'You do not have enough petty amount in hand then to '
                                                  f'Debit Rs. {attrs["amount"]}!'
                                                  f' Available amount at date ({attrs["created_at"]}) is : Rs.{petty_then}')
        return attrs

    def save(self, **kwargs):
        return super(PettySerializer, self).save(created_by=self.context['request'].user, **kwargs)

    def get_added_by(self, instance):
        try:
            return instance.created_by and (instance.created_by.get_full_name() or instance.created_by.username)
        except Exception as e:
            return "System"


class PettyViewSet(viewsets.ModelViewSet):
    queryset = Petty.objects.all().order_by('-id')
    serializer_class = PettySerializer
    authentication_classes = [CSRFExemptSessionAuthentication, ]

    def filter_queryset(self, queryset):
        qs = super(PettyViewSet, self).filter_queryset(queryset)
        if self.request.method == 'GET' and 'pk' not in self.kwargs.keys():
            return qs.filter(
                created_at__month=self.kwargs['month'], created_at__year=self.kwargs['year'], )
        return qs

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        this_month = date(day=1, month=self.kwargs['month'], year=self.kwargs['year'])
        cash_in_hand_now = Petty.petty_amount() or 0
        opening_balance = Petty.objects.filter(created_at__lt=this_month).aggregate(total=Sum('amount'))['total'] or 0
        _credit = []
        _credit_sum = 0
        _debit = []
        _debit_sum = 0
        for petty in serializer.data:
            if petty['direction'] == Petty.IN:
                _credit.append(petty)
                _credit_sum += petty['amount']
            else:
                _debit.append(petty)
        opening_balance = Petty(
            direction=Petty.IN,
            amount=opening_balance,
            description="Opening Balance",
            created_at=this_month,
            created_by=User(first_name="System")
        )

        opn_bal = self.get_serializer(instance=opening_balance)
        cash_in_hand_prev_month = Petty.brought_forward(this_month.month, this_month.year) or 0
        return Response({
            'credit': [opn_bal.data] + _credit,
            'debit': _debit,
            'net_cash_credited': cash_in_hand_prev_month + Petty.net_credit(this_month.month, this_month.year),
            'net_cash_debited': abs(Petty.net_debit(this_month.month, this_month.year)),
            'cash_in_hand_now_month': cash_in_hand_now,
            'cash_in_hand_last_month': cash_in_hand_prev_month,
        })



