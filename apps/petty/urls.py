from django.urls import path, include

from apps.petty.api import PettyViewSet
from apps.petty.views import PettyList, PettyUpdate, export_petty_xls

app_name = 'petty'

petty_list = PettyViewSet.as_view({'get': 'list', 'post': 'create'})
petty_detail = PettyViewSet.as_view({'get': 'retrieve', 'patch': 'partial_update', 'put': 'update', 'delete': 'destroy'})


api_urlpatterns = [
    path('petty/<int:month>/<int:year>/', petty_list, name="petty-list"),
    path('petty/<int:pk>/', petty_detail, name="petty-detail"),
]


urlpatterns = [
    path('petty/', PettyList.as_view(), name="dashboard-petty"),
    path('petty/<int:month>/<int:year>/download/excel', export_petty_xls, name="download-petty-excel"),
    path('petty/<int:pk>/', PettyUpdate.as_view(), name="dashboard-petty-detail"),
    path('api/v1/', include(api_urlpatterns)),
]
