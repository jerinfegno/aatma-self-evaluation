from django.contrib.auth.mixins import UserPassesTestMixin
from django.urls import reverse_lazy


class LoginRequired(UserPassesTestMixin):
    permission_denied_message = 'Restricted Access!'
    login_url = reverse_lazy('dashboard-index')

    def test_func(self):
        return self.request.user.is_authenticated

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        return super().dispatch(request, *args, **kwargs)


class FieldRequired(LoginRequired):
    permission_fields = []

    def get_permission_fields(self):
        return self.permission_fields

    def test_func(self):
        u = self.request.user
        return super(FieldRequired, self).test_func() and any([getattr(u, f) for f in self.get_permission_fields()])


class ManagerRequired(FieldRequired):
    permission_fields = ['is_manager', ]


class HRRequired(FieldRequired):
    permission_fields = ['is_hr', ]


class BusinessEmployeeRequired(FieldRequired):
    permission_fields = ['is_in_business', ]


class OperationsEmployeeRequired(FieldRequired):
    permission_fields = ['is_in_operations', ]


def manager_required(user): return user.is_authenticated and user.is_manager
def hr_required(user): return user.is_authenticated and user.is_hr
def operations_required(user): return user.is_authenticated and user.is_in_operations
def business_required(user): return user.is_authenticated and user.is_in_business

