from django import forms
from django.conf import settings
from django.forms import DateField, DateTimeField, TimeField, Textarea, ModelForm
from django.forms.utils import ErrorList


class BootstrapFormMixin(object):
    """
    Sample Django Form To add Bootstrap's Form Control class to every field.
    """
    def __init__(self, request=None, *args, **kwargs):
        if request:
            self.request = request
        super(BootstrapFormMixin, self).__init__(*args,  **kwargs)
        for field in getattr(self, 'fields', []):
            field_attr = self.fields[field].widget.attrs
            field_attr.update({
                'class': field_attr.get('class', '') + ' form-control form-control-alternative ',
                'placeholder': self.fields[field].label or field
            })
            if type(self.fields[field]) == DateField:
                field_attr['class'] += ' datepicker'
                field_attr['data-date-format'] = "dd-mm-yyyy"
                field_attr['autocomplete'] = "off"
                self.fields[field].widget.foramt = settings.DATE_INPUT_FORMATS[0]
                self.fields[field].input_formats = settings.DATE_INPUT_FORMATS

            elif type(self.fields[field]) == TimeField:
                field_attr['class'] += ' timepicker'
                field_attr['autocomplete'] = "off"

            elif type(self.fields[field]) == DateTimeField:
                field_attr['class'] += ' datetimepicker'
                field_attr['data-date-format'] = "dd-mm-yyyy"
                field_attr['autocomplete'] = "off"
                self.fields[field].widget.foramt = settings.DATE_INPUT_FORMATS[0]
                self.fields[field].input_formats = settings.DATE_INPUT_FORMATS

            elif type(self.fields[field]) == Textarea:
                field_attr['rows'] = '2'


class BootstrapModelForm(BootstrapFormMixin, ModelForm):
    pass
