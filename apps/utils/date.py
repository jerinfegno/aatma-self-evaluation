from datetime import date


def next_occurrence(event_date):
    if not event_date:
        return
    today = date.today()
    next_occ = date(year=today.year, month=event_date.month, day=event_date.day)
    if next_occ < today:
        next_occ = date(year=today.year + 1, month=event_date.month, day=event_date.day)
    return next_occ
