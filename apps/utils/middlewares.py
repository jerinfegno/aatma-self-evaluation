from django.conf import settings
from django.http import Http404
from django.shortcuts import redirect
from django.utils.deprecation import MiddlewareMixin
from django_hosts import reverse_lazy
from rest_framework.reverse import reverse


class UserAccessRestrictionMiddleware(MiddlewareMixin):
    """
    This class is used to redirect those users out of restricred users.
    """

    def _onboarding_access_verification(self, request):
        return request.user.is_authenticated and request.user.is_non_employee_candidate

    def _oauth_access_verification(self, request):
        return True

    def _fegnomin_access_verification(self, request):
        return request.user.is_authenticated and (
            request.user.is_superuser
            or request.user.is_staff
        )

    def process_request(self, request):
        func_name = f'_{request.host.name}_access_verification'
        if hasattr(self, func_name):
            access = getattr(self, func_name)(request)
            if not access:
                home_url = f'get_{request.host.name}_homepage'
                return
        if request.user.is_authenticated and request.user.is_non_employee_candidate and request.host.name != 'onboarding':
            recruitment_home_url = reverse_lazy('onboarding:home', host='onboarding')
            return redirect(recruitment_home_url)
            # skippable_url_startswith = (
            #     settings.STATIC_URL, settings.MEDIA_URL, '/ckeditor', '/accounts/', '/__debug__/'
            # )
            # path = request.path
            # for listed_url_startswith in self.skippable_url_startswith
            #     if path.startswith(listed_url_startswith):
            #         break
            # else:
            #     # enter only if break won't hit.
            #
            #     if not path.startswith(recruitment_home_url):
            #         return redirect(recruitment_home_url)
            #
