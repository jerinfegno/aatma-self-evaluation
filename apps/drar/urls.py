from datetime import datetime

from django.urls import path, register_converter, include

from apps.drar.views import *


class DateConverter:
    regex = r'\d{2}-\d{2}-\d{4}'

    def to_python(self, value):
        try:
            return datetime.strptime(value, '%d-%m-%Y').date()
        except Exception as e:
            raise Http404("This Date is not Valid.")

    def to_url(self, value):
        return value


register_converter(DateConverter, 'date_pattern')

app_name = 'drar'

urlpatterns = [
    path('drar/', include([
        path('', DrarManageView.as_view(), name="drar-today"),
        path('<date_pattern:date>/', DrarManageView.as_view(), name="drar-create"),
        path('<date_pattern:date>/note-<int:pk>/', DrarManageView.as_view(), name="drar-update"),
        path('<date_pattern:date>/note-<int:pk>/completed/', DrarCompleteView.as_view(), name="drar-complete-task"),
        path('<date_pattern:date>/note-<int:pk>/delete/', DrarDeleteView.as_view(), name="drar-delete"),
        path('<date_pattern:date>/note-<int:pk>/copy/', CopyDrarTask.as_view(), name="copy-drar-task"),
        path('configuration/', DrarConfigurationView.as_view(), name="drar-configuration"),
        path('shared/', UsersSharedDrarListView.as_view(), name="shared-drar-users"),
        path('shared/emp-<int:user_pk>/', SharedDrarListView.as_view(), name="shared-drar-list"),
        path('shared/emp-<int:user_pk>/<date_pattern:date>/', SharedDrarListView.as_view(), name="shared-drar-dated-list"),
    ])),
]
