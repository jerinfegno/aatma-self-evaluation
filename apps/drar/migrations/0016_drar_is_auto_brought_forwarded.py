# Generated by Django 3.1.3 on 2022-08-28 12:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drar', '0015_drarcomment'),
    ]

    operations = [
        migrations.AddField(
            model_name='drar',
            name='is_auto_brought_forwarded',
            field=models.BooleanField(default=False),
        ),
    ]
