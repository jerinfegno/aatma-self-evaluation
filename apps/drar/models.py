import datetime
from django.utils.timezone import localtime

from django.conf import settings
from django.db import models
from django.templatetags.static import static
from django.utils.safestring import mark_safe


class DrarConfiguration(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    who_all_can_view = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name="Submit to",  blank=True, related_name='viewable_drar_configs')

    @classmethod
    def get_config(cls, user):
        config, created = cls.objects.get_or_create(user=user)
        if created:
            if user.reporting_to:
                config.who_all_can_view.add(user.reporting_to)
            if user.department.head:
                config.who_all_can_view.add(user.department.head)
        return config


class DrarIntermediateStatusUpdate(models.Model):
    drar = models.ForeignKey('drar.Drar', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    is_done = models.BooleanField(default=False)

    class Meta:
        db_table = 'drar_drar_linked_with'


class Drar(models.Model):
    """
    There is no grouping entity such as Drar & DrarItems.
    Instead, Drar is grouped with the combination of User & Date Together.
    """
    title = models.CharField(max_length=512)
    is_completed = models.BooleanField(default=False)
    is_auto_brought_forwarded = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=datetime.date.today)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='drars')
    linked_with = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name="Needs Support From...",
                                         through=DrarIntermediateStatusUpdate, through_fields=('drar', 'user'),  blank=True, related_name='assigned_drars')
    copied_from = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL, )
    copied_for = models.SmallIntegerField(default=0)
    complete_within = models.TimeField(default=datetime.time(hour=6, minute=30))
    priority = models.PositiveSmallIntegerField(default=2, choices=(
        (1, 'High'), (2, 'Normal'), (3, 'Least'),
    ))

    def __str__(self):
        return f'{(self.user and self.user.get_full_name()) or "Unassigned"} -> {self.title} ({self.created_at})'

    def drar_status_img_html(self):
        size = '20'
        if self.is_completed:
            img = static('images/tick.jpg')
        elif self.created_at.date() < datetime.datetime.utcnow().date():
            img = static('images/cross.jpg')
        else:
            size = '60'
            img = static('images/inprogress.gif')
        return mark_safe(f'<img src="{img}" alt="tick" width="{size}" height="{size}" loading="lazy">')

    @property
    def date_str(self):
        return localtime(self.created_at).strftime('%d-%m-%Y')

    @property
    def is_todays_task(self):
        return localtime(self.created_at).date() == datetime.date.today()

    drar_status_img_html.allow_tags = True

    def clone_post_process(self):
        """
        to be called, if this drar is clonned from a previous drar.
        """
        disu_array = []
        drar = self.copied_from
        if not drar:
            return
        for disu in DrarIntermediateStatusUpdate.objects.filter(user_id__in=self.copied_from.linked_with.all(), drar=drar):
            disu.pk = disu.id = None
            disu.drar = self
            disu_array.append(disu)
        DrarIntermediateStatusUpdate.objects.bulk_create(disu_array, ignore_conflicts=True)

        dc_array = []
        for dc in DrarComment.objects.filter(drar=drar):
            dc.pk = dc.id = None
            dc.drar = self
            dc_array.append(dc)
        DrarComment.objects.bulk_create(dc_array, ignore_conflicts=True)


class DrarComment(models.Model):
    drar = models.ForeignKey('drar.Drar', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    comment = models.TextField()
    created_at = models.DateTimeField(default=datetime.datetime.now)


class DrarReadReciept(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='drar_reiept')
    read_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='read_reiept')
    created_at = models.DateTimeField(auto_created=True)
    created_on = models.DateField(auto_created=True, default=datetime.datetime.now)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user', 'read_by', 'created_on'], name='need_just_one_read_reciept_for_a_day')
        ]
