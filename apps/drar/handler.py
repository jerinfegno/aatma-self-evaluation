from django.db import IntegrityError
from django.utils.datetime_safe import date, datetime
from django.db.models import Q, Max
from django.utils.functional import cached_property

from apps.drar.models import Drar, DrarConfiguration, DrarReadReciept, DrarIntermediateStatusUpdate, DrarComment
from apps.user.models import User
from django.forms import model_to_dict


class DrarAgent(object):

    def __init__(self, user, search_date=None):
        if search_date is None:
            search_date = date.today()
        self.user = user
        self.date = search_date
        self.today = date.today()

    def accessable(self):
        return Drar.objects.filter(
            id__in=Drar.objects.filter(Q(user=self.user) | Q(linked_with=self.user), created_at__date=self.date)
        ).prefetch_related('linked_with').select_related('user', 'copied_from')

    def my_tasks(self):
        return Drar.objects.filter(Q(user=self.user))

    def editable(self):
        return Drar.objects.filter(user=self.user, created_at__date=self.today).prefetch_related('linked_with').select_related('user', 'copied_from')

    @staticmethod
    def clone(drar):
        pass

    def brought_forward(self):
        last_date = Drar.objects.all().filter(user=self.user, created_at__date__lt=self.today).aggregate(last_drar=Max('created_at__date'))['last_drar']
        if last_date:
            last_date = last_date.date()
            for drar in Drar.objects.filter(user=self.user, created_at__contains=last_date, is_completed=False):
                kwargs = model_to_dict(drar, exclude=['id'])
                kwargs['priority'] = max(kwargs['priority']-1, 1)
                kwargs['copied_for'] += 1
                kwargs['copied_from'] = drar
                kwargs['user_id'] = kwargs.pop('user')
                kwargs['is_auto_brought_forwarded'] = True
                kwargs['created_at'] = datetime.now()

                if 'id' in kwargs:
                    kwargs.pop('id')
                linked_with = ()
                if 'linked_with' in kwargs:
                    linked_with = kwargs.pop('linked_with')
                new_drar = Drar.objects.create(**kwargs)
                new_drar.clone_post_process()

    @cached_property
    def _shared_users(self):
        through_qs = DrarConfiguration.who_all_can_view.through.objects.filter(user=self.user)
        return DrarConfiguration.objects.filter(
            id__in=through_qs.values_list('drarconfiguration_id')
        ).values_list('user_id', flat=True)

    def drar_shared_users(self):
        return User.objects.filter(is_manager=False, is_active=True, id__in=self._shared_users)

    def shared_with_me(self, user_pk):
        print(self._shared_users)
        if user_pk in self._shared_users:
            try:
                # TODO: pass Unique Constraint Exception
                DrarReadReciept.objects.create(user_id=user_pk, read_by=self.user, created_at=datetime.now())
            except IntegrityError as e:
                if 'need_just_one_read_reciept_for_a_day' in str(e):
                    pass
                else:
                    raise e
            return Drar.objects.filter(Q(user_id=user_pk) | Q(linked_with=user_pk), created_at__date=self.date).select_related('user', 'copied_from').prefetch_related('linked_with')

        return Drar.objects.none()

