from django.apps import AppConfig


class DrarConfig(AppConfig):
    name = 'apps.drar'
