from datetime import time

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.forms import model_to_dict
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DeleteView, DetailView
from django.views.generic import UpdateView
from django.utils.datetime_safe import date, datetime

from apps.drar.handler import DrarAgent
from apps.drar.models import Drar, DrarConfiguration, DrarReadReciept, DrarComment
from apps.user.models import User


@method_decorator(login_required, name='dispatch')
class DrarDeleteView(DeleteView):
    template_name = 'drar/drar_delete.html'

    def get_queryset(self):
        return DrarAgent(self.request.user).editable()

    def get_success_url(self):
        messages.success(self.request, "The Drar Task has been removed!")
        return reverse('drar:drar-create', kwargs={"date": self.object.date_str})


@method_decorator(login_required, name='dispatch')
class CopyDrarTask(DetailView):
    template_name = 'drar/drar_delete.html'

    def get_queryset(self):
        return DrarAgent(self.request.user).my_tasks()

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.request.GET.get('copy_to'):
            _date = datetime.strptime(self.request.GET.get('copy_to'), '%y-%m-%d')
            print(_date)
            kwargs = model_to_dict(self.object, exclude=['id'])
            kwargs['priority'] = max(kwargs['priority'] - 1, 1)
            kwargs['copied_for'] += 1
            kwargs['copied_from'] = self.object
            kwargs['user_id'] = kwargs.pop('user')
            kwargs['is_auto_brought_forwarded'] = False
            kwargs['created_at'] = datetime.combine(_date.date(), self.object.complete_within)
            new_drar = Drar.objects.create(
                **kwargs
            )
            self.object.complete_within = time(hour=23, minute=59, second=30)
            self.object.priority = 3
            self.object.save()
            new_drar.clone_post_process()
            messages.success(request, f"DRAR task has been copied to {self.request.GET.get('copy_to')}'s List")

        else:
            kwargs = model_to_dict(self.object, exclude=['id'])
            kwargs['priority'] = max(kwargs['priority'] - 1, 1)
            kwargs['copied_for'] += 1
            kwargs['copied_from'] = self.object
            kwargs['user_id'] = kwargs.pop('user')
            kwargs['is_auto_brought_forwarded'] = False
            kwargs['created_at'] = datetime.now()
            new_drar = Drar.objects.create(
                **kwargs
            )
            new_drar.clone_post_process()
            messages.success(request, "DRAR task has been copied to today's List")
        return redirect('drar:drar-today')


@method_decorator(login_required, name='dispatch')
class DrarCompleteView(UpdateView):
    template_name = 'drar/drar_form.html'

    def get_queryset(self):
        return DrarAgent(self.request.user).editable()

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return redirect(self.get_success_url())

    def get_success_url(self):
        messages.success(self.request, "Marked as Completed!")
        if self.object.created_at.date() == datetime.utcnow().date():
            return reverse('drar:drar-today')
        return reverse('drar:drar-create', kwargs={"date": self.object.date_str})

    @property
    def fields(self):
        return 'is_completed',


@method_decorator(login_required, name='dispatch')
class DrarManageView(UpdateView):
    template_name = 'drar/drar_form.html'

    # def dispatch(self, request, *args, **kwargs):
    #     _date = self.kwargs.get('date')
    #     if _date and _date > date.today():
    #         raise Http404("You cannot parse through future dates.")
    #     return super(DrarManageView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        response = super().form_valid(form)
        if self.object.linked_with.filter(user=self.request.user).exists():
            self.object.linked_with.remove(self.request.user)
        return response

    def get_context_data(self, **kwargs):
        kwargs['read_receipts'] = set(DrarReadReciept.objects.all().exclude(user=self.request.user))
        kwargs['object_list'] = self.get_queryset().order_by('complete_within', 'id')
        kwargs['is_today'] = self.kwargs.get('date', date.today()) == date.today()
        kwargs['date_object'] = self.kwargs.get('date', date.today())
        kwargs['date_object__js'] = self.kwargs.get('date', date.today()).strftime("%m/%d/%Y")
        kwargs['date_object__js_datepicker'] = self.kwargs.get('date', date.today()).strftime("%YH-%m-%d")
        kwargs['date_object__py'] = self.kwargs.get('date', date.today()).strftime("%d-%m-%Y")
        kwargs['shared_users'] = DrarAgent(self.request.user).drar_shared_users()
        kwargs['datediff'] = (self.kwargs.get('date', date.today()) - date.today()).days

        return super(DrarManageView, self).get_context_data(**kwargs)

    @property
    def fields(self):
        return 'title', 'linked_with', "priority", "complete_within", 'created_at'

    def get_queryset(self):
        if self.request.method == 'GET':
            _date = self.kwargs.get('date')
            return DrarAgent(self.request.user, search_date=_date).accessable()
        else:
            return DrarAgent(self.request.user).editable()

    def get_success_url(self):
        if self.object.created_at.date() == datetime.utcnow().date():
            return reverse('drar:drar-today')
        return reverse('drar:drar-create', kwargs={"date": self.object.date_str})

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        if 'pk' in self.kwargs:
            return get_object_or_404(queryset, pk=self.kwargs['pk'], user=self.request.user)
        return None

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        brf_agent = DrarAgent(self.request.user)
        if self.kwargs.get('date', date.today()) == date.today() and not brf_agent.editable().filter(is_auto_brought_forwarded=True).exists():       # if do not have any brought forwarded task
            brf_agent.brought_forward()
        return super().post(request, *args, **kwargs)


@method_decorator(login_required, name='dispatch')
class DrarConfigurationView(UpdateView):
    queryset = DrarConfiguration.objects.all()
    fields = ('who_all_can_view', )
    template_name = 'drar/drarconfiguration_form.html'

    def get_success_url(self):
        messages.success(self.request, "Successfully updated the configuraton!")
        return reverse('drar:drar-today')

    def get_object(self, queryset=None):
        return DrarConfiguration.get_config(self.request.user)


@method_decorator(login_required, name='dispatch')
class UsersSharedDrarListView(ListView):
    queryset = User.objects.filter()
    template_name = 'drar/user_list.html'

    def get_queryset(self):
        return DrarAgent(self.request.user).drar_shared_users()


@method_decorator(login_required, name='dispatch')
class SharedDrarListView(ListView):
    template_name = 'drar/shared_drar_list.html'

    def dispatch(self, request, *args, **kwargs):
        _date = self.kwargs.get('date')
        # if _date and _date > datetime.utcnow().date():
        #     raise Http404("You cannot parse through future dates.")
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        _date = self.kwargs.get('date')
        user_pk = self.kwargs.get('user_pk')
        DrarReadReciept.objects.get_or_create(read_by=self.request.user, user_id=user_pk, created_on=_date or datetime.now().date())
        return DrarAgent(self.request.user, search_date=_date).shared_with_me(user_pk=user_pk)
    
    def get_object(self):
        self.object = None
        if 'task' in self.request.GET:
            self.object = get_object_or_404(self.get_queryset(), pk=self.request.GET.get('task'))
        return self.object

    def post(self, request, *args, **kwargs):
        drar_task = self.get_object()
        user = self.request.user
        comment = self.request.POST.get('comment')
        DrarComment.objects.create(drar=drar_task, user=user, comment=comment)
        return redirect("shared-drar-list", kwargs=self.kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        user_pk = self.kwargs.get('user_pk')
        kwargs['shared_user'] = get_object_or_404(User.objects, pk=user_pk)
        object_list = self.get_queryset()
        kwargs['read_receipts'] = set(DrarReadReciept.objects.all().filter(user=self.request.user))
        kwargs['object_list'] = self.get_queryset().order_by('id')
        kwargs['object'] = self.get_object()
        kwargs['is_today'] = self.kwargs.get('date', date.today()) == date.today()
        kwargs['date_object'] = self.kwargs.get('date', date.today())
        kwargs['date_object__js'] = self.kwargs.get('date', date.today()).strftime("%m/%d/%Y")
        kwargs['date_object__js_datepicker'] = self.kwargs.get('date', date.today()).strftime("%YH-%m-%d")
        kwargs['date_object__py'] = self.kwargs.get('date', date.today()).strftime("%d-%m-%Y")
        kwargs['shared_users'] = DrarAgent(self.request.user).drar_shared_users()
        kwargs['datediff'] = (self.kwargs.get('date', date.today()) - date.today()).days
        return super().get_context_data(**kwargs)


