
def settings(request):
    from django.conf import settings
    return {'settings': settings}


def notification(request):
    from apps.notifications.models import Notification
    from apps.user.models import User
    circular = Notification.objects.all().order_by('-created_at')[:10]
    data = {'circular': circular}
    return data