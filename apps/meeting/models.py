from django.contrib.auth import get_user_model
from django.db import models
from django.utils.datetime_safe import date

today = date.today()


class GeneralMeeting(models.Model):
    title = models.CharField(max_length=256)
    meeting_schedule = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)


class GeneralMeetingMinute(models.Model):
    title = models.CharField(max_length=1024)
    meeting = models.ForeignKey(GeneralMeeting, on_delete=models.CASCADE)

# =================================================================================


class DailyStandUpMeeting(models.Model):
    meeting_schedule = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)


class StandUpMeetingMinute(models.Model):
    meeting = models.ForeignKey(DailyStandUpMeeting, on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    task = models.CharField(max_length=1024)
    status = models.CharField(default='NEW', max_length=20, choices=[
        ('NEW', 'NEW'),
        ('COMPLETED', 'COMPLETED'),
        ('LATE_COMPLETED', 'LATE_COMPLETED'),
    ])


