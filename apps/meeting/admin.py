from django.contrib import admin

# Register your models here.
from import_export.admin import ImportExportModelAdmin, ExportActionMixin

from apps.meeting.models import StandUpMeetingMinute, DailyStandUpMeeting, GeneralMeeting, GeneralMeetingMinute


class GeneralMeetingMinuteInline(admin.TabularInline):
    model = GeneralMeetingMinute
    fields = ('title', )


class GeneralMeetingAdmin(ImportExportModelAdmin):
    inlines = [GeneralMeetingMinuteInline]


class StandUpMeetingMinuteInline(admin.TabularInline):
    model = StandUpMeetingMinute
    fields = ('user', 'task', 'status')


class DailyStandUpMeetingAdmin(ImportExportModelAdmin):
    inlines = [StandUpMeetingMinuteInline]


admin.site.register(DailyStandUpMeeting, DailyStandUpMeetingAdmin)
admin.site.register(GeneralMeeting, GeneralMeetingAdmin)
