# Generated by Django 3.1.3 on 2021-06-14 07:34

import apps.generic.models.address
import apps.utils.fields
from django.db import migrations, models
import django.db.models.deletion
import gst_field.modelfields
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contact_01', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None)),
                ('contact_01_holder_name', models.CharField(blank=True, max_length=256, null=True)),
                ('contact_02', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None)),
                ('contact_02_holder_name', models.CharField(blank=True, max_length=256, null=True)),
                ('address_line_1', apps.utils.fields.TextFieldAsChar(blank=True, help_text='Billing Address, Line 1, (Office Name)', null=True, verbose_name='Line 1')),
                ('address_line_2', apps.utils.fields.TextFieldAsChar(blank=True, help_text='Billing Address, Line 2, (Street Name )', null=True, verbose_name='Line 2')),
                ('address_line_3', apps.utils.fields.TextFieldAsChar(blank=True, help_text='Billing Address, Line 3, (City, [PinCode/Zipcode])', null=True, verbose_name='Line 3')),
                ('address_line_4', apps.utils.fields.TextFieldAsChar(blank=True, help_text='Billing Address, Line 4 (State (State Code), Country)', null=True, verbose_name='Line 4')),
                ('pincode', models.CharField(blank=True, max_length=6, null=True, validators=[apps.generic.models.address.pincode_validator])),
                ('name', models.CharField(max_length=256)),
                ('gst_in', gst_field.modelfields.GSTField(blank=True, max_length=15, null=True)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('short_code', models.CharField(max_length=4, verbose_name='Short Code')),
                ('concern_person', models.CharField(blank=True, max_length=256, null=True)),
                ('concern_number', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None)),
                ('is_active', models.BooleanField(default=True)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='project.client')),
            ],
        ),
    ]
