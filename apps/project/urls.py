from django.urls import path, include

from apps.project.views import (
    ProjectListView, ProjectCreateView, ProjectUpdateView, ProjectActivationView,
    ClientListView, ClientCreateView, ClientUpdateView, HostingListView, HostingCreateView, HostingUpdateView,
    BillingStatusCreateView, BillingStatusDeleteView, ProspectCreateView, ProspectDeleteView, InflowListView,
    InflowUpdateView, InflowDetailView, BranchUpdateView, BranchDeleteView
)

app_name = "project"

urlpatterns = [
    path('project/', include([
        path('', ProjectListView.as_view(), name="project-list"),
        path('create/', ProjectCreateView.as_view(),
             name="project-create"),
        path('<int:pk>/update/', ProjectUpdateView.as_view(),
             name="project-update"),
        path('<int:pk>/activation/', ProjectActivationView.as_view(),
             name="project-activation"),
    ])),
    path('project/client/', include([
        path('', ClientListView.as_view(), name="client-list"),
        path('create/', ClientCreateView.as_view(),
             name="client-create"),
        path('<int:pk>/update/', ClientUpdateView.as_view(),
             name="client-update"),
    ])),
    path('project/hosting/', include([
        path('', HostingListView.as_view(), name="hosting-list"),
        path('create/', HostingCreateView.as_view(),
             name="hosting-create"),
        path('<int:pk>/update/', HostingUpdateView.as_view(),
             name="hosting-update"),
    ])),
    path('project/billing/', include([
        path('', BillingStatusCreateView.as_view(), name="create_billing"),
        path('update/<int:pk>/', BillingStatusCreateView.as_view(), name="update_billing"),
        path('delete/<int:pk>/', BillingStatusDeleteView.as_view(), name="delete_billing"),
    ])),
    path('project/prospects/', include([
        path('', ProspectCreateView.as_view(), name="create_prospect"),
        path('update/<int:pk>/', ProspectCreateView.as_view(), name="update_prospect"),
        path('delete/<int:pk>/', ProspectDeleteView.as_view(), name="delete_prospect"),
    ])),
    path('project/inflow-outflow/', include([
        path('', InflowListView.as_view(), name="inflow_list"),
        path('update/<int:pk>/', InflowUpdateView.as_view(), name="inflow_update"),
        path('detail/<int:pk>/', InflowDetailView.as_view(), name="inflow_detail"),
        # path('delete/<int:pk>/', InflowDeleteView.as_view(), name="inflow_delete"),
    ])),
    path('project/branch/', include([
        path('', BranchUpdateView.as_view(), name="branch_list"),
        path('update/<int:pk>/', BranchUpdateView.as_view(), name="branch_update"),
        path('delete/<int:pk>/', BranchDeleteView.as_view(), name="branch_delete"),
    ]))
]

