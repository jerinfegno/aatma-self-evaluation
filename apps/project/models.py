from datetime import datetime
from django.db import models
from gst_field.modelfields import GSTField
from phonenumber_field.modelfields import PhoneNumberField

from apps.generic.models.address import AddressAbstract
from apps.user.models import User
from fims import settings


class Client(AddressAbstract, models.Model):
    name = models.CharField(max_length=256)
    gst_in = GSTField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=256)
    short_code = models.CharField(max_length=4, verbose_name="Short Code")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, )
    concern_person = models.CharField(max_length=256, null=True, blank=True)
    concern_number = PhoneNumberField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    @property
    def state(self):
        return 'Activated' if self.is_active else 'Closed'

    def __str__(self):
        return self.name


class HostingService(models.Model):
    name = models.CharField(max_length=256)
    service_provider = models.CharField(max_length=256)
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True, blank=True)
    date_of_hosting = models.DateField()
    date_of_expiry = models.DateField()
    amount = models.FloatField(null=True)

    def __str__(self):
        return self.name



"""
Below contains models  created for business related prospects and Billing status
"""


class BillingStatus(models.Model):
    STATE_CHOICES = [
        ('SALES', 'SALES'),
        ('DEVELOPMENT', 'DEVELOPMENT'),
        ('AMC', 'AMC')
    ]
    name = models.CharField(max_length=255)
    state = models.CharField(max_length=255, choices=STATE_CHOICES)
    display_order = models.IntegerField(default=20)

    class Meta:
        ordering = ('display_order',)

    def __str__(self):
        return self.name


class Prospect(models.Model):
    name = models.CharField(max_length=255, help_text="Project or client name")
    amount = models.FloatField(null=True, blank=True)
    status = models.ForeignKey(BillingStatus, on_delete=models.CASCADE, null=True, blank=True)
    assigned_to = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateField(auto_now_add=True)
    target_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-id',)

