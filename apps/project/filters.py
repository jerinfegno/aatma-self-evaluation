from datetime import datetime, date

from django_filters import FilterSet, ModelChoiceFilter, ChoiceFilter

from apps.project.models import Prospect


class ProspectSearchFilter(FilterSet):
    TODAY = date.today()
    year_range = range(2020, TODAY.year + 1)
    YEAR_LIST = [(val, val) for val in year_range]
    _tm = None
    _tmnext = None

    MONTH_CHOICES = (
        # ('ALL', 'ALL'),
        ('01', 'JANUARY'),
        ('02', 'FEBRUARY'),
        ('03', 'MARCH'),
        ('04', 'APRIL'),
        ('05', 'MAY'),
        ('06', 'JUNE'),
        ('07', 'JULY'),
        ('08', 'AUGUST'),
        ('09', 'SEPTEMBER'),
        ('10', 'OCTOBER'),
        ('11', 'NOVEMBER'),
        ('12', 'DECEMBER'),
    )
    now = datetime.now()

    target_date = ChoiceFilter(choices=MONTH_CHOICES, method='target_date_filter', label='Search By Target Month')
    target_year = ChoiceFilter(choices=YEAR_LIST, method='target_year_filter', label='Search By Target Year')

    class Meta:
        model = Prospect
        fields = ['target_date', 'target_year']

    def target_date_filter(self, queryset, name, value):
        prosp = queryset.select_related('status').filter(target_date__month=value).all()
        return prosp

    def target_year_filter(self, queryset, name, value):
        # range_date = (self.get_first_date(), self.get_last_date())
        # print(range_date)
        prosp = queryset.select_related('status').filter(target_date__year=value).all()
        return prosp

    # def get_first_date(self):
    #     if self._tm is None:
    #         self._tm = date(day=1, month=self.get_month(), year=self.get_year())
    #     return self._tm
    #
    # def get_first_of_next_month(self):
    #     if self._tmnext is None:
    #         month = self.get_month()
    #         year = self.get_year()
    #         next_month = date(day=28, month=month, year=year) + datetime.timedelta(days=10)
    #         next_month_starting = date(day=1, month=next_month.month, year=next_month.year)
    #         self._tmnext = next_month_starting
    #     return self._tmnext
    #
    # def get_last_date(self):
    #     next_first = self.get_first_of_next_month()
    #     return next_first - datetime.timedelta(days=1)
    #
    # def get_month(self):
    #     return int(self.request.GET.get('month', self.today.month))
    #
    # def get_year(self):
    #     return int(self.request.GET.get('year', self.today.year))


