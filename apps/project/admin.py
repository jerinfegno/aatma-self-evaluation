from django.contrib import admin

# Register your models here.
from apps.project.models import HostingService, Project, Client, Prospect, BillingStatus

from import_export.admin import ImportExportModelAdmin, ExportActionMixin


class CommonAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Client, CommonAdmin)
admin.site.register(Project, CommonAdmin)
admin.site.register(HostingService, CommonAdmin)


admin.site.register(Prospect, CommonAdmin)
admin.site.register(BillingStatus, CommonAdmin)


