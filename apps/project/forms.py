from django.core.exceptions import ValidationError
from django.forms import ModelForm, DateField, DateInput, ChoiceField
from django.utils.datetime_safe import date


from apps.accounts.models import month_validation, year_validation, InflowOutflow
from apps.payslip.utils.calender import CALENDER
from apps.project.models import Prospect
from apps.utils.forms import BootstrapFormMixin


class DatePickerInput(DateInput):
    input_type = 'date'


class ProspectForm(ModelForm):
    target_date = DateField(
        widget=DatePickerInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Prospect
        fields = ('name', 'amount', 'assigned_to', 'status', 'target_date')


class InflowOutflowGenerator(BootstrapFormMixin, ModelForm):
    class Meta:
        model = InflowOutflow
        fields = ('month', 'year')

    month = ChoiceField(choices=CALENDER.MONTHS, validators=[month_validation])
    year = ChoiceField(choices=[(i, i) for i in range(date.today().year+1, date.today().year-1, -1)],
                       validators=[year_validation])

    def clean_month(self):
        return int(self.cleaned_data['month'])

    def clean_year(self):
        return int(self.cleaned_data['year'])

    def clean(self):
        cleaned_data = self.cleaned_data
        cleaned_year = int(cleaned_data['year'])
        cleaned_month = int(cleaned_data['month'])
        exists = InflowOutflow.objects.filter(year=cleaned_year, month=cleaned_month).exists()
        InflowOutflow.objects.get_or_create(year=int(cleaned_data['year']), month=int(cleaned_data['month']))
        if exists:
            raise ValidationError("Inflow Outflow already Generated")
        return cleaned_data


class InflowOutflowUpdateViewForm(ModelForm):
    class Meta:
        model = InflowOutflow
        exclude = ('month','year',)