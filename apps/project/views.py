import json
from datetime import date
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.urls import reverse, reverse_lazy
from django.views.generic import UpdateView, ListView, DeleteView, DetailView
from django.views.generic.edit import FormMixin, ProcessFormView
from extra_views import SuccessMessageMixin


from apps.accounts.models import InflowOutflow, Branch
from apps.project.filters import ProspectSearchFilter
from apps.project.forms import ProspectForm, InflowOutflowGenerator, InflowOutflowUpdateViewForm
from apps.project.models import Client, Project, HostingService, BillingStatus, Prospect
from apps.utils.permissions import OperationsEmployeeRequired


class ClientListView(ListView):
    queryset = Client.objects.order_by('-id')


class ClientUpdateView(OperationsEmployeeRequired, UpdateView):
    model = Client
    fields = '__all__'
    success_message = "%(name)s was created successfully"

    def get_success_url(self):
        return reverse('project:client-list')


class ClientCreateView(ClientUpdateView):
    success_message = "%(name)s was created successfully"

    def get_object(self, queryset=None):
        return

    def get_success_url(self):
        return reverse('project:project-create')


class HostingListView(OperationsEmployeeRequired, ListView):
    queryset = HostingService.objects.order_by('-id')


class HostingUpdateView(UpdateView):
    model = HostingService
    fields = '__all__'
    success_message = "%(name)s was created successfully"

    def get_success_url(self):
        return reverse('project:hosting-list')


class HostingCreateView(HostingUpdateView):
    success_message = "%(name)s was created successfully"

    def get_object(self, queryset=None):
        return

    def get_success_url(self):
        return reverse('project:hosting-create')


class ProjectListView(OperationsEmployeeRequired, ListView):
    queryset = Project.objects.order_by('-id')


class ProjectUpdateView(OperationsEmployeeRequired, SuccessMessageMixin, UpdateView):
    model = Project
    fields = '__all__'
    success_message = "%(name)s was updated successfully"

    def get_success_url(self):
        return reverse('project:project-list')


class ProjectActivationView(ProjectUpdateView):
    fields = ('is_active',)
    success_message = "%(name)s was %(state) successfully"


class ProjectCreateView(ProjectUpdateView):
    success_message = "%(name)s was created successfully"

    def get_object(self, queryset=None):
        return None

    def get_success_url(self):
        return reverse('project:project-update', kwargs={'pk': self.object.pk})


"""
Below contains Views for business related prospects and Billing status
"""


class BillingStatusCreateView(OperationsEmployeeRequired, UpdateView):
    model = BillingStatus
    fields = ['name', 'state', 'display_order']
    template_name = 'project/billing_create.html'
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(BillingStatusCreateView, self).get_context_data(**kwargs)
        context['object_list'] = BillingStatus.objects.all()
        return context

    def get_success_url(self):
        return reverse('project:create_billing')

    def get_object(self, queryset=None):
        if 'pk' in self.kwargs:
            return super(BillingStatusCreateView, self).get_object()
        return None

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        self.object = self.get_object(self.object_list)
        context = self.get_context_data()
        return self.render_to_response(context)


class BillingStatusDeleteView(OperationsEmployeeRequired, DeleteView):
    model = BillingStatus

    def get_success_url(self):
        return reverse_lazy('project:create_billing')


class ProspectCreateView(OperationsEmployeeRequired, UpdateView):
    model = Prospect
    template_name = 'project/prospect_page.html'
    form_class = ProspectForm
    allow_empty = True
    object = None
    today = date.today()
    is_annual = True

    def get_success_url(self):
        return reverse('project:create_prospect')

    def get_object(self, queryset=None):
        if 'pk' in self.kwargs:
            return super(ProspectCreateView, self).get_object(queryset=Prospect.objects.select_related('status').filter(id=self.kwargs['pk']))
        return None

    def get(self, request, *args, **kwargs):
        # import pdb;pdb.set_trace()
        self.object_list = self.get_queryset()
        self.object = self.get_object(self.object_list)
        context = self.get_context_data()
        if self.object is not None:
            context['filterdata'] = ProspectSearchFilter(self.request.GET, Prospect.objects.select_related('status').filter(id=self.object.pk))
        else:
            context['filterdata'] = ProspectSearchFilter(self.request.GET, Prospect.objects.select_related('status'))
        page = self.request.GET.get('page', 1)
        paginator = Paginator(context['filterdata'].qs, 20)
        try:
            page_obj = paginator.page(page)
        except PageNotAnInteger:
            page_obj = paginator.page(1)
        except EmptyPage:
            page_obj = paginator.page(paginator.num_pages)
        context['filter'] = page_obj
        return self.render_to_response(context)


class ProspectDeleteView(OperationsEmployeeRequired, DeleteView):
    model = Prospect

    def get_success_url(self):
        return reverse_lazy('project:create_prospect')


"""Below contains Inflow Outflow"""


class InflowListView(OperationsEmployeeRequired, FormMixin, ListView, ProcessFormView):
    model = InflowOutflow
    template_name = "project/inflow_outflow_page.html"
    form_class = InflowOutflowGenerator
    http_method_names = ('get', 'post')
    object = None
    object_list = None
    ordering = ('-year', '-month')

    def get_success_url(self):
        return reverse('project:inflow_list')

    def post(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return super(InflowListView, self).post(request, *args, **kwargs)

    # def form_valid(self, form):
    #     pc = InflowOutflowConstructor(inflow_outflow_class=InflowOutflow, **form.cleaned_data)
    #     pc.calculate()
    #     self.object = pc.inflow_outflow
    #     return HttpResponseRedirect(self.get_success_url())

    def get(self, request, *args, **kwargs):

        self.object_list = self.get_queryset()
        return super(InflowListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        inflowdata = InflowOutflow.objects.all()

        dlist = []
        for data in inflowdata:
            d = dict()
            d['x'] = data.month_name
            d['net_expense'] = data.net_expense
            d['net_billed_amount'] = data.net_billed_amount
            d['net_amount_converted'] = data.net_amount_converted
            d['net_inflow_from_aop'] = data.net_inflow_from_aop
            dlist.append(d)
        context['monthlylabel'] = json.dumps(dlist)
        print(context['monthlylabel'])
        return context


class InflowUpdateView(OperationsEmployeeRequired, UpdateView):
    model = InflowOutflow
    form_class = InflowOutflowUpdateViewForm
    template_name = "project/inflow_outflow_list.html"

    def get_success_url(self):
        return reverse('project:inflow_list')


class InflowDetailView(OperationsEmployeeRequired, DetailView):
    model = InflowOutflow
    # form_class = InflowOutflowUpdateViewForm
    fields = '__all__'
    template_name = "project/inflow_outflow_detail.html"

    def get_queryset(self):
        return super(InflowDetailView, self).get_queryset()

    def get_success_url(self):
        return reverse('project:inflow_list')


class BranchUpdateView(OperationsEmployeeRequired, UpdateView):
    model = Branch
    fields = '__all__'
    template_name = 'project/branch_create_list.html'

    def get_success_url(self):
        return reverse('project:branch_list')

    def get_object(self, queryset=None):
        print("get_object", self.kwargs)
        if 'pk' in self.kwargs:
            return super(BranchUpdateView, self).get_object(queryset=Branch.objects.filter(id=self.kwargs['pk']))
        return None

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        self.object = self.get_object(self.object_list)
        context = self.get_context_data()
        if self.object is not None:
            context['object_list'] = Branch.objects.filter(id=self.object.pk)
        else:
            context['object_list'] = Branch.objects.all()
        print("get", self.object_list, context)
        return self.render_to_response(context)


class BranchDeleteView(OperationsEmployeeRequired, DeleteView):
    model = Branch

    def get_success_url(self):
        return reverse_lazy('project:branch_list')