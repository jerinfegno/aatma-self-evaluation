from django.apps import AppConfig


class AtsConfig(AppConfig):
    name = 'apps.ats'
