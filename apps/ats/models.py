from django.conf import settings
from django.db import models

default_resume_status_order = 28                # to be kept to handle migrations!
default_resume_status_order_lazy = 28           # to be kept to handle migrations!


class ResumeSource(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Vacency(models.Model):
    name = models.CharField(max_length=30)
    positions_filled = models.PositiveSmallIntegerField(default=0)
    no_of_positions = models.PositiveSmallIntegerField(default=1)

    def __str__(self):
        return f"{self.name}, {self.positions_filled} / {self.no_of_positions}"


class ResumeStatus(models.Model):
    name = models.CharField(max_length=80)
    order = models.PositiveSmallIntegerField(default=28)
    state = models.PositiveSmallIntegerField(choices=[
        (1, 'To do'),
        (2, 'In Progress'),
        (3, 'Done'),
    ])

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('order', )


class Candidate(models.Model):
    name = models.CharField(max_length=80)
    against = models.ForeignKey(Vacency, on_delete=models.SET_NULL, null=True)
    current_status = models.TextField(null=True, blank=True)
    resume = models.FileField(upload_to='ats/resumes/', null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    mobile = models.CharField(null=True, blank=True, max_length=18)
    source = models.ForeignKey(ResumeSource, on_delete=models.SET_NULL, null=True, blank=True)
    status = models.ForeignKey(ResumeStatus, on_delete=models.SET_NULL, null=True)
    location = models.CharField(null=True, blank=True, max_length=18)

    assigned_to = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)
    next_meeting = models.DateTimeField(null=True, blank=True)
    current_exp = models.FloatField(default=1.0)
    current_ctc = models.FloatField(null=True, blank=True)
    expected_ctc = models.FloatField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.name}, {self.location or ''} ({self.email or 'no email provided '})"


