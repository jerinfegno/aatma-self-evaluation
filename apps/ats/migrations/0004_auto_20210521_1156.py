# Generated by Django 3.1.3 on 2021-05-21 06:26

import apps.ats.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ats', '0003_auto_20210521_1154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resumestatus',
            name='order',
            field=models.PositiveSmallIntegerField(default=apps.ats.models.default_resume_status_order),
        ),
    ]
