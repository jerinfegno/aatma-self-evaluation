# Generated by Django 3.1.3 on 2021-05-21 06:57

import apps.ats.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ats', '0006_auto_20210521_1205'),
    ]

    operations = [
        migrations.AddField(
            model_name='candidate',
            name='next_meeting',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='resumestatus',
            name='order',
            field=models.PositiveSmallIntegerField(default=apps.ats.models.default_resume_status_order),
        ),
    ]
