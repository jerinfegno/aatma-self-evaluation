from django import forms
from django.contrib import admin
from django.contrib.admin import widgets
from django.forms import Textarea

from apps.ats.models import *

admin.site.register(ResumeSource)
admin.site.register(ResumeStatus)
admin.site.register(Vacency)


class CandidateAdmin(admin.ModelAdmin):
    list_display = ['name', 'against', 'current_ctc', 'current_exp', 'status', 'next_meeting', 'assigned_to', 'current_status', 'source', ]
    list_filter = ['location', 'against', 'status', 'assigned_to', 'source']
    list_editable = ['status', 'assigned_to', 'current_status']
    date_hierarchy = 'created_at'
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 2, 'cols': 40})},
    }



admin.site.register(Candidate, CandidateAdmin)
