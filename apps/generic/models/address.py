from django import forms
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.safestring import mark_safe
from gst_field.modelfields import GSTField
from phonenumber_field.modelfields import PhoneNumberField

from apps.utils.fields import TextFieldAsChar


def pincode_validator(value):
    if not len(value) == 6:
        raise ValidationError("Invalid Postcode")


class AddressAbstract(models.Model):

    contact_01 = PhoneNumberField(null=True, blank=True)
    contact_01_holder_name = models.CharField(max_length=256, null=True, blank=True, help_text="Contact holder name and relation")
    contact_02 = PhoneNumberField(null=True, blank=True)
    contact_02_holder_name = models.CharField(max_length=256, null=True, blank=True, help_text="Contact holder name and relation")

    address_line_1 = TextFieldAsChar(
        null=True, blank=True,
        help_text="Line 1, (Office/House Name)", verbose_name="Line 1",
    )
    address_line_2 = TextFieldAsChar(
        null=True, blank=True,
        help_text="Line 2, (Street Name )", verbose_name="Line 2",
    )
    address_line_3 = TextFieldAsChar(
        null=True, blank=True,
        help_text="Line 3, (City, [PinCode/Zipcode])", verbose_name="Line 3",
    )
    address_line_4 = TextFieldAsChar(
        null=True, blank=True,
        help_text="Line 4 (State (State Code), Country)", verbose_name="Line 4",
    )
    pincode = models.CharField(max_length=6, null=True, blank=True, validators=[pincode_validator])

    def address_as_text(self):
        return f"{self.address_line_1},\n {self.address_line_2},\n {self.address_line_3} - {self.pincode},\n {self.address_line_4}"

    def address_as_html(self):
        return mark_safe('<br />'.join(self.address_as_text().split('\n')))

    class Meta:
        abstract = True





