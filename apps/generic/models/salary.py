from django.db import models


class SalaryAbstractModel(models.Model):
    """
    net_pay = Total salary ; <Manual entry>;
    
    basic_pay = 40% of net_pay;
    dearness_allowance = 20% of net_pay;
    house_rent_allowance = 20% of net_pay;
    travelling_allowance = 20% of net_pay;
    provident_fund = 0 ; <Manual entry>;
    employee_state_insurance = 0 ; <Manual entry>;
    tax_deducted_at_source = 0 ; <Manual entry>;
    """

    net_pay: float = models.FloatField(
        default=0.0,
        help_text="Total Payment per month (30 days scale)",
    )
    basic_pay: float = models.FloatField(
        default=0.0, verbose_name="Basic Pay", blank=True,
        help_text="Basic Payment")
    dearness_allowance: float = models.FloatField(
        default=0.0, verbose_name="DA", blank=True,
        help_text="Dearness Allowance (20% of Basic Pay)")
    house_rent_allowance: float = models.FloatField(
        default=0.0, verbose_name="HRA", blank=True,
        help_text="House Rent Allowance,  Section 10(13A) of the Income Tax Act, (20% of Basic Pay)")
    travelling_allowance: float = models.FloatField(
        default=0.0, verbose_name="TA", blank=True,
        help_text="Fees and Act 1951, (20% of Basic Pay)")

    provident_fund: float = models.FloatField(
        default=0.0, verbose_name="PF", blank=True,
        help_text="As Per Provident Fund Scheme, Funds and Miscellaneous Provisions Act, 1952.")
    employee_state_insurance: float = models.FloatField(
        default=0.0, verbose_name="ESI", blank=True,
        help_text="As per Employees State Insurance Act, 1948 - ESIC")
    tax_deducted_at_source: float = models.FloatField(
        default=0.0, verbose_name="TDS", blank=True,
        help_text="As per Tax Deducted at Source, Income Tax Department of India")

    addable_payment = ('basic_pay', 'dearness_allowance', 'house_rent_allowance', 'travelling_allowance',)
    deductible_payment = ('provident_fund', 'employee_state_insurance', 'tax_deducted_at_source',)
    addable_payment_percentage = (.40, .20, .20, .20,)
    deductible_payment_percentage = (0.0, 0.0, 0.0, 0.0,)

    @property
    def gross_pay(self) -> float:
        out = 0
        for field in self.addable_payment:
            out += getattr(self, field)
        return out

    @property
    def gross_deduction(self) -> float:
        out = 0
        for field in self.deductible_payment:
            out += getattr(self, field)
        return out

    def save(self, *args, **kwargs):
        if self.net_pay and not any((getattr(self, field) for field in self.addable_payment)):
            self._calculate_default_payment_distribution()
        super(SalaryAbstractModel, self).save(*args, **kwargs)

    def _calculate_default_payment_distribution(self):
        for field, percentage in zip(self.addable_payment, self.addable_payment_percentage):
            setattr(self, field, percentage * self.net_pay)
        for field, percentage in zip(self.deductible_payment, self.deductible_payment_percentage):
            setattr(self, field, percentage * self.net_pay)

    class Meta:
        abstract = True
