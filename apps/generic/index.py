import datetime
from datetime import date

from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.edit import ProcessFormView, BaseUpdateView

from apps.leave.models import LeaveRequest, Holiday, RestrictedHolidayGroup, WFHRequest
from apps.user.models import User, Profile
from apps.utils.forms import BootstrapModelForm
from apps.utils.permissions import LoginRequired
from django.contrib.auth.views import LoginView as CoreLoginView


def trigger_error(request):
    division_by_zero = 1 / 0


class UserProfileForm(BootstrapModelForm):

    class Meta:
        fields = ['facebook_handle', 'instagram_handle', 'linkedin_handle', 'twitter_handle', 'github_handle', 'fegno_github_handle']
        model = Profile

    # def __init__(self, *args, **kwargs):
    #     super(UserProfileForm, self).__init__( *args, **kwargs)
        # self.fields['dob'].widget = forms.DateInput(format="%d-%m-%Y")
        # self.fields['doj'].widget = forms.DateInput(format="%d-%m-%Y")


class LoginView(CoreLoginView):
    form_class = AuthenticationForm
    template_name = "paper/login.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        import uuid                 # noqa
        uuid = uuid.uuid4()
        _id = str(uuid)
        context['uuid'] = uuid
        context['event_name'] = settings.PUSHER_LOGIN_EVENT_NAME.format(_id)
        return context


class HomeView(LoginRequired, SuccessMessageMixin, BaseUpdateView, TemplateView):
    template_name = 'index.html'
    form_class = UserProfileForm
    login_url = settings.LOGIN_URL
    success_message = "Your Profile has been updated!! "

    def get_object(self, queryset=None):
        return Profile.objects.get_or_create(user=self.request.user)[0]

    def get_success_url(self):
        return reverse('dashboard-index')

    @staticmethod
    def delta_for_next_occurrence(event_date):
        today = date.today()
        next_occ = date(year=today.year, month=event_date.month, day=event_date.day)
        if next_occ < today:
            next_occ = date(year=today.year + 1, month=event_date.month, day=event_date.day)
        return next_occ - today

    def get_context_data(self, **kwargs):
        users = User.objects.filter(is_active=True, )
        today = date.today()

        def get_data(qs, field, size=4, next_occurrence=True, _filter_cond=lambda x: x, reverse=False):
            return sorted(
                filter(lambda user: _filter_cond(getattr(user, field)), qs),
                key=lambda user: self.delta_for_next_occurrence(getattr(user, field)) if next_occurrence else getattr(user, field), reverse=reverse)[:size]

        new_joinies_from = datetime.date.today() - datetime.timedelta(weeks=4)
        date_filter = lambda x: x and x >= new_joinies_from
        date_filter_upto_yesterday = lambda x: x and datetime.date.today() > x

        kwargs['new_joinies'] = get_data(users, field='doj', size=100, next_occurrence=False, _filter_cond=date_filter, reverse=True)
        # kwargs['birthdays'] = get_data(users, field='dob')
        kwargs['workanniversary'] = get_data(users, field='doj', _filter_cond=date_filter_upto_yesterday)
        # kwargs['weddinganniversary'] = get_data(users, field='dom')
        kwargs['leaves'] = LeaveRequest.objects.filter(
            Q(from_date__gte=today)&Q(to_date__lte=today), approval_status__in=[LeaveRequest.APPROVED, LeaveRequest.WAITING_FOR_APPROVAL])
        kwargs['leaves'] = kwargs['leaves'].select_related('user').order_by('-from_date')[:8]
        kwargs['wfh_today'] = WFHRequest.objects.filter(wfh_date=today, approval_status__in=[WFHRequest.APPROVED, WFHRequest.WAITING_FOR_APPROVAL])

        kwargs['holidays'] = Holiday.objects.filter(
            date__gte=today, date__year=today.year
        ).select_related('restricted_group').order_by('date')[:10]

        return super(HomeView, self).get_context_data(**kwargs)
