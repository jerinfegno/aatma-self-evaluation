from rest_auth.views import LoginView as CoreLoginView
from rest_framework import status
from rest_framework.response import Response

from apps.user.serializers import UserSerializer


class LoginView(CoreLoginView):
    authentication_classes = ()

    def get_response(self):
        response = Response({
            'user': UserSerializer(instance=self.user).data
        }, status=status.HTTP_200_OK)
        return response
