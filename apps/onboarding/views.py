from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django_hosts.resolvers import reverse, reverse_lazy


from django.utils.decorators import method_decorator
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.generic import TemplateView, UpdateView, ListView, DetailView
from django.views.generic.edit import ProcessFormView, ModelFormMixin, DeleteView

from apps.documents.models import UserDocument, UserDocumentType
from apps.recruitment.forms import CandidateProfileForm, CandidateProfileUpdateForm


# def login_required(function=None, redirect_field_name=None, login_url=None):
#     from django.contrib.auth.decorators import login_required as lr_decorator, REDIRECT_FIELD_NAME
#     if redirect_field_name is None:
#         redirect_field_name = REDIRECT_FIELD_NAME
#     return lr_decorator(function=function, redirect_field_name=redirect_field_name, login_url=login_url)
from apps.user.models import Profile


@method_decorator(login_required, name="dispatch")
class RecruitmentHome(TemplateView):
    template_name = 'onboarding/home.html'


@method_decorator(login_required, name="dispatch")
class SubmissionView(TemplateView):
    template_name = 'onboarding/submission.html'

    def post(self, request, *args, **kwargs):
        self.request.user.new_candidate_form_is_in_draft_mode = False
        self.request.user.save()
        messages.success(
            request,
            "Your Form is submitted for verification from HR Department. "
            "You can wait for your HR's response."
        )
        return redirect(reverse('onboarding:information', host='onboarding'))



@method_decorator(login_required, name="dispatch")
class RecruitmentInformation(TemplateView):
    template_name = 'onboarding/information.html'


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(lambda user: user.new_candidate_form_is_in_draft_mode), name="dispatch")
class GeneralInformaton(SuccessMessageMixin, UpdateView):
    form_class = CandidateProfileForm
    login_url = settings.LOGIN_URL
    template_name = 'onboarding/general-information.html'
    model = get_user_model()
    success_message = "Your Profile has been updated successfully!"

    def get_object(self, queryset=None):
        return self.request.user

    success_url = reverse_lazy('onboarding:complete-profile', host='onboarding')


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(lambda user: user.new_candidate_form_is_in_draft_mode), name="dispatch")
class ProfileComplete(SuccessMessageMixin, UpdateView):
    form_class = CandidateProfileUpdateForm
    login_url = settings.LOGIN_URL
    template_name = 'onboarding/complete-profile.html'
    model = get_user_model()
    success_message = "Your Profile has been updated successfully!"

    def get_object(self, queryset=None):
        return Profile.objects.get_or_create(user=self.request.user)[0]

    success_url = reverse_lazy('onboarding:complete-profile', host='onboarding')


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(lambda user: user.new_candidate_form_is_in_draft_mode), name="dispatch")
class CandidateDocumentList(ModelFormMixin, ListView, ProcessFormView):
    object = None
    fields = ('doc_type', 'file_type', 'file', 'document_id', 'document_expiry')
    template_name = 'onboarding/candidate-document-list.html'

    def get_success_url(self):
        return reverse('onboarding:add-documents', host='onboarding')

    def get_queryset(self):
        return UserDocument.objects.filter(user=self.request.user)

    def get_context_data(self, *, object_list=None, **kwargs):
        self.object_list = self.get_queryset()
        pending_docs = UserDocumentType.objects.all().filter(is_required_for_new_candidates=True).exclude(id__in=self.object_list.values_list('doc_type', flat=True))
        kwargs['pending_docs'] = pending_docs
        return super(ModelFormMixin, self).get_context_data(object_list=self.object_list, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CandidateDocumentList, self).form_valid(form=form)


@method_decorator(login_required, name="dispatch")
@method_decorator(xframe_options_exempt, name="dispatch")
@method_decorator(user_passes_test(lambda user: user.new_candidate_form_is_in_draft_mode), name="dispatch")
class CandidateDocumentDelete(DeleteView):
    model = UserDocument
    template_name = 'onboarding/candidate-document-detail.html'

    def get_success_url(self):
        return reverse('onboarding:add-documents', host='onboarding')


@method_decorator(login_required, name="dispatch")
@method_decorator(xframe_options_exempt, name="dispatch")
@method_decorator(user_passes_test(lambda user: user.new_candidate_form_is_in_draft_mode), name="dispatch")
class CandidateDocumentDetail(ModelFormMixin, DetailView, ProcessFormView):
    model = UserDocument
    fields = ('doc_type', 'file_type', 'document_id', 'document_expiry')
    template_name = 'onboarding/candidate-document-detail.html'

    def get_queryset(self):
        return UserDocument.objects.filter(user=self.request.user)

    def get_success_url(self):
        return reverse('onboarding:add-documents', host='onboarding')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = {}
        self.object_list = self.get_queryset()
        self.object = self.get_object(self.object_list)
        context['object_list'] = self.object_list
        if self.object:
            context['object'] = self.object
            context_object_name = self.get_context_object_name(self.object)
            if context_object_name:
                context[context_object_name] = self.object
        context.update(kwargs)
        return super(ModelFormMixin, self).get_context_data(**context)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CandidateDocumentDetail, self).form_valid(form=form)
