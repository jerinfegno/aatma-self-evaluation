from django import template

from apps.user.models import User

register = template.Library()


@register.simple_tag
def net_active_users_count():
    return User.objects.filter(is_active=True).count()


@register.simple_tag
def profile_completion(user):
    return user.profile_completion_percentage()

