from django.urls import path

from apps.onboarding.views import *

app_name = "onboarding"


urlpatterns = [
    path('', RecruitmentHome.as_view(), name='home'),
    path('general-information/', GeneralInformaton.as_view(), name='general-information'),
    path('complete-profile/', ProfileComplete.as_view(), name='complete-profile'),
    path('view-documents/', CandidateDocumentList.as_view(), name='add-documents'),
    path('update-documents/<int:pk>/', CandidateDocumentDetail.as_view(), name='add-documents-update'),
    path('update-documents/<int:pk>/delete/', CandidateDocumentDelete.as_view(), name='add-documents-delete'),
    path('information/', RecruitmentInformation.as_view(), name='information'),
    path('submit/', SubmissionView.as_view(), name='submit'),
]


