import os

from django.conf import settings
from django.db import models
from django.utils.datetime_safe import date
from django.utils.safestring import mark_safe
# from private_files import PrivateFileField


def upload_directory_name(instance, filename):
    user = hasattr(instance, 'user') and instance.user
    model_name = instance._meta.verbose_name.replace(' ', '-')
    if user:
        name = f"{user.username}-{user.get_full_name().replace(' ', '-')}"
        return os.path.join('non-disclosable', model_name, name, filename)
    return os.path.join('non-disclosable', model_name, filename)


def is_authorized_file_access(request, instance):
    return request.user.is_authenticated


class FileTypeMixin(models.Model):
    TEXT = 'text'
    IMAGE = 'image'
    VIDEO = 'video'
    PDF = 'pdf'
    DOC = 'file'
    AUDIO = 'audio'
    file_type_choices = (
        (TEXT, TEXT),
        (IMAGE, IMAGE),
        (VIDEO, VIDEO),
        (DOC, DOC),
        (PDF, PDF),
        (AUDIO, AUDIO),
    )

    file_type = models.CharField(default=TEXT, blank=True, max_length=5,
                                 choices=file_type_choices,
                                 verbose_name="File Type")
    # file = PrivateFileField(upload_to=upload_directory_name, verbose_name="File Input",
    #                         condition=is_authorized_file_access, attachment=False)
    file = models.FileField(upload_to=upload_directory_name, verbose_name="File Input",)
                            # condition=is_authorized_file_access, attachment=False)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Uploaded On")
    needs_nda = models.BooleanField(default=False, verbose_name="Non Disclosable")
    name = ''

    def as_html(self):
        width = "auto"
        height = "320px"
        src = self.file and self.file.url or '#'
        doc_icon = f"{settings.STATIC_URL}img/doc.png"
        out = f"""<span>{self.name}</span>"""
        _id = f'document__{self.id}'

        if self.file and self.file_type == self.IMAGE:
            out = f"""<div>
                        <figure class="enlarge2-image">
                            <img class="thumbnail img-popper " src="{src}" width="{width}" height="{height}" alt="" />
                            <figcaption>{self.name}</figcaption>
                        </figure>
                    </div>"""
        elif self.file and self.file_type == self.VIDEO:
            out = f"""<div  class="enlarge-video">
                        <video controls width="{width}" height="{height}">
                            <source src="{src}">
                            <a href="{src}" download> Download Video </a>
                        </video>
                        <p>{self.name}</p>
                    </div>"""
        elif self.file and self.file_type == self.AUDIO:
            out = f"""<div>
                        <audio controls>
                            <source src="{src}" >
                            <a href="{src}" download> Download Audio </a>
                        </audio>
                        <p>{self.name}</p>
                    </div>"""
        elif self.file and self.file_type == self.PDF:
            if self.needs_nda:
                full_url = f'{src}#zoom=85&scrollbar=0&toolbar=0&navpanes=0" id="{_id}" style="pointer-events:none;'
                out = f"""<div>
                            <object width="100%" height="650" type="application/pdf" data="{full_url}">
                                <p>Couldnot load pdf.</p>
                            </object>
                        </div>
                        """
            else:
                out = f"""<div><embed src="{src}" width="100%" height="720px"  type="application/pdf" /></div>"""

        elif self.file:  # and self.file_type == self.DOC:
            out = f"""<div>
                        <p>{self.name}</p>
                        <a href="{src}" download class="bordered"> 
                            <img src="{doc_icon}" width="32px" height="auto" alt="Doc icon" />
                            Download this {self.file_type}.
                        </a>
                    </div>"""
        return mark_safe(out)

    class Meta:
        abstract = True


class UserDocumentType(models.Model):
    name = models.CharField(max_length=128)
    is_mandatory = models.BooleanField(default=False)
    is_required_for_new_candidates = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class UserDocument(FileTypeMixin):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    is_verified = models.BooleanField(default=False)
    document_id = models.CharField(max_length=128, null=True, blank=False, help_text='Document ID / Reference Number; if applicable')
    document_expiry = models.DateField(null=True, help_text='Expiry of the document; if applicable; like aadhar number, pan card number..')
    doc_type = models.ForeignKey(
        'documents.UserDocumentType', on_delete=models.SET_NULL, null=True,
        verbose_name="Document Type")

    def __str__(self):
        if self.doc_type:
            return f'{self.doc_type.name} - {(self.user and self.user.get_full_name() or "")}'
        return str(self.id)


class SharedDocument(FileTypeMixin):
    name = models.CharField(max_length=128)

    class Meta:
        verbose_name = "Policy Document"

    def __str__(self):
        return f"{self.name}   [Dated: {self.created_at}]"


class Circular(FileTypeMixin):
    name = models.CharField(max_length=128)
    description = models.TextField(null=True, blank=True)
    needs_nda = False
    is_new = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.name}   [Dated: {self.created_at}]"


PublicDocument = SharedDocument         # backward compatability


