from django.contrib import admin

# Register your models here.
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ExportActionMixin
from import_export.fields import Field

from apps.documents.models import *


class UserDocumentResource(resources.ModelResource):
    emp_id = Field()
    user = Field()
    address = Field()

    def dehydrate_emp_id(self, doc):
        return doc.user and doc.user.employee_id

    def dehydrate_user(self, doc):
        return doc.user and doc.user.get_full_name()

    def dehydrate_address(self, doc):
        return doc.user and doc.user.address_as_text()

    class Meta:
        model = UserDocument
        fields = ('id', 'emp_id', 'user', 'doc_type', 'file_type', 'file')


class UserDocumentAdmin(ImportExportModelAdmin):
    list_display = ['__str__', 'user', 'doc_type', 'file_type', 'file']
    list_filter = ['user', 'doc_type', 'file_type', 'file']
    resource_class = UserDocumentResource


admin.site.register(UserDocument, UserDocumentAdmin)
admin.site.register(UserDocumentType)
admin.site.register(SharedDocument)
admin.site.register(Circular)



