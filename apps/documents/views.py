from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.generic import ListView, DetailView
from django.views.generic.edit import ModelFormMixin, ProcessFormView

from apps.documents.models import SharedDocument, UserDocument, Circular


@method_decorator(login_required, name="dispatch")
class SharedDocumentList(ListView):
    model = SharedDocument
    fields = ('name', 'file_type', 'file', 'needs_nda')

    def get(self, request, *args, **kwargs):
        return super(SharedDocumentList, self).get(request, *args, **kwargs)


@method_decorator(login_required, name="dispatch")
class CircularList(ListView):
    model = Circular


@method_decorator(login_required, name="dispatch")
class CircularDetail(DetailView):
    model = Circular


@method_decorator(login_required, name="dispatch")
class UserDocumentList(ModelFormMixin, ListView, ProcessFormView):
    fields = ('doc_type', 'file_type', 'file', 'document_id', 'document_expiry')

    object = None

    def get_success_url(self):
        return reverse('documents:userdocuments-detail', kwargs={'pk': self.object.id})

    def get_queryset(self):
        return UserDocument.objects.filter(user=self.request.user)

    def get_context_data(self, *, object_list=None, **kwargs):
        self.object_list = self.get_queryset()
        return super(ModelFormMixin, self).get_context_data(object_list=self.object_list, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(UserDocumentList, self).form_valid(form=form)


@method_decorator(login_required, name="dispatch")
@method_decorator(xframe_options_exempt, name="dispatch")
class UserDocumentDetail(ModelFormMixin, DetailView, ProcessFormView):
    model = UserDocument
    fields = ('id', 'doc_type', 'file_type', 'file', 'document_id', 'document_expiry')

    def get_queryset(self):
        return UserDocument.objects.filter(user=self.request.user)

    def get_success_url(self):
        return reverse('documents:userdocuments-list')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = {}
        self.object_list = self.get_queryset()
        self.object = self.get_object(self.object_list)
        context['object_list'] = self.object_list
        if self.object:
            context['object'] = self.object
            context_object_name = self.get_context_object_name(self.object)
            if context_object_name:
                context[context_object_name] = self.object
        context.update(kwargs)
        return super(ModelFormMixin, self).get_context_data(**context)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(UserDocumentDetail, self).form_valid(form=form)


@method_decorator(login_required, name="dispatch")
@method_decorator(xframe_options_exempt, name="dispatch")
class SharedDocumentDetail(DetailView):
    model = SharedDocument


@login_required
def user_document_delete(request, pk):
    if request.method == 'POST':
        UserDocument.objects.filter(pk=pk, user=request.user).delete()
    return HttpResponseRedirect(reverse('documents:userdocuments-list'))



