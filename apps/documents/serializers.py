from rest_framework import serializers

from apps.documents.models import Circular, SharedDocument, UserDocument


class CircularSerializer(serializers.ModelSerializer):

    class Meta:
        model = Circular
        fields = ('name', 'file_type', 'file', 'created_at')


class SharedDocumentSerializer(serializers.ModelSerializer):

    class Meta:
        model = SharedDocument
        fields = ('name', 'file_type', 'file', 'created_at')


class UserDocumentSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserDocument
        fields = ('name', 'file_type', 'file', 'created_at')


