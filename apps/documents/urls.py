from django.test import TestCase
from django.urls import path, include

from apps.documents.api import CircularAPIList, UserDocumentDetailAPIList, UserDocumentAPIList, SharedDocumentAPIList
from apps.documents.views import *

app_name = 'documents'


api_urlpatterns = [
    path('circulars/', CircularAPIList.as_view(), name='circular-api-v1'),
    path('shared-document/', SharedDocumentAPIList.as_view(), name='shared-document-api-v1'),
    path('user-document/', UserDocumentAPIList.as_view(), name='user-document-api-v1'),
    path('user-document/<int:pk>/detail/', UserDocumentDetailAPIList.as_view(), name='user-document-detail-api-v1'),
]

urlpatterns = [
    path('circular/', CircularList.as_view(), name='circular-list'),
    path('circular/<int:pk>/', CircularDetail.as_view(), name='circular-detail'),
    path('policy-documents/', SharedDocumentList.as_view(), name='publicdocuments-list'),
    path('policy-documents/<int:pk>/', SharedDocumentDetail.as_view(), name='publicdocuments-detail'),
    path('user-documents/', UserDocumentList.as_view(), name='userdocuments-list'),
    path('user-documents/<int:pk>/', UserDocumentDetail.as_view(), name='userdocuments-detail'),
    path('user-documents/<int:pk>/delete/', user_document_delete, name='userdocuments-delete'),
    path('api/v1/', include(api_urlpatterns))

]

