from rest_framework.generics import *

from apps.documents.models import (SharedDocument, UserDocument, Circular)
from apps.documents.serializers import (SharedDocumentSerializer, UserDocumentSerializer, CircularSerializer)


class CircularAPIList(ListAPIView):
    serializer_class = CircularSerializer
    queryset = Circular.objects.all().order_by('-id')


class SharedDocumentAPIList(ListAPIView):
    serializer_class = SharedDocumentSerializer
    queryset = SharedDocument.objects.all().order_by('-id')


class UserDocumentAPIList(ListCreateAPIView):
    serializer_class = UserDocumentSerializer

    def get_queryset(self):
        return UserDocument.objects.filter(user=self.request.user)


class UserDocumentDetailAPIList(RetrieveUpdateDestroyAPIView):
    serializer_class = UserDocumentSerializer

    def get_queryset(self):
        return UserDocument.objects.filter(user=self.request.user)

