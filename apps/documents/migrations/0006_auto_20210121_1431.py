# Generated by Django 3.1.3 on 2021-01-21 09:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0005_auto_20210121_1406'),
    ]

    operations = [
        migrations.AddField(
            model_name='shareddocument',
            name='needs_nda',
            field=models.BooleanField(default=False, verbose_name='Non Disclosable'),
        ),
        migrations.AddField(
            model_name='userdocument',
            name='needs_nda',
            field=models.BooleanField(default=False, verbose_name='Non Disclosable'),
        ),
    ]
