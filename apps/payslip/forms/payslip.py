from django.forms import inlineformset_factory, modelform_factory, HiddenInput

from apps.payslip.models import MonthlyPaymentSummery, AttendanceData
from apps.user.models import User
from apps.utils.forms import BootstrapModelForm

PayslipForm = modelform_factory(
    MonthlyPaymentSummery,
    fields=("is_frozen", ))

PayslipUpdateForm = modelform_factory(
    MonthlyPaymentSummery,
    fields=('is_frozen', ),
    widgets={'is_frozen': HiddenInput()},
)


class AttendanceDataForm(BootstrapModelForm):

    class Meta:
        model = AttendanceData
        fields = ('user', 'days_present', 'loss_of_pay', 'paid_leaves',)


BaseAttendanceDataFormSet = inlineformset_factory(
    MonthlyPaymentSummery,
    AttendanceData,
    form=AttendanceDataForm,
    extra=0,
    validate_min=True,
)


class AttendanceDataFormSet(BaseAttendanceDataFormSet):

    def __init__(self, *args, **kwargs):
        kwargs['min_num'] = User.objects.filter(is_active=True).exclude(username='admin').count(),
        super(AttendanceDataFormSet, self).__init__(*args, **kwargs)


__all__ = (
    'AttendanceDataFormSet', 'AttendanceDataForm', 'PayslipUpdateForm', 'PayslipForm'
)




