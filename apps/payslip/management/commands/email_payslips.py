import logging

from django.conf import settings
from django.core.mail import send_mass_mail, get_connection, EmailMessage, EmailMultiAlternatives
from django.core.management import BaseCommand
from django.template.loader import render_to_string

from apps.payslip.models import AttendanceData
from apps.payslip.views import generate_payslip


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        emails = []
        updates = []
        connection = get_connection()
        qs = AttendanceData.objects.filter(is_frozen=True, grouper__is_frozen=True, payslip_sent=False).select_related('grouper', 'user')
        print("sending email to ", qs.count(),  "Users")
        print([q.user for q in qs])
        for payslip_object in qs:
            pdf = generate_payslip(pk=payslip_object.id)
            # subject = '', body = '', from_email = None, to = None, bcc = None,
            # connection = None, attachments = None, headers = None, cc = None,
            # reply_to = None
            context = {
                'object': payslip_object
            }
            em = EmailMultiAlternatives(
                subject=f"""Payslip for month {payslip_object.grouper.month_name}, {payslip_object.grouper.year}""",
                body=render_to_string('payslip/email-template/payslip_for_the_month.txt', context=context),
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[payslip_object.user.email],
                connection=connection,
                reply_to=("hello@fegno.com", )
            )
            html_message = render_to_string('payslip/email-template/payslip_for_the_month.html', context=context)
            em.attach_alternative(html_message, "text/html")

            filename = f"Payslip_{payslip_object.grouper.month_name}_{payslip_object.grouper.year}_"
            filename += f"{payslip_object.user.employee_id or payslip_object.user.username}"
            filename += f"_Fegno_Technologies.pdf"

            em.attach(filename, pdf, 'application/pdf')
            emails.append(em)
            updates.append(payslip_object.pk)
        try:
            print(connection.send_messages(emails))
            print("Successfully Send Emails!")
        except Exception as e:
            logging.error(e)
        else:
            AttendanceData.objects.filter(pk__in=updates).update(payslip_sent=True)
