from django.core.management import BaseCommand
from django.utils.datetime_safe import date

from apps.payslip.models import MonthlyPaymentSummery


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        for payslip_object in MonthlyPaymentSummery.objects.all().filter(is_frozen=True):
            payslip_object.generated_at = date(day=5, month=payslip_object.month+1, year=payslip_object.year)
            payslip_object.save()
