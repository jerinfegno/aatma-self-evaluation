from crispy_forms.helper import FormHelper
from django.contrib.auth.decorators import user_passes_test, login_required
from django.db.models.functions import Concat
from django.forms import modelform_factory
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.datetime_safe import date
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import ListView, DetailView
from django.views.generic.edit import ProcessFormView, FormMixin, UpdateView
# from easy_pdf.rendering import render_to_pdf
from easy_pdf.views import PDFTemplateView
from extra_views import UpdateWithInlinesView

from apps.payslip.form import PaySlipCreateForm, AttendanceDataInline
from apps.payslip.forms.payslip import PayslipUpdateForm
from apps.payslip.models import MonthlyPaymentSummery, AttendanceData
from apps.payslip.utils.payslip_generator import generate_payslip
from apps.payslip.utils.payslip_manager import PayslipConstructor
from django.db.models import Prefetch, F, Value
from apps.utils.permissions import ManagerRequired, manager_required, LoginRequired, HRRequired


class AttendanceDataList(LoginRequired, ListView):
    model = AttendanceData
    template_name = 'payslip/attendancedata_list.html'

    def get_queryset(self):
        return AttendanceData.objects.filter(
            user=self.request.user, is_frozen=True, grouper__is_frozen=True).order_by('-id').select_related('user', 'grouper')


class PaySlipList(HRRequired, FormMixin, ListView, ProcessFormView):
    model = MonthlyPaymentSummery
    template_name = 'payslip/paymentsummery_list.html'
    form_class = PaySlipCreateForm
    http_method_names = ('get', 'post')
    object = None
    object_list = None
    ordering = ('-year', '-month')

    def get_success_url(self):
        return reverse('payslip:payslip-detail', kwargs={'pk': self.object.pk})

    def get(self, request, *args, **kwargs):
        self.pending_object_list = self.get_queryset().filter(is_frozen=False)
        self.object_list = self.get_queryset().filter(is_frozen=True)
        return super(PaySlipList, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        self.pending_object_list = None
        return super(PaySlipList, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return super().get_context_data(pending_object_list=self.pending_object_list, **kwargs)

    def form_valid(self, form):
        pc = PayslipConstructor(payslip_class=MonthlyPaymentSummery, **form.cleaned_data)
        pc.calculate()
        self.object = pc.payslip
        return HttpResponseRedirect(self.get_success_url())


class PayslipDetail(HRRequired, DetailView):
    model = MonthlyPaymentSummery
    login_url = reverse_lazy('payslip:payslip-my-list')

    def get_context_data(self, **kwargs):
        if self.object.data.exists() and not self.object.data.filter(is_frozen=False).exists() and not self.object.is_frozen:
            self.object.is_frozen = True
            self.object.generated_at = date(day=5, month=self.object.month+1 if self.object.month < 12 else 1, year=self.object.year if self.object.month < 12 else self.object.year+1)
            self.object.save()
        return super(PayslipDetail, self).get_context_data(**kwargs)

    def get_queryset(self):
        prefetch_qs = AttendanceData.objects.select_related(
            'user', 'user__department', 'user__designation', 'user__reporting_to').order_by('user_id')
        return self.model.objects.prefetch_related(Prefetch('data', queryset=prefetch_qs))


@login_required
@user_passes_test(manager_required)
def payslip_details_for_account(request, **kwargs):
    import django_excel as excel
    pay_object = get_object_or_404(MonthlyPaymentSummery.objects.filter(is_frozen=True), **kwargs)

    headers = ['Employee', 'Days Present', 'Leave', 'LOP', 'Salary to be credited']
    data = AttendanceData.objects.filter(grouper=pay_object.pk,).order_by('user_id').annotate(
        user_name=Concat(F('user__first_name'), Value(' '), F('user__last_name')),
        total_leave=F('loss_of_pay')+F('paid_leaves')
    ).values_list(
        'user_name', 'days_present', 'total_leave', 'loss_of_pay', 'net_pay'
    )
    print([headers] + list(data))
    sheet = excel.pe.Sheet([headers] + list(data))
    return excel.make_response(sheet, "xlsx")


class AttendanceDataUpdate(LoginRequired, UpdateView):
    queryset = AttendanceData.objects.all().select_related('grouper', 'user')
    fields = (
        'days_present', 'loss_of_pay', 'paid_leaves',
        *AttendanceData.addable_payment,
        *AttendanceData.deductible_payment,
        'payment_method', 'net_pay', 'is_frozen',
    )
    template_name = 'payslip/attendancedata_form.html'

    def get_queryset(self):
        qs = super(AttendanceDataUpdate, self).get_queryset()
        if not self.request.user.is_hr and not self.request.user.is_manager:
            qs = qs.filter(user=self.request.user)
        return qs

    def get_success_url(self):
        return reverse('payslip:attendance-update', kwargs={'pk': self.object.pk})


@method_decorator(login_required, 'dispatch')
class AttendanceDataDownload(PDFTemplateView):
    queryset = AttendanceData.objects.all().select_related('grouper', 'user')
    fields = (
        'days_present', 'loss_of_pay', 'paid_leaves',
        *AttendanceData.addable_payment,
        *AttendanceData.deductible_payment,
        'payment_method', 'net_pay', 'is_frozen',
    )
    template_name = 'payslip/attendance_pdf.html'

    def get_object(self):
        return get_object_or_404(self.queryset, **self.kwargs)

    def get_context_data(self, **kwargs):
        kwargs['form'] = modelform_factory(AttendanceData, fields=self.fields)
        kwargs['object'] = self.get_object()
        return super(AttendanceDataDownload, self).get_context_data(**kwargs)


@method_decorator(login_required, 'dispatch')
class GeneratePdf(View):

    def get(self, request, *args, **kwargs):
        pdf = generate_payslip(**kwargs)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "Payslip_FegnoTechnologies.pdf"
            content = "inline; filename=%s" % filename
            download = request.GET.get("download")
            if download:
                content = "attachment; filename=%s" % filename
            response['Content-Disposition'] = content
            return response

        return HttpResponse("No Content", status=404)


class PaySlipUpdate(HRRequired, UpdateWithInlinesView):
    model = MonthlyPaymentSummery
    inlines = [AttendanceDataInline, ]

    form_class = PayslipUpdateForm
    template_name = 'payslip/paymentsummery_form.html'

    def get_success_url(self):
        return reverse('payslip:payslip-detail', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        kwargs = super(PaySlipUpdate, self).get_context_data(**kwargs)
        for formset in kwargs['inlines']:
            formset.helper = FormHelper()
            formset.helper.form_show_labels = False
        kwargs['form'] = self.get_form(self.get_form_class())
        return kwargs

