{% load humanize solo_tags %}
{% get_solo 'accounts.CompanyConfig' as site_config %}
Hi {{object.user.first_name}},

Your Payslip for this month '{{object.grouper.month_name}}, {{object.grouper.year}}' is attached along with.
Please find the attachment!
If you have any question, Please contact us back!


Regards,
HR Department,
{{ site_config.name }}.
Cochin - {{ site_config.pincode }}

Dated: {% now "jS F Y H:i" %}

This is an automated email from Fegno Internal Management System!
The contents of this email message and any attachments are intended solely for the addressee(s)
and may contain confidential and/or privileged information and may be legally protected from
disclosure. If you are not the intended recipient of this message or their agent, or if this message
has been addressed to you in error, please immediately alert the sender by reply email and then
delete this message and any attachments. If you are not the intended recipient, you are hereby
notified that any use, dissemination, copying, or storage of this message or its attachments is
strictly prohibited.

