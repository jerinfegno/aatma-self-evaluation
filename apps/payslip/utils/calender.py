import calendar as py_calender
from datetime import date
from typing import Optional

from apps.leave.models import Holiday


class CALENDER:
    JANUARY = 'January'
    FEBRUARY = 'February'
    MARCH = 'March'
    APRIL = 'April'
    MAY = 'May'
    JUNE = 'June'
    JULY = 'July'
    AUGUST = 'August'
    SEPTEMBER = 'September'
    OCTOBER = 'October'
    NOVEMBER = 'November'
    DECEMBER = 'December'
    MONTHS = (
        (1, JANUARY),
        (2, FEBRUARY),
        (3, MARCH),
        (4, APRIL),
        (5, MAY),
        (6, JUNE),
        (7, JULY),
        (8, AUGUST),
        (9, SEPTEMBER),
        (10, OCTOBER),
        (11, NOVEMBER),
        (12, DECEMBER),
    )

    @staticmethod
    def no_of_days(year: int, month: int):
        return [
            0,  # 0
            31,  # 1 => Jan
            28 + CALENDER.isleap(year),  # 2 => Feb
            31,  # 3 => Mar
            30,  # 4=> Apr
            31,  # 5 => May
            30,  # 6 => Jun
            31,  # 7 => Jul
            31,  # 8 => Aug
            30,  # 9 => Sept
            31,  # 10 => Oct
            30,  # 11 => Nov
            31,  # 12 => Dec
        ][month]

    weekends = ['Saturday', 'Sunday']

    @staticmethod
    def month_index(month: str):
        return dict((
            (CALENDER.JANUARY, 1),
            (CALENDER.FEBRUARY, 2),
            (CALENDER.MARCH, 3),
            (CALENDER.APRIL, 4),
            (CALENDER.MAY, 5),
            (CALENDER.JUNE, 6),
            (CALENDER.JULY, 7),
            (CALENDER.AUGUST, 8),
            (CALENDER.SEPTEMBER, 9),
            (CALENDER.OCTOBER, 10),
            (CALENDER.NOVEMBER, 11),
            (CALENDER.DECEMBER, 12),
        ))[month]

    @staticmethod
    def month_index_reverse(month: int):
        return (None,
                CALENDER.JANUARY,
                CALENDER.FEBRUARY,
                CALENDER.MARCH,
                CALENDER.APRIL,
                CALENDER.MAY,
                CALENDER.JUNE,
                CALENDER.JULY,
                CALENDER.AUGUST,
                CALENDER.SEPTEMBER,
                CALENDER.OCTOBER,
                CALENDER.NOVEMBER,
                CALENDER.DECEMBER,
                )[month]

    @staticmethod
    def get_all_official_holidays(year: int, month: int) -> set:
        """
        Returns a set of dates, including saturday, sunday and Official holidays.
        This wont include any Restricted Holiday
        """
        # holidays can be from saturday or sunday
        holidays_set = Holiday.objects.filter(
            date__year=year, date__month=month,
            restricted_group__isnull=True).only('date')

        holidays = set([h.date for h in holidays_set])
        weekends = set([
            date(year, month, day)
            for day in range(1, py_calender.monthrange(year, month)[1] + 1)
            if date(year, month, day).strftime('%A') in CALENDER.weekends])
        return holidays.union(weekends)

    @staticmethod
    def calculate_working_days_for_month(year: int, month: int):
        no_of_days = CALENDER.no_of_days(year, month)
        holidays = CALENDER.get_all_official_holidays(year, month)
        return no_of_days - len(holidays)

    @staticmethod
    def isleap(year):
        return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)
