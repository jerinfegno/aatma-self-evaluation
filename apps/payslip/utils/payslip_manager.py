from typing import Tuple, Union

from django.db.models import Q, F, QuerySet
from django.utils.datetime_safe import date

from apps.leave.models import LeaveRequest
from apps.user.models import User
from apps.payslip.utils.calender import CALENDER


class PayslipCouldNotBeGenerated(Exception):
    pass


class PayslipGenerationValidator:

    def __init__(self, payslip_class: type, year: int, month: int):
        self.payslip_class: type = payslip_class
        self.year: int = year
        self.month: int = month
        self.status: bool = True
        self.reason: str = ""
        today = date.today()
        last_date_of_month = CALENDER.no_of_days(year, month)
        last_day_of_payslip = date(day=last_date_of_month, month=month, year=year)
        if last_day_of_payslip >= today:
            self.status = False
            self.reason = "You cannot generate Payslip for Future"
            return
        exists = payslip_class.objects.filter(year=year, month=month).exists()
        if exists:
            self.status = False
            self.reason = "Payslip already Generated"
            return

        qs: QuerySet = LeaveRequest.objects.filter(
            Q(
                Q(from_date__month=self.month) & Q(from_date__year=self.year)
            ) | Q(
                Q(to_date__month=self.month) & Q(to_date__year=self.year)
            ),
            approval_status=LeaveRequest.WAITING_FOR_APPROVAL,
            user__is_active=True,
        ).annotate(user_name=F('user__first_name')).values_list('user_name', flat=True)
        if qs:
            self.status = False
            users = ','.join([q for q in qs])
            self.reason = f"There are some pending leave request from {users} waiting for your action! " \
                          f"These will conflict with the dates in {CALENDER.month_index_reverse(month)}, {year}." \
                          f"Please Take an action on these first and then proceed Generating the Payslip further."
            return

    def __bool__(self):
        return self.status


class PayslipConstructor(object):

    def __init__(self, payslip_class: type, year: int, month: Union[int, str]):
        self.payslip_class: type = payslip_class
        if type(month) is str:
            month = int(month)
        self.month: int = month
        self.year: int = year
        self.payslip = None

    def _calculate_user_leaves(self, qs, user) -> dict:
        assert self.payslip is not None, "Payslip must be defined before calling this function."
        assert user.net_pay is not None, "Please Update User Salary First"
        loss_of_pay = paid_leaves = 0
        for leave in qs:
            if leave.user == user and user.username is not 'admin':
                paid_leaves += leave.leave_approved_in_days
                loss_of_pay += leave.leave_lop_in_days
        days_present = self.payslip.net_working_days - (paid_leaves + loss_of_pay) + (paid_leaves and loss_of_pay)
        lop_deduction: float = (float(loss_of_pay)/self.payslip.net_salarified_days) * user.net_pay

        first_day_of_payslip_month = date(day=1, month=self.payslip.month, year=self.payslip.year)
        leave_due_to_delay_in_joining = (first_day_of_payslip_month - user.doj).days  # if user joined this month
        if user.doj > first_day_of_payslip_month:
            """
            leave_due_to_delay_in_joining is positive means, this user joined this month. 
            hence we have to consider, these days as lop in payslip.
            """
            days_present -= leave_due_to_delay_in_joining
            loss_of_pay += leave_due_to_delay_in_joining

        out = {
            'days_present': days_present,
            'loss_of_pay': loss_of_pay,
            'paid_leaves': paid_leaves,
        }
        from apps.payslip.models import AttendanceData

        for field in AttendanceData.addable_payment:
            if hasattr(user, field):
                out[field] = getattr(user, field)
            else:
                out[field] = 0.0
        for field in AttendanceData.deductible_payment:
            if hasattr(user, field):
                out[field] = getattr(user, field)
            else:
                out[field] = 0.0
        out['loss_of_pay_deduction'] = lop_deduction    # to get overridden from automated value calculation
        out['net_pay'] = (user.net_pay or 0) - lop_deduction    # to get overridden from automated value calculation
        return out

    def calculate(self):
        payslip_validation = PayslipGenerationValidator(self.payslip_class, self.year, self.month, )
        if not payslip_validation:
            raise PayslipCouldNotBeGenerated(payslip_validation.reason)
        self.payslip = self.payslip_class.objects.create(
            year=self.year, month=self.month,
            net_working_days=self.payslip_class().get_net_working_days()
        )
        leave_data = LeaveRequest.objects.filter(
            Q(
                Q(from_date__month=self.month) & Q(from_date__year=self.year)
            ) | Q(
                Q(to_date__month=self.month) & Q(to_date__year=self.year)
            ),
            approval_status=LeaveRequest.APPROVED,
            user__is_active=True,
        ).select_related('leave_type')
        from apps.payslip.models import AttendanceData

        for employee in User.objects.filter(is_active=True).exclude(is_manager=True):
            if (
                    employee.doj.year < self.payslip.year or
                    (self.payslip.year == employee.doj.year and employee.doj.month <= self.payslip.month)
            ):
                ad, _ = AttendanceData.objects.get_or_create(
                    grouper=self.payslip,
                    user=employee,
                    defaults=self._calculate_user_leaves(leave_data, user=employee)
                )
        return self.payslip


