from datetime import datetime

from django.contrib import messages
from django.db import transaction
from django.http import HttpResponseRedirect


class FormsetViewMixin:
    formset_object = None

    def get_formset(self):
        if self.formset_object is None:
            instance = self.get_object(self.get_queryset())
            if self.request.POST:
                self.formset_object = self.formset_class(
                    self.request.POST, self.request.FILES, instance=instance)
            else:
                self.formset_object = self.formset_class(instance=instance)
        return self.formset_object

    def get_context_data(self, **kwargs):
        data: dict = super(FormsetViewMixin, self).get_context_data(**kwargs)
        # instance = self.get_object(self.get_queryset())
        if 'formset' not in kwargs:
            data['formset'] = self.get_formset()
        data['formset_empty_form'] = data['formset'].empty_form         # since 'empty_form' is a generator, we take a clone.
        return data

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        self.object = self.get_object()
        form = self.get_form()
        formset = self.get_formset()
        formset.instance = self.object
        valid_formset = formset.is_valid()
        valid_form = form.is_valid()
        if valid_form and valid_formset:
            return self.form_valid(form=form, formset=formset)
        else:
            form.instance.is_submitted = False
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        context = self.get_context_data()
        # formset = context['formset']
        with transaction.atomic():
            form.instance.created_by = self.request.user
            form.instance.created_at = datetime.now()
            self.object = form.save()
            formset.instance = self.object

            formset.save()
            if form.instance.entries.count() > 0:
                messages.success(self.request, "Form Submitted Successfully!")
                return HttpResponseRedirect(self.get_success_url())
            else:
                obj = self.object or form.instance
                obj.is_submitted = False
                obj.save()
                messages.error(self.request,
                               "Couldnot Submit The form since there is no line entry. Form saved as draft.")
        return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        return self.render_to_response(self.get_context_data(form=form, formset=formset))

