from django.forms import modelform_factory
from django.shortcuts import get_object_or_404
from easy_pdf.rendering import render_to_pdf

from apps.payslip.models import AttendanceData


def generate_payslip(pagesize='A5 landscape', **kwargs):
    queryset = AttendanceData.objects.all().select_related('grouper', 'user')
    fields = (
        'days_present', 'loss_of_pay', 'paid_leaves',
        *AttendanceData.addable_payment,
        *AttendanceData.deductible_payment,
        'payment_method', 'net_pay', 'is_frozen',
    )
    template_name = 'payslip/attendance_pdf.html'
    kwargs['object'] = get_object_or_404(queryset, **kwargs)
    kwargs['form'] = modelform_factory(AttendanceData, fields=fields)
    kwargs['pagesize'] = pagesize
    return render_to_pdf(template_name, kwargs)

