from datetime import date

from django import forms
from django.forms import inlineformset_factory
from extra_views import InlineFormSetFactory

from apps.payslip.models import MonthlyPaymentSummery, month_validation, year_validation, AttendanceData
from apps.payslip.utils.calender import CALENDER
from apps.payslip.utils.payslip_manager import PayslipGenerationValidator, PayslipConstructor
from apps.utils.forms import BootstrapFormMixin, BootstrapModelForm


class PaySlipCreateForm(BootstrapFormMixin, forms.Form):
    month = forms.ChoiceField(choices=CALENDER.MONTHS, validators=[month_validation])
    year = forms.ChoiceField(choices=[(i, i) for i in range(date.today().year, 2020, -1)], validators=[year_validation])

    def clean_month(self):
        return int(self.cleaned_data['month'])

    def clean_year(self):
        return int(self.cleaned_data['year'])

    def clean(self):
        cleaned_data = self.cleaned_data
        payslip_validation = PayslipGenerationValidator(
            MonthlyPaymentSummery, int(cleaned_data['year']), int(cleaned_data['month'])
        )
        if not payslip_validation:
            raise forms.ValidationError(payslip_validation.reason)
        return cleaned_data


class AttendanceDataForm(BootstrapModelForm):

    class Meta:
        model = AttendanceData
        fields = (
            # 'days_present', 'loss_of_pay', 'paid_leaves',
            # *AttendanceData.addable_payment, *AttendanceData.deductible_payment
        )


class AttendanceDataInline(InlineFormSetFactory):
    model = AttendanceData
    form_class = AttendanceDataForm

    def get_factory_kwargs(self):
        kwargs = super(AttendanceDataInline, self).get_factory_kwargs()
        kwargs['extra'] = 0
        kwargs['can_delete'] = False
        return kwargs

    def construct_formset(self):
        formset = super(AttendanceDataInline, self).construct_formset()
        formset.queryset = formset.queryset.order_by('user_id')
        return formset


