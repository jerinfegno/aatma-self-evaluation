from django.shortcuts import render
from django.urls import path, include, reverse_lazy
from django.views.generic import DeleteView

from apps.payslip.models import MonthlyPaymentSummery, AttendanceData
from apps.payslip.views import PaySlipList, AttendanceDataUpdate, PayslipDetail, \
    GeneratePdf, payslip_details_for_account, AttendanceDataList

app_name = 'payslip'

api_urlpatterns = []


def template_preview(request):
    from apps.accounts.models import ProformaInvoice
    cxt = {
        'object': ProformaInvoice.objects.order_by('id').first()
    }
    return render(request, template_name='accounts/proforma-invoice-templates.html', context=cxt)


urlpatterns = [
    path('view/', template_preview),
    path('payslip/', include([
        path('', PaySlipList.as_view(), name="payslip-list"),
        path('<int:pk>/detail/', PayslipDetail.as_view(), name="payslip-detail"),
        path('<int:pk>/detail-sheet.xlsx', payslip_details_for_account, name="payslip-sheet"),

        # path('<int:pk>/update/', PaySlipUpdate.as_view(), name="payslip-update"),
        path('<int:pk>/delete/', DeleteView.as_view(
            model=MonthlyPaymentSummery, success_url=reverse_lazy("payslip:payslip-list")
        ), name="payslip-delete"),
        path('attendance/<int:pk>/view/', AttendanceDataUpdate.as_view(), name="attendance-update"),
        path('attendance/<int:pk>/download/', GeneratePdf.as_view(), name="attendance-download"),

        # Common view to list all of my payslip
        path('all/', AttendanceDataList.as_view(), name="payslip-my-list"),
    ]))
]
