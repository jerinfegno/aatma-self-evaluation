from django.apps import AppConfig


class PayslipConfig(AppConfig):
    name = 'apps.payslip'
