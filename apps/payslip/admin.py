from django.contrib import admin
from import_export.admin import ImportExportModelAdmin, ExportActionMixin

from apps.payslip.models import MonthlyPaymentSummery, AttendanceData
from apps.user.models import Designation, Department


class InlineAttendanceData(admin.TabularInline):
    model = AttendanceData
    fields = ('user', 'days_present', 'loss_of_pay', 'paid_leaves', )


class MonthlyPaymentSummeryAdmin(ImportExportModelAdmin):
    inlines = [InlineAttendanceData, ]


class CommonAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Department, CommonAdmin)
admin.site.register(Designation, CommonAdmin)
admin.site.register(MonthlyPaymentSummery, MonthlyPaymentSummeryAdmin)

