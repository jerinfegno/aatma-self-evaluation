import logging

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.utils.datetime_safe import date
from num2words import num2words

from apps.generic.models.salary import SalaryAbstractModel
from apps.payslip.utils.calender import CALENDER
from apps.payslip.utils.payslip_manager import PayslipConstructor, PayslipCouldNotBeGenerated

logger = logging.getLogger(__name__)


def year_validation(value):
    if type(value) is str:
        value = int(value)
    if not (2020 <= value <= date.today().year):
        raise ValidationError(f"Year must be between 2020 and {date.today().year}")
    return value


def month_validation(value):
    if type(value) is str:
        value = int(value)
    if not (1 <= value <= 12):
        raise ValidationError(f"Month must  be between 1 and 12")
    return value


class MonthlyPaymentSummery(models.Model):
    month: int = models.PositiveSmallIntegerField(choices=CALENDER.MONTHS, validators=[month_validation])
    year: int = models.PositiveSmallIntegerField(validators=[year_validation])
    is_frozen: bool = models.BooleanField(default=False)
    net_working_days: int = models.PositiveSmallIntegerField(default=31)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    generated_at = models.DateTimeField(null=True)

    @property
    def net_salarified_days(self) -> int:
        return CALENDER.no_of_days(year=self.year, month=self.month)

    class Meta:
        unique_together = [
            ('month', 'year')
        ]

    def __str__(self):
        return f"{self.month_name}, {self.year} ({self.net_working_days} days)"

    @property
    def month_name(self) -> str:
        return CALENDER.month_index_reverse(self.month)

    @classmethod
    @transaction.atomic
    def generate_pay_summery(cls, year, month) -> None:
        pc = PayslipConstructor(cls, year, month)
        try:
            pc.calculate()
        except PayslipCouldNotBeGenerated as e:
            transaction.rollback()
            logger.warning(e)

    def get_net_working_days(self):
        # return CALENDER.calculate_working_days_for_month(self.year, self.month)
        return 30

    def save(self, **kwargs) -> None:
        self.net_working_days = self.get_net_working_days()          # fixed for company
        if self.is_frozen and (
                self.year > date.today().year
                or (self.year == date.today().year and self.month > date.today().month)):
            self.is_frozen = False
            logger.warning("You cannot freeze payslip before month ends.")
        if self.is_frozen and self.generated_at is None:
            self.generated_at = date(day=5, month=self.month+1 if self.month < 12 else 1, year=self.year if self.month < 12 else self.year+1)
        super(MonthlyPaymentSummery, self).save(**kwargs)


class AttendanceData(SalaryAbstractModel):
    CHEQUE = 'Cheque'
    BANK_TRANSFER = 'Bank Transfer'
    BY_HAND = 'By Hand'

    grouper = models.ForeignKey(MonthlyPaymentSummery, on_delete=models.CASCADE, related_name='data')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    days_present = models.FloatField()
    loss_of_pay = models.FloatField()
    paid_leaves = models.FloatField()
    payment_method = models.CharField(choices=[
        (BANK_TRANSFER, BANK_TRANSFER), (CHEQUE, CHEQUE), (BY_HAND, BY_HAND),
    ], default=BANK_TRANSFER, max_length=20)
    is_frozen: bool = models.BooleanField(default=False, verbose_name="Mark as Finalized")

    advance_payment = models.FloatField(
        default=0.0, verbose_name="Advance",
        help_text="Advance Salary from Next Month.")
    loss_of_pay_deduction = models.FloatField(
        default=0.0, verbose_name="LOP",
        help_text="Loss Of Pay, as per Company Policies [2021].")
    advance_deduction = models.FloatField(
        default=0.0, verbose_name="Advance Deduction",
        help_text="Debiting of Salary paid Advance in previous Month.")

    payslip_sent = models.BooleanField(default=False)
    addable_payment = SalaryAbstractModel.addable_payment + ('advance_payment', )
    deductible_payment = SalaryAbstractModel.deductible_payment + ('loss_of_pay_deduction', 'advance_deduction', )
    addable_payment_percentage = SalaryAbstractModel.addable_payment + (0.0, )
    deductible_payment_percentage = SalaryAbstractModel.deductible_payment + (0.0, 0.0, )

    @property
    def paid_days(self):
        return self.days_present + self.paid_leaves

    @property
    def payroll_number(self):
        return "PRN" + str(self.pk).zfill(8)

    @property
    def days_absent(self) -> int:
        return self.paid_leaves + self.loss_of_pay

    @property
    def to_word(self):
        return num2words(self.net_pay, lang='en_IN')
